package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.OrganisasiModel;
import com.example.demo.repository.OrganisasiRepository;

@Service
@Transactional
public class OrganisasiService {

	@Autowired
	private OrganisasiRepository organisasiRepository;
	
	public void create(OrganisasiModel organisasiModel) {
		this.organisasiRepository.save(organisasiModel);
	}
	
	public Long searchMaxId() {
		return this.organisasiRepository.searchMaxId();
	}
	
	public List<OrganisasiModel> read(){
		return this.organisasiRepository.findAll();
	}
	
	public OrganisasiModel searchIdOrg(Long id) {
		return this.organisasiRepository.searchIdOrganisasi(id);
	}
	
	public OrganisasiModel searchId(String id) {
		return this.organisasiRepository.searchId(id);
	}
	
	public List<OrganisasiModel> searchNotDelete(){
		return this.organisasiRepository.searchNotDelete();
	}
	
	public void update(OrganisasiModel organisasiModel) {
		this.organisasiRepository.save(organisasiModel);
	}
	
	public void delete(OrganisasiModel organisasiModel) {
		this.organisasiRepository.delete(organisasiModel);
	}
	
	public List<OrganisasiModel> findByBiodataAndIsNotDelete(Long id){
		return this.organisasiRepository.findByBiodataAndIsNotDelete(id);
	}
}
