package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

//import com.example.demo.model.BiodataModel;
//import com.example.demo.model.TimePeriodModel;
import com.example.demo.model.RiwayatPelatihanModel;
//import com.example.demo.repository.BiodataRepository;
//import com.example.demo.repository.TimePeriodRepository;
import com.example.demo.repository.RiwayatPelatihanRepository;

@Service
@Transactional
public class RiwayatPelatihanService {

	@Autowired
	private RiwayatPelatihanRepository riwayatPelatihanRepository;
	
	public List<RiwayatPelatihanModel> findByBiodataAndIsNotDelete(Long biodataId) {
		return this.riwayatPelatihanRepository.findByBiodataAndIsNotDelete(biodataId);
	}
	
	public RiwayatPelatihanModel searchIdRiwayatPelatihan(Long id) {
		return this.riwayatPelatihanRepository.searchIdRiwayatPelatihan(id);
	}
	
	public List<RiwayatPelatihanModel> searchRiwayatPelatihanByBiodataId(Long biodataId){
		return this.riwayatPelatihanRepository.searchPelatihanByBiodataId(biodataId);
	}
	
	public void save(RiwayatPelatihanModel riwayatPelatihanModel) {
		this.riwayatPelatihanRepository.save(riwayatPelatihanModel);
	}
	
	public void update(RiwayatPelatihanModel riwayatPelatihanModel) {
		this.riwayatPelatihanRepository.save(riwayatPelatihanModel);
	}
	
}
