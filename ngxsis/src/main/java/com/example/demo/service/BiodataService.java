package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.BiodataModel;
import com.example.demo.repository.BiodataCRUDRepository;
import com.example.demo.repository.BiodataRepository;

@Service
@Transactional
public class BiodataService {
	@Autowired
	private BiodataRepository biodataRepository;
	
	private BiodataCRUDRepository crud;

	public String create(BiodataModel biodataModel) {
		Boolean cek = false;
		String message = "";

		if (biodataModel.getEmail() != null && biodataRepository.emailSama(biodataModel.getEmail())) {
			cek = true;
			message = "Email sudah terdaftar!";
		}

		if (biodataModel.getPhoneNumber1() != null && biodataRepository.noSama(biodataModel.getPhoneNumber1())) {
			cek = true;
			message = "Nomor HP sudah terdaftar!";
			
		}
		if (biodataModel.getIdentityTypeId() != null && biodataModel.getIdentityNo() != null
				&& biodataRepository.identitasSama(biodataModel.getIdentityTypeId(), biodataModel.getIdentityNo())) {
			cek = true;
			message = "No Identitas sudah terdaftar!";
		}

		if (cek == false) {
			this.biodataRepository.save(biodataModel);
			message = "Data berhasil disimpan";
		} else {

		}
		return message;
	}

	public List<BiodataModel> read() {

		return this.biodataRepository.findAll();
	}

	public BiodataModel searchIdBiodata(Long id) {
		return this.biodataRepository.searchIdBiodata(id);
	}
	
	public BiodataModel searchAddrBookId(Long addrbookid) {
		return this.biodataRepository.searchAddrBookId(addrbookid);
	}
	
	public List<BiodataModel> searchNamaPelamar( String fullname) {
		return this.biodataRepository.searchNamaPelamar(fullname);
	}
	
	public List<BiodataModel> ascPelamar() {
		return this.biodataRepository.ascPelamar();
	}
	
	public List<BiodataModel> descPelamar() {
		return this.biodataRepository.descPelamar();
	}
	public void update(BiodataModel biodataModel){
		this.biodataRepository.save(biodataModel);
		
	}
	
	public List<BiodataModel> searchNotDelete() {
		return this.biodataRepository.searchNotDelete();
	}	
	
	public List<String> search(String keyword) {
		return crud.search(keyword);
	}
	
}
