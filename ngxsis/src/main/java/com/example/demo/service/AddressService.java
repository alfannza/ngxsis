package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.AddressModel;

import com.example.demo.repository.AddressRepository;


@Service
@Transactional
public class AddressService {
	@Autowired
	private AddressRepository addressRepository;
	
	public void create(AddressModel addressModel) {
		this.addressRepository.save(addressModel);
	}

	public List<AddressModel> read() {

		return this.addressRepository.findAll();
	}

	public AddressModel searchIdBiodata(Long id) {
		// TODO Auto-generated method stub
		return this.addressRepository.searchIdBiodata(id);
	}
	public void update(AddressModel addressModel){
		this.addressRepository.save(addressModel);
		
	}
}
