package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.RiwayatProyekModel;
import com.example.demo.repository.RiwayatProyekRepository;

@Service
@Transactional
public class RiwayatProyekService {

	@Autowired
	private RiwayatProyekRepository riwayatProyekRepository;
	
	public void create(RiwayatProyekModel riwayatProyekModel) {
		this.riwayatProyekRepository.save(riwayatProyekModel);
	}
	
	public Long searchMaxId() {
		return this.riwayatProyekRepository.searchMaxId();
	}
	
	public List<RiwayatProyekModel> read(){
		return this.riwayatProyekRepository.findAll();
	}
	
	public RiwayatProyekModel searchIdProyek(Long id) {
		return this.riwayatProyekRepository.searchIdProyek(id);
	}
	
	public RiwayatProyekModel searchId(String id) {
		return this.riwayatProyekRepository.searchId(id);
	}
	
	public List<RiwayatProyekModel> searchNotDelete(){
		return this.riwayatProyekRepository.searchNotDelete();
	}
	
	public void update(RiwayatProyekModel riwayatProyekModel) {
		this.riwayatProyekRepository.save(riwayatProyekModel);
	}
	
	public void delete(RiwayatProyekModel riwayatProyekModel) {
		this.riwayatProyekRepository.delete(riwayatProyekModel);
	}
	
	public List<RiwayatProyekModel> searchByPekerjaanId(Long id){
		return this.riwayatProyekRepository.searchByPekerjaanId(id);
	}
}
