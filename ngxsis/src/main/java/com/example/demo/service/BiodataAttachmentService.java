package com.example.demo.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.BiodataAttachmentModel;
import com.example.demo.repository.BiodataAttachmentRepository;

@Service
@Transactional
public class BiodataAttachmentService {
	
	@Autowired
	BiodataAttachmentRepository biodataAttachmentRepository;

	public BiodataAttachmentModel searchByBiodataId(Long biodataId) {
		return this.biodataAttachmentRepository.searchByBiodataId(biodataId);
	}
	
}
