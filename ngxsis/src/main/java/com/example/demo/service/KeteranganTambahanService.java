package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.KeteranganTambahanModel;
import com.example.demo.repository.KeteranganTambahanRepository;

@Service
@Transactional
public class KeteranganTambahanService {
	@Autowired
	private KeteranganTambahanRepository keteranganTambahanRepository;
	
	public List<KeteranganTambahanModel> searchKeteranganByBiodataId(Long biodataId) {
		return (List<KeteranganTambahanModel>) this.keteranganTambahanRepository.getAdditionalInfo(biodataId);
	}
}
