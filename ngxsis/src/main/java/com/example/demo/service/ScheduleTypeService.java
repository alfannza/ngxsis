package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.ScheduleTypeModel;
import com.example.demo.repository.ScheduleTypeRepository;

@Service
@Transactional
public class ScheduleTypeService {

	@Autowired
	private ScheduleTypeRepository scheduleTypeRepository;
	
	public ScheduleTypeModel searchById(Long id) {
		return this.scheduleTypeRepository.searchById(id);
	}
	
	public void save(ScheduleTypeModel scheduleTypeModel) {
		this.scheduleTypeRepository.save(scheduleTypeModel);
	}
	
	public void update(ScheduleTypeModel scheduleTypeModel) {
		this.scheduleTypeRepository.save(scheduleTypeModel);
	}
	
	public List<ScheduleTypeModel> findIsNotDelete(){
		return this.scheduleTypeRepository.findIsNotDelete();
	}
	
	public List<ScheduleTypeModel> findAscending(){
		return this.scheduleTypeRepository.findAscending();
	}
	
	public List<ScheduleTypeModel> findDescending(){
		return this.scheduleTypeRepository.findDescending();
	}
	
	public List<ScheduleTypeModel> searchLikeName(String name){
		return this.scheduleTypeRepository.searchLikeName(name);
	}
	
}
