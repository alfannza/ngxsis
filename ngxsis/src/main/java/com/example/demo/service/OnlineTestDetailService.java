package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.OnlineTestDetailModel;
import com.example.demo.repository.OnlineTestDetailRepository;

@Service
@Transactional
public class OnlineTestDetailService {

	@Autowired
	private OnlineTestDetailRepository onlineTestDetailRepository;
	
	public void create(OnlineTestDetailModel onlineTestDetailModel) {
		this.onlineTestDetailRepository.save(onlineTestDetailModel);
	}
	
	public Long searchMaxId() {
		return this.onlineTestDetailRepository.searchMaxId();
	}
	
	public List<OnlineTestDetailModel> read(){
		return this.onlineTestDetailRepository.findAll();
	}
	
	public OnlineTestDetailModel searchIdOnlineTest(Long idOnlineTest) {
		return this.onlineTestDetailRepository.searchIdOnlineTest(idOnlineTest);
	}
	
	public List<OnlineTestDetailModel> findByOnlineTestIdAndIsNotDelete(Long onlineTestId){
		return this.onlineTestDetailRepository.findByOnlineTestIdAndIsNotDelete(onlineTestId);
	}
	
	public OnlineTestDetailModel searchId(String id) {
		return this.onlineTestDetailRepository.searchId(id);
	}
	
	public List<OnlineTestDetailModel> searchNotDelete(){
		return this.onlineTestDetailRepository.searchNotDelete();
	}
	
	public void update(OnlineTestDetailModel onlineTestDetailModel) {
		this.onlineTestDetailRepository.save(onlineTestDetailModel);
	}
	
	public void delete(OnlineTestDetailModel onlineTestDetailModel) {
		this.onlineTestDetailRepository.delete(onlineTestDetailModel);
	}
	
	public Integer findMaxOrderByOnlineTestId(Long onlineTestId){
		return this.onlineTestDetailRepository.findMaxOrderByOnlineTestId(onlineTestId);
	}
	
	public void updateTestOrder(Long onlineTestId, Integer testOrder) {
		this.onlineTestDetailRepository.updateTestOrder(onlineTestId, testOrder);
	};
}
