package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.example.demo.model.IdentityTypeModel;
import com.example.demo.repository.IdentityTypeRepository;

@Service
@Transactional
public class IdentityTypeService {

	@Autowired
	IdentityTypeRepository identityTypeRepository;
	
	public List<IdentityTypeModel> read() {
		return this.identityTypeRepository.findAll();
			}
	public IdentityTypeModel searchIdIdentityType( Long id) {
		return this.identityTypeRepository.searchIdIdentityType(id);
	}
}
