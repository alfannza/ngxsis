package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.RiwayatPekerjaanModel;
import com.example.demo.repository.RiwayatPekerjaanRepository;

@Service
@Transactional
public class RiwayatPekerjaanService {

	@Autowired
	RiwayatPekerjaanRepository riwayatPekerjaanRepository;

	public RiwayatPekerjaanModel searchIdPekerjaan(Long id) {
		return this.riwayatPekerjaanRepository.searchIdPekerjaan(id);
	}
	
	public List<RiwayatPekerjaanModel> searchPekerjaanByBiodataId(Long id) {
		return this.riwayatPekerjaanRepository.searchPekerjaanByBiodataId(id);
	}
	
	public List<RiwayatPekerjaanModel> searchNotDelete(){
		return this.riwayatPekerjaanRepository.searchNotDelete();
	}
	
	public List<RiwayatPekerjaanModel> read(){
		return this.riwayatPekerjaanRepository.findAll();
	}
	
	public void create(RiwayatPekerjaanModel rpm) {
		this.riwayatPekerjaanRepository.save(rpm);
	}
	
	public void update(RiwayatPekerjaanModel rpm) {
		this.riwayatPekerjaanRepository.save(rpm);
	}
	
	public void delete(RiwayatPekerjaanModel rpm) {
		this.riwayatPekerjaanRepository.delete(rpm);
	}

	public List<RiwayatPekerjaanModel> findByBiodataAndIsNotDelete(Long id) {
		return this.riwayatPekerjaanRepository.findByBiodataAndIsNotDelete(id);
	}
}
