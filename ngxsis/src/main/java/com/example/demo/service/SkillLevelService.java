package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.SkillLevelModel;
import com.example.demo.repository.SkillLevelRepository;

@Service
@Transactional
public class SkillLevelService {
	
	@Autowired
	SkillLevelRepository skillLevelRepository;

	public List<SkillLevelModel> read() {
		return this.skillLevelRepository.findAll();
	}
	
	public List<SkillLevelModel> searchIdSkillLevel(){
		return this.skillLevelRepository.searchSkillLevelIsNotDelete();
	}
}
