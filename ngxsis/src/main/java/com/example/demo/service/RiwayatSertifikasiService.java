package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.RiwayatSertifikasiModel;
import com.example.demo.repository.RiwayatSertifikasiRepository;

@Service
@Transactional
public class RiwayatSertifikasiService {

	@Autowired
	RiwayatSertifikasiRepository riwayatSertifikasiRepository;
	
	public List<RiwayatSertifikasiModel>  searchSertifikasiByBiodataId(Long biodataId) {
		return this.riwayatSertifikasiRepository. searchSertifikasiByBiodataId(biodataId);
	}
	
	public List<RiwayatSertifikasiModel>  findByBiodataAndIsNotDelete(Long biodataId) {
		return this.riwayatSertifikasiRepository.findByBiodataAndIsNotDelete(biodataId);
	}
	
	public RiwayatSertifikasiModel searchIdRiwayatSertifikasi(Long id) {
		return this.riwayatSertifikasiRepository.searchIdRiwayatSertifikasi(id);
	}
	
	public void save(RiwayatSertifikasiModel riwayatSertifikasiModel) {
		this.riwayatSertifikasiRepository.save(riwayatSertifikasiModel);
	}
	
	public void update(RiwayatSertifikasiModel riwayatSertifikasiModel) {
		this.riwayatSertifikasiRepository.save(riwayatSertifikasiModel);
	}
}
