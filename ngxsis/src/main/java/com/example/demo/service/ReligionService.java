package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.ReligionModel;
import com.example.demo.repository.ReligionRepository;

@Service
@Transactional
public class ReligionService {
	
	@Autowired
	ReligionRepository religionRepository;

	public List<ReligionModel> read() {

		return this.religionRepository.findAll();
	}
	public ReligionModel searchIdReligion(Long id) {
		return this.religionRepository.searchIdReligion(id);
	}
}
