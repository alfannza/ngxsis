package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.PeReferensiModel;
import com.example.demo.repository.PeReferensiRepository;

@Service
@Transactional
public class PeReferensiService {
	
	@Autowired
	private PeReferensiRepository peReferensiRepository;
	
	public List<PeReferensiModel> searchPeReferensiByBiodataId(Long biodataId) {
		return this.peReferensiRepository.searchPeRefrensiByBiodataId(biodataId);
	}
	
	public PeReferensiModel searchOnePeReferensiByBiodataId(Long id) {
		return this.peReferensiRepository.searchPeRefrensiById(id);
	}
	
	public List<PeReferensiModel> findUndeleted(){
		return this.peReferensiRepository.findIsNotDelete();
	}
	public List<PeReferensiModel> getReference(Long id){
		return this.peReferensiRepository.getReference(id);
	}
	
	public void save(PeReferensiModel preferensiModel) {
		this.peReferensiRepository.save(preferensiModel);
	}
} 
