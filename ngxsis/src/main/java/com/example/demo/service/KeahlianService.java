package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.KeahlianModel;
import com.example.demo.repository.KeahlianRepository;

@Service
@Transactional
public class KeahlianService {
	
	@Autowired
	private KeahlianRepository keahlianRepository;
	
	public List<KeahlianModel> findByBiodataAndIsNotDelete(Long biodataId) {
		return this.keahlianRepository.findByBiodataAndIsNotDelete(biodataId);
	} // pake
	
	public KeahlianModel searchIdKeahlian(Long id) {
		return this.keahlianRepository.searchIdKeahlian(id);
	} // pake
	
	public List<KeahlianModel> searchKeahlianByBiodataId(Long biodataId, Long skillLevelId){
		return this.keahlianRepository.searchKeahlianByBiodataId(biodataId, skillLevelId);
	}
	
	public void save(KeahlianModel keahlianModel) {
		this.keahlianRepository.save(keahlianModel);
	} // pake
	
	public void update(KeahlianModel keahlianModel) {
		this.keahlianRepository.save(keahlianModel);
	} // pake
}
