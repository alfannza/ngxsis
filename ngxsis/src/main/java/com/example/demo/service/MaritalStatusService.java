package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.MaritalStatusModel;
import com.example.demo.repository.MaritalStatusRepository;

@Service
@Transactional
public class MaritalStatusService {
	
	@Autowired
	MaritalStatusRepository maritalStatusRepository;

	public List<MaritalStatusModel> read() {

		return this.maritalStatusRepository.findAll();
	}
	public MaritalStatusModel searchIdMaritalStatus(Long id) {
		return this.maritalStatusRepository.searchIdMaritalStatus(id);
	}
}
