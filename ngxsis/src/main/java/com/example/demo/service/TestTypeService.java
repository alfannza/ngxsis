package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.TestTypeModel;
import com.example.demo.repository.TestTypeRepository;

@Service
@Transactional
public class TestTypeService {

	@Autowired
	private TestTypeRepository testTypeRepository;
	
	
	public List<TestTypeModel> read(){
		return this.testTypeRepository.findAll();
	}
	
	public List<TestTypeModel> searchNotDelete(){
		return this.testTypeRepository.searchNotDelete();
	}
	
}
