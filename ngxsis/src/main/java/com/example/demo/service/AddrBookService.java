package com.example.demo.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.model.AddrBookModel;
import com.example.demo.repository.AddrBookRepository;

@Service
@Transactional
public class AddrBookService {

	@Autowired
	AddrBookRepository addrBookRepository;

	public AddrBookModel loginByUsernamer(String email, String abuid, String abpwd) {
		return this.addrBookRepository.searchUsername(email, abuid, abpwd);
	}
	
	public AddrBookModel cekUser(String email, String abuid) {
		return this.addrBookRepository.cekUsername(email, abuid);
	}
	
	public AddrBookModel findById(Long id) {
		return this.addrBookRepository.findByIdAddrbook(id);
	}
	
	public void update(AddrBookModel addrBookModel) {
		this.addrBookRepository.save(addrBookModel);
	}
	
	public Long getSize() {
		return this.addrBookRepository.getSize();
	}
	
	
}
