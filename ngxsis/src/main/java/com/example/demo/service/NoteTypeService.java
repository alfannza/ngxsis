package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.NoteTypeModel;
import com.example.demo.repository.NoteTypeRepository;

@Service
@Transactional
public class NoteTypeService {

	@Autowired
	private NoteTypeRepository noteTypeRepository;
	
	
	public List<NoteTypeModel> read(){
		return this.noteTypeRepository.findAll();
	}
	
	
}
