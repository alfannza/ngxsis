package com.example.demo.service;

import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.MenuTreeModel;
import com.example.demo.repository.MenuTreeRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class MenuTreeService {

	@Autowired
	private MenuTreeRepository menuTreeRepository;
	
	public List<MenuTreeModel> read(){
		return this.menuTreeRepository.findIsNotDelete();
	}
	
}
