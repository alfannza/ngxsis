package com.example.demo.service;

import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.EducationLevel;
import com.example.demo.repository.EducationLevelRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class EducationLevelService {

	@Autowired
	private EducationLevelRepository educationLevelRepository;
	
	public List<EducationLevel> read(){
		return this.educationLevelRepository.searchIsNotDelete();
	}
	
	public void create(EducationLevel educationLevel) {
		this.educationLevelRepository.save(educationLevel);
	}
	
	public void update(EducationLevel educationLevel) {
		this.educationLevelRepository.save(educationLevel);
	}
	
	public EducationLevel serachById (Long id) {
		return this.educationLevelRepository.searchById(id);
	}
	
}
