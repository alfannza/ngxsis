package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.RiwayatPendidikanModel;
import com.example.demo.repository.RiwayatPendidikanRepository;

@Service
@Transactional
public class RiwayatPendidikanService {

	@Autowired
	private RiwayatPendidikanRepository riwayatPendidikanRepository;

	public List<RiwayatPendidikanModel> findByBiodataAndIsNotDelete(Long biodataId) {
		return this.riwayatPendidikanRepository.findByBiodataAndIsNotDelete(biodataId);
	}
	
	public RiwayatPendidikanModel searchIdRiwayatPendidikan(Long id) {
		return this.riwayatPendidikanRepository.searchIdRiwayatPendidikan(id);
	}

	public List<RiwayatPendidikanModel> searchPendidikanByBiodataId(Long biodataId) {
		return this.riwayatPendidikanRepository.searchPendidikanByBiodataId(biodataId);
	}
	
	public List<RiwayatPendidikanModel> searchIdPendidikanTerahir(Long biodataId) {
		return this.riwayatPendidikanRepository.searchIdPendidikanTerahir(biodataId);
	}

	public void save(RiwayatPendidikanModel pendidikanModel) {
		this.riwayatPendidikanRepository.save(pendidikanModel);
	}
	
	public void update(RiwayatPendidikanModel riwayatPendidikanModel) {
		this.riwayatPendidikanRepository.save(riwayatPendidikanModel);
	}
	
}
