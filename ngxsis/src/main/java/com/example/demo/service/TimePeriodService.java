package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.TimePeriodModel;
import com.example.demo.repository.TimePeriodRepository;

@Service
@Transactional
public class TimePeriodService {

	@Autowired
	private TimePeriodRepository timePeriodRepository;
	
	public List<TimePeriodModel> read(){
		return this.timePeriodRepository.findAll();
	}
}
