package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.UndanganDetail;
import com.example.demo.repository.UndanganDetailRepository;

@Service
@Transactional
public class UndanganDetailService {

	@Autowired
	private UndanganDetailRepository undanganDetailRepository;
	
	public void save(UndanganDetail undanganDetail) {
		this.undanganDetailRepository.save(undanganDetail);
	}
	
	public void update(UndanganDetail undanganDetail) {
		this.undanganDetailRepository.save(undanganDetail);
	}
	
	public List<UndanganDetail> findIsNotDelete(){
		return this.undanganDetailRepository.findIsNotDelete();
	}
	
	public List<UndanganDetail> findAscendingBiodata(){
		return this.undanganDetailRepository.findAscendingBiodata();
	}
	
	public List<UndanganDetail> findDescendingBiodata(){
		return this.undanganDetailRepository.findDescendingBiodata();
	}
	
	public UndanganDetail searchById(Long id) {
		return this.undanganDetailRepository.searchById(id);
	}
	
	public List<UndanganDetail> searchLikeNamePelamar(String name){
		return this.undanganDetailRepository.searchLikeNamePelamar(name);
	}
}
