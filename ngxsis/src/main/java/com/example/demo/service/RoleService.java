package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.RoleModel;
import com.example.demo.repository.RoleRepository;

@Service
@Transactional
public class RoleService {

	@Autowired
	private RoleRepository roleRepository;
	
	public List<RoleModel> read(){
		return this.roleRepository.findIsNotDelete();
	}
	
	public void create(RoleModel roleModel) {
		this.roleRepository.save(roleModel);
	}
	
	public void update(RoleModel roleModel) {
		this.roleRepository.save(roleModel);
	}
	
	public RoleModel findByName(String name) {
		return this.roleRepository.findByName(name);
	}
	
	public RoleModel findByCode(String code) {
		return this.roleRepository.findByCode(code);
	}
	
	public List<RoleModel> findAscending(){
		return this.roleRepository.findAscending();
	}
	
	public List<RoleModel> findDescending(){
		return this.roleRepository.findDescending();
	}
	
	public List<RoleModel> searchLikeName(String name){
		return this.roleRepository.searchLikeName(name);
	}
}
