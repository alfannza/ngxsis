package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.UserRoleModel;
import com.example.demo.repository.UserRoleRepository;

@Service
@Transactional
public class UserRoleService {

	@Autowired
	private UserRoleRepository userRoleRepository;
	
	public List<UserRoleModel> findByAddrBookId(Long id) {
		return this.userRoleRepository.findByAddrBookId(id);
	}
	
	public UserRoleModel findByUserRoleAndAddrBook(Long addrBookId, Long roleId) {
		return this.userRoleRepository.findByUserRoleAndAddrBook(addrBookId, roleId);
	}
	
}
