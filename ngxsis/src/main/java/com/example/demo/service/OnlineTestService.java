package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.OnlineTestModel;
import com.example.demo.repository.OnlineTestRepository;

@Service
@Transactional
public class OnlineTestService {

	@Autowired
	private OnlineTestRepository onlineTestRepository;
	
	public void create(OnlineTestModel catatanModel) {
		this.onlineTestRepository.save(catatanModel);
	}
	
	public Long searchMaxId() {
		return this.onlineTestRepository.searchMaxId();
	}
	
	public List<OnlineTestModel> read(){
		return this.onlineTestRepository.findAll();
	}
	
	public OnlineTestModel searchIdOnlineTest(Long idOnlineTest) {
		return this.onlineTestRepository.searchIdOnlineTest(idOnlineTest);
	}
	
	public List<OnlineTestModel> findByBiodataAndIsNotDelete(Long id){
		return this.onlineTestRepository.findByBiodataAndIsNotDelete(id);
	}
	
	public List<OnlineTestModel> findByBiodata(Long id){
		return this.onlineTestRepository.findByBiodata(id);
	}
	
	public OnlineTestModel searchId(String id) {
		return this.onlineTestRepository.searchId(id);
	}
	
	public List<OnlineTestModel> searchNotDelete(){
		return this.onlineTestRepository.searchNotDelete();
	}
	
	public void update(OnlineTestModel onlineTestModel) {
		this.onlineTestRepository.save(onlineTestModel);
	}
	
	public void delete(OnlineTestModel onlineTestModel) {
		this.onlineTestRepository.delete(onlineTestModel);
	}
	
	public Integer searchMaxPeriodByBiodataId(Long biodataId) {
		return this.onlineTestRepository.searchMaxPeriodByBiodataId(biodataId);
	}
	
	public String searchLastPRD() {
		return this.onlineTestRepository.searchLastPRD();
	}
	
	public Integer getSize() {
		return this.onlineTestRepository.getSize();
	}
}
