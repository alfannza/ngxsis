package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.CatatanModel;
import com.example.demo.repository.CatatanRepository;

@Service
@Transactional
public class CatatanService {

	@Autowired
	private CatatanRepository catatanRepository;
	
	public void create(CatatanModel catatanModel) {
		this.catatanRepository.save(catatanModel);
	}
	
	public Long searchMaxId() {
		return this.catatanRepository.searchMaxId();
	}
	
	public List<CatatanModel> read(){
		return this.catatanRepository.findAll();
	}
	
	public CatatanModel searchIdCatatan(Long idCatatan) {
		return this.catatanRepository.searchIdCatatan(idCatatan);
	}
	
	public List<CatatanModel> findByBiodataAndIsNotDelete(Long id){
		return this.catatanRepository.findByBiodataAndIsNotDelete(id);
	}
	
	public CatatanModel searchId(String id) {
		return this.catatanRepository.searchId(id);
	}
	
	public List<CatatanModel> searchNotDelete(){
		return this.catatanRepository.searchNotDelete();
	}
	
	public void update(CatatanModel catatanModel) {
		this.catatanRepository.save(catatanModel);
	}
	
	public void delete(CatatanModel fakultasModel) {
		this.catatanRepository.delete(fakultasModel);
	}
}
