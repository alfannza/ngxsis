package com.example.demo.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.Undangan;
import com.example.demo.repository.UndanganRepository;

@Service
@Transactional
public class UndanganService {

	@Autowired
	private UndanganRepository undanganRepository;

	public void save(Undangan undangan) {
		this.undanganRepository.save(undangan);
	}

	public void update(Undangan undangan) {
		this.undanganRepository.save(undangan);
	}
	
	public Undangan searchById(Long id) {
		return this.undanganRepository.searchById(id);
	}
	
	public List<Undangan> findIsNotDelete() {
		return this.undanganRepository.findIsNotDelete();
	}
	
	public List<Undangan> findAscending() {
		return this.undanganRepository.findAscending();
	}
	
	public List<Undangan> findDescending(){
		return this.undanganRepository.findDescending();
	}
	
	public List<Undangan> findByScheduleTypeId(Long id){
		return this.undanganRepository.findByScheduleTypeId(id);
	}

	public Undangan findByCreatedOn(Date createdOn) {
		return this.findByCreatedOn(createdOn);
	}
	
	public Undangan findByModifiedOn(Date modifiedOn) {
		return this.findByModifiedOn(modifiedOn);
	}


}
