package com.example.demo;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.AddrBookModel;
import com.example.demo.model.BiodataModel;
import com.example.demo.model.CompanyModel;
import com.example.demo.model.MenuTreeModel;
import com.example.demo.model.UserRoleModel;
import com.example.demo.service.BiodataService;
import com.example.demo.service.CompanyService;
import com.example.demo.service.MenuTreeService;
import com.example.demo.service.UserRoleService;

@Controller
@SpringBootApplication
public class NgxsisApplication {

	

	public static void main(String[] args) {
		SpringApplication.run(NgxsisApplication.class, args);
	}

}
