package com.example.demo.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.BiodataModel;
import com.example.demo.model.OrganisasiModel;
import com.example.demo.model.UserRoleModel;
import com.example.demo.service.BiodataService;
import com.example.demo.service.OrganisasiService;

@Controller
@RequestMapping("/organisasi")
public class OrganisasiController {

	@Autowired
	OrganisasiService organisasiService;
	
	@Autowired
	BiodataService biodataService;
	
	@RequestMapping("/index")
	public String doIndex() {
		return "/organisasi/index";
	}
	
	@RequestMapping("/home")
	public String doHome(HttpServletRequest req, Model model) {
		Long id = Long.parseLong(req.getParameter("id"));
		BiodataModel bm = this.biodataService.searchIdBiodata(id);
		model.addAttribute("biodataModel", bm);
		return "/organisasi/home";
	}
	
	@RequestMapping("/tambah")
	public String doTambah(HttpServletRequest req, Model model) {
		Long idBiodata = Long.parseLong(req.getParameter("id"));
		BiodataModel bm = this.biodataService.searchIdBiodata(idBiodata);
		model.addAttribute("biodataModel", bm);
		return "/organisasi/add";
	}
	
	@RequestMapping("/create") // belum selesai, createdBy dan biodataId masih manual
	public String doCreate(HttpServletRequest req) {
		Long idBiodata = Long.parseLong(req.getParameter("id"));
		String name = req.getParameter("name");
		String position = req.getParameter("position");
		String responsibility = req.getParameter("responsibility");
		String notes = req.getParameter("notes");
		String entryYear = req.getParameter("entryYear");
		String exitYear = req.getParameter("exitYear");
		
		HttpSession session = req.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date createdOn = ts;
		
		OrganisasiModel om = new OrganisasiModel();
		om.setBiodataId(biodataModel.getId());  // belum
		om.setName(name);
		om.setPosition(position);
		om.setResponsibility(responsibility);
		om.setNotes(notes);
		om.setEntryYear(entryYear);
		om.setExitYear(exitYear);
		om.setCreatedOn(createdOn);
		om.setCreatedBy(userRoleModel.getAddrbookld());
		om.setIsDelete(false);
		this.organisasiService.create(om);
		return "/organisasi/list"; //list
	}

	@RequestMapping("/data")
	public String doData(HttpServletRequest req, Model model) {
		Long idBiodata = Long.parseLong(req.getParameter("id"));
		BiodataModel bm = this.biodataService.searchIdBiodata(idBiodata);
		List<OrganisasiModel> omList = new ArrayList<OrganisasiModel>();
		omList = this.organisasiService.findByBiodataAndIsNotDelete(bm.getId()); //searchNotDelete
		model.addAttribute("organisasiModelList", omList);
		//this.doListNoteType(model);
		return "/organisasi/list";
	}
	
	public void doList(Model model) {
		List<OrganisasiModel> omList = new ArrayList<OrganisasiModel>();
		omList = this.organisasiService.read(); //searchNotDelete
		model.addAttribute("organisasiModelList", omList);
	}
		
	@RequestMapping("/detail")
	public String doDetail(HttpServletRequest req, Model model) {
		Long id = Long.parseLong(req.getParameter("id"));
		OrganisasiModel om = new OrganisasiModel();
		om = this.organisasiService.searchIdOrg(id);
		model.addAttribute("organisasiModel", om);
		return "/organisasi/detail";
	}
	
	@RequestMapping("/ubah")
	public String doUbah(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		//String id = request.getParameter("id");
		OrganisasiModel om = new OrganisasiModel();
		om = this.organisasiService.searchIdOrg(id);
		model.addAttribute("organisasiModel", om);
		return "/organisasi/edit";
	}
	
	@RequestMapping("/update") // belum selesai, modifiedBy masih manual
	public String doUpdate(HttpServletRequest req, Model model) {
		Long id = Long.parseLong(req.getParameter("id"));
		String name = req.getParameter("name");
		String position = req.getParameter("position");
		String responsibility = req.getParameter("responsibility");
		String notes = req.getParameter("notes");
		String entryYear = req.getParameter("entryYear");
		String exitYear = req.getParameter("exitYear");
		
		HttpSession session = req.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date modifiedOn = ts;
		
		OrganisasiModel om = new OrganisasiModel();
		om = this.organisasiService.searchIdOrg(id);
		om.setName(name);
		om.setPosition(position);
		om.setResponsibility(responsibility);
		om.setNotes(notes);
		om.setEntryYear(entryYear);
		om.setExitYear(exitYear);
		om.setModifiedOn(modifiedOn);
		om.setModifiedBy(userRoleModel.getAddrbookld());
		this.organisasiService.update(om);
		return "/organisasi/list";
	}
	
	@RequestMapping("/hapus")
	public String doHapus(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		OrganisasiModel om = new OrganisasiModel();
		om = this.organisasiService.searchIdOrg(id);
		model.addAttribute("organisasiModel", om);
		return "/organisasi/delete";
	}

	@RequestMapping("/confirmdelete")
	public String doConfirmDelete(HttpServletRequest req, Model model) {
		Long id = Long.parseLong(req.getParameter("id"));
		
		HttpSession session = req.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date deletedOn = ts;
		
		OrganisasiModel om = new OrganisasiModel();
		om = this.organisasiService.searchIdOrg(id);
		om.setIsDelete(true);
		om.setDeletedOn(deletedOn);
		om.setDeletedBy(userRoleModel.getAddrbookld());
		
		this.organisasiService.update(om);
		return "/organisasi/list";
	}
	
	
}
