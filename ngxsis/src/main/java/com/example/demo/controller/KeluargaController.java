package com.example.demo.controller;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.BiodataModel;
import com.example.demo.model.EducationLevel;
import com.example.demo.model.FamilyRelationModel;
import com.example.demo.model.FamilyTreeTypeModel;
import com.example.demo.model.KeluargaModel;
import com.example.demo.model.UserRoleModel;
import com.example.demo.repository.FamilyRelationRepository;
import com.example.demo.repository.FamilyTreeTypeRepository;
import com.example.demo.repository.KeluargaRepository;
import com.example.demo.service.BiodataService;
import com.example.demo.service.EducationLevelService;
import com.example.demo.service.FamilyTreeTypeService;

@Controller
@RequestMapping("/keluarga")
public class KeluargaController {

	@Autowired
	private KeluargaRepository keluargaRepository;

	@Autowired
	private FamilyTreeTypeRepository familyTree;

	@Autowired
	private EducationLevelService educationLevelService;

	@Autowired
	private FamilyTreeTypeService familyService;
	
	@Autowired
	private FamilyRelationRepository familyRelation;

	@Autowired
	private BiodataService biodataService;

	@RequestMapping("/home")
	public String doHome(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		System.out.println(id);
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(id);
		model.addAttribute("biodataModel", biodataModel);
		List<KeluargaModel> keluarga = new ArrayList<KeluargaModel>();
		keluarga = this.keluargaRepository.searchKeluargaByBiodataId(biodataModel.getId());
		model.addAttribute("keluargaModel", keluarga);
		return "keluarga/fam-index";
	}

	@RequestMapping("/data")
	public String populate(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(id);
		model.addAttribute("biodataModel", biodataModel);
		List<KeluargaModel> keluarga = new ArrayList<KeluargaModel>();
		keluarga = this.keluargaRepository.searchKeluargaByBiodataId(biodataModel.getId());
		model.addAttribute("keluargaModel", keluarga);
		return "keluarga/fam-list";
	}

	@RequestMapping("/add")
	public String add(HttpServletRequest request, Model model) {
		Long idBiodata = Long.parseLong(request.getParameter("id"));
		// System.out.println(idBiodata);
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);
		this.doType(model);
		this.doJenjang(model);
		List<FamilyRelationModel> familyRelationModel = new ArrayList<FamilyRelationModel>();
		familyRelationModel = this.familyRelation.findAll();
		model.addAttribute("familyRelationModel", familyRelationModel);
		model.addAttribute("biodataModel", biodataModel);
		return "/keluarga/fam-add";
	}

	public void doJenjang(Model model) {

		List<EducationLevel> jenjangPendidikan = new ArrayList<EducationLevel>();
		jenjangPendidikan = this.educationLevelService.read();
		model.addAttribute("jenjangPendidikan", jenjangPendidikan);
	}

	public void doType(Model model) {
		List<FamilyTreeTypeModel> familyTreeTypeModel = new ArrayList<FamilyTreeTypeModel>();
		familyTreeTypeModel = this.familyService.read();
		model.addAttribute("familyTreeTypeModel", familyTreeTypeModel);
	}
	
	@RequestMapping("/getRelation")
	public String doRelate(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		List<FamilyRelationModel> familyRelationModel = new ArrayList<FamilyRelationModel>();
		familyRelationModel = this.familyRelation.searchRelationList(id);
		model.addAttribute("familyRelationModel", familyRelationModel);
		return "/keluarga/dropdown-relation-list";
	}

	@RequestMapping("/create")
	public String create(HttpServletRequest request) throws ParseException {
		Long id = Long.parseLong(request.getParameter("id"));
		System.out.println(id);
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(id);
		Date createdOn = new Timestamp(System.currentTimeMillis());
		String date = request.getParameter("datePicker");
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		Date dob = format.parse(date);
		
		// System.out.println(biodataModel.getId());
		
		KeluargaModel keluarga = new KeluargaModel();
		keluarga.setBiodataId(biodataModel.getId());
		keluarga.setFamilyTreeTypeId(Long.parseLong(request.getParameter(
		"familyTreeTypeId")));
		keluarga.setFamilyRelationId(Long.parseLong(request.getParameter(
		"familyRelationId"))); keluarga.setName(request.getParameter("name"));
		keluarga.setGender(Boolean.parseBoolean(request.getParameter("gender")));
		keluarga.setDob(dob);
		keluarga.setEducationLevelId(Long.parseLong(request.getParameter("educationLevel")));
		keluarga.setJob(request.getParameter("job"));
		keluarga.setNotes(request.getParameter("notes"));
		keluarga.setCreatedBy(userRoleModel.getAddrbookld());
		keluarga.setCreatedOn(createdOn);
		
		keluarga.setIsDelete(false);
		this.keluargaRepository.save(keluarga);
		return "keluarga/fam-list";
	}
	
	@RequestMapping("/ubah")
	public String doEdit(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		KeluargaModel keluargaModel = this.keluargaRepository.searchKeluargaById(id);
		this.doJenjang(model);
		model.addAttribute("keluargaModel", keluargaModel);
		
		return "keluarga/fam-edit";
	}
	
	@RequestMapping("/update")
	public String update(HttpServletRequest request, Model model) throws ParseException {
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Long id = Long.parseLong(request.getParameter("id"));
		Date modifiedOn = ts;
		String date = request.getParameter("datePicker");
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		Date dob = format.parse(date);
		KeluargaModel keluarga = this.keluargaRepository.searchKeluargaById(id);
		
		keluarga.setGender(Boolean.parseBoolean(request.getParameter("gender")));
		keluarga.setDob(dob);
		keluarga.setEducationLevelId(Long.parseLong(request.getParameter("educationLevel")));
		keluarga.setJob(request.getParameter("job"));
		keluarga.setNotes(request.getParameter("notes"));
		keluarga.setModifiedBy(userRoleModel.getAddrbookld());
		keluarga.setModifiedOn(modifiedOn);
		
		this.keluargaRepository.save(keluarga);
		return "keluarga/fam-list";
	}
	
	@RequestMapping("/hapus")
	public String hapus(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		System.out.println("id = "+id);
		KeluargaModel keluargaModel = this.keluargaRepository.searchKeluargaById(id);
		model.addAttribute("keluargaModel", keluargaModel);
		return "keluarga/fam-delete";
	}
	
	@RequestMapping("/delete")
	public String doHapus(HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		Date deletedOn = new Timestamp(System.currentTimeMillis());
		KeluargaModel keluarga = this.keluargaRepository.searchKeluargaById(id);
		System.out.println("id = "+id);
		keluarga.setIsDelete(true);
		keluarga.setDeletedBy(userRoleModel.getAddrbookld());
		keluarga.setDeletedOn(deletedOn);
		this.keluargaRepository.save(keluarga);
		return "keluarga/fam-list";
	}
	
	
}
