package com.example.demo.controller;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.configuration.SmtpMailSender;
import com.example.demo.model.AddrBookModel;
import com.example.demo.model.BiodataModel;
import com.example.demo.model.OnlineTestDetailModel;
import com.example.demo.model.OnlineTestModel;
import com.example.demo.model.TestTypeModel;
import com.example.demo.model.UserRoleModel;
import com.example.demo.service.AddrBookService;
import com.example.demo.service.BiodataService;
import com.example.demo.service.OnlineTestDetailService;
import com.example.demo.service.OnlineTestService;
import com.example.demo.service.TestTypeService;

@Controller
@RequestMapping("/aktivasi akun")
public class OnlineTestController {

	@Autowired
	OnlineTestService onlineTestService;

	@Autowired
	BiodataService biodataService;

	@Autowired
	AddrBookService addrBookService;

	@Autowired
	OnlineTestDetailService onlineTestDetailService;

	@Autowired
	TestTypeService testTypeService;

	@Autowired
	SmtpMailSender smtpMailSender;

	@RequestMapping("/home")
	public String doHome(HttpServletRequest req, Model model) {
		Long id = Long.parseLong(req.getParameter("id"));
		BiodataModel bm = this.biodataService.searchIdBiodata(id);
		model.addAttribute("biodataModel", bm);
		return "/aktivasi akun/home";
	}

	@RequestMapping("/add") // tambah
	public String doTambah(HttpServletRequest req, Model model) {
		Long idBiodata = Long.parseLong(req.getParameter("id"));
		BiodataModel bm = this.biodataService.searchIdBiodata(idBiodata);
		model.addAttribute("biodataModel", bm);
		return "/aktivasi akun/add";
	}

	@RequestMapping("/aktifkan") // tambah
	public String doTblAktifkan(HttpServletRequest req, Model model) {
		Long idBiodata = Long.parseLong(req.getParameter("id"));
		BiodataModel bm = this.biodataService.searchIdBiodata(idBiodata);
		model.addAttribute("biodataModel", bm);
		return "/aktivasi akun/activation";
	}

	@ResponseBody
	@RequestMapping("/cekLocked")
	public String cekLocked(@RequestParam Integer id, HttpServletRequest req, HttpServletResponse response,
			Model model) {
		Long idBiodata = Long.parseLong(req.getParameter("id"));
		BiodataModel bm = this.biodataService.searchIdBiodata(idBiodata);
		AddrBookModel abm = new AddrBookModel();
		if (bm.getAddrbookId() == null) {
			return "belum ada";
		} else {
			abm = this.addrBookService.findById(bm.getAddrBookModel().getId());
			// Boolean isLocked = abm.getIsLocked();
			if (abm.getIsDelete() == true) {
				return "dikunci";
			} else {
				return "tidak dikunci";
			}
		}
	}

	@ExceptionHandler(MessagingException.class)
	@ResponseBody
	@RequestMapping("/doAktifkan")
	public String doSimpanDanKirim(HttpServletRequest req, Model model) throws ParseException, MessagingException {
		try {
			Long idBiodata = Long.parseLong(req.getParameter("id"));
			String periodCode = this.maxPeriodIncrement();
			String tanggalTesStr = req.getParameter("tglTes");
			String tanggalAkhirTesStr = req.getParameter("tglAkhirTes");
			String userAccess = req.getParameter("userAccess");
			String jenisTes = req.getParameter("arrJenisTes");
			List<String> jenisTesList = Arrays.asList(jenisTes.split("\\s*,\\s*")); // [1, 3, 1, 2]
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date tglTes = format.parse(tanggalTesStr);
			Date tglAkhirTes = format.parse(tanggalAkhirTesStr);

			HttpSession session = req.getSession(false);
			UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
			BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			Date createdOn = ts;

			OnlineTestModel otm = new OnlineTestModel();
			otm.setBiodataId(biodataModel.getId());
			otm.setPeriodCode(periodCode);
			// Integer rowCount = this.onlineTestService.getSize();
			// "TR" + String.format("%05d", rowCount+1);
			String generatedID = periodCode;
			AddrBookModel addrBookModel = new AddrBookModel();
			if (this.isThisBiodataEverActivating(idBiodata) == false) {
				// System.out.println("belum pernah aktivasi");
				otm.setPeriod(1);
				addrBookModel.setAbuid(generatedID);
				addrBookModel.setAbpwd("X" + generatedID);
				addrBookModel.setEmail(biodataModel.getEmail());
				addrBookModel.setIsLocked(false);
				addrBookModel.setIsDelete(false);
				addrBookModel.setCreatedBy(userRoleModel.getAddrbookld());
				addrBookModel.setCreatedOn(createdOn);
				this.addrBookService.update(addrBookModel);
				biodataModel.setAddrbookId(this.addrBookService.getSize());
				this.biodataService.update(biodataModel);
			} else {
				// System.out.println("sudah pernah aktivasi");
				otm.setPeriod(1 + this.onlineTestService.searchMaxPeriodByBiodataId(idBiodata));
				addrBookModel = this.addrBookService.findById(biodataModel.getAddrBookModel().getId());
				addrBookModel.setIsLocked(false);
				addrBookModel.setIsDelete(false);
				addrBookModel.setModifiedBy(userRoleModel.getAddrbookld());
				addrBookModel.setModifiedOn(createdOn);
				this.addrBookService.update(addrBookModel);
			}

			otm.setTestDate(tglTes);
			otm.setExpiredTest(tglAkhirTes);
			otm.setUserAccess(userAccess);
			otm.setStatus("Dibuat");
			otm.setIsDelete(false);
			otm.setCreatedOn(createdOn);
			otm.setCreatedBy(userRoleModel.getAddrbookld());
			this.onlineTestService.create(otm);

			// insert onlinetestdetail disini
			if (jenisTesList.size() > 1) {
				for (int i = 0; i < jenisTesList.size(); i++) {
					OnlineTestDetailModel otdm = new OnlineTestDetailModel();
					otdm.setOnlineTestId(otm.getId());
					otdm.setTestOrder(i + 1);
					otdm.setTestTypeId(Long.parseLong(jenisTesList.get(i)));
					otdm.setIsDelete(false);
					otdm.setCreatedOn(createdOn);
					otdm.setCreatedBy(userRoleModel.getAddrbookld());
					this.onlineTestDetailService.create(otdm);
				}
			}

			// kirim email disini
			smtpMailSender.send(addrBookModel.getEmail(), // biodataModel.getEmail()
					"Aktivasi Akun Sukses",
					"Hai " + biodataModel.getNickName()
							+ ", akun tes anda telah berhasil aktif. Anda dapat login dengan menggunakan :\r"
							+ "Username = " + addrBookModel.getAbuid() + "\r" + "Password = " + addrBookModel.getAbpwd()
							+ "\r" + ". Waktu tes anda dimulai pada tanggal "
							+ DateFormat.getDateInstance().format(tglTes) + " dan berakhir pada "
							+ DateFormat.getDateInstance().format(tglAkhirTes));
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("exception occured");
		}

		return "/aktivasi akun/list";
	}

	@RequestMapping("/doNonAktifkan")
	public String doNonAktifkan(HttpServletRequest req, Model model) {
		Long idBiodata = Long.parseLong(req.getParameter("id"));

		HttpSession session = req.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date createdOn = ts;

		AddrBookModel abm = new AddrBookModel();
		abm = this.addrBookService.findById(biodataModel.getAddrBookModel().getId());
		abm.setIsDelete(true); // abm.setIsLocked(true);
		abm.setModifiedBy(userRoleModel.getAddrbookld());
		abm.setModifiedOn(createdOn);
		this.addrBookService.update(abm);

		return "/aktivasi akun/list";
	}

	// tambah tes pada tampilkan
	@RequestMapping("/tambahTes")
	public String tambahTes(HttpServletRequest req, Model model) {
		Long idTes = Long.parseLong(req.getParameter("id"));
		// System.out.println(idTes);
		OnlineTestModel otm = this.onlineTestService.searchIdOnlineTest(idTes);
		this.doListTestType(model);
		model.addAttribute("onlenTestModel", otm);
		return "/aktivasi akun/add_test";
	}

	@RequestMapping("/doTambahTes") // belum selesai
	public String doTambahTes(HttpServletRequest req, Model model) {
		Long onlineTestId = Long.parseLong(req.getParameter("onlineTestId"));
		Long testTypeId = Long.parseLong(req.getParameter("testTypeId"));
		// System.out.println(onlineTestId);
		// System.out.println(testTypeId);

		HttpSession session = req.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
		OnlineTestModel onlineTestModel = this.onlineTestService.searchIdOnlineTest(onlineTestId);
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date createdOn = ts;

		OnlineTestDetailModel otdm = new OnlineTestDetailModel();
		otdm.setOnlineTestId(onlineTestModel.getId());
		otdm.setTestTypeId(testTypeId);
		// set order
		otdm.setTestOrder(1 + this.findMaxTestOrder(onlineTestModel.getId()));
		otdm.setIsDelete(false);
		otdm.setCreatedOn(createdOn);
		otdm.setCreatedBy(userRoleModel.getAddrbookld());
		this.onlineTestDetailService.create(otdm);

		return "/aktivasi akun/list_test";
	}

	// tambah tes pada aktivasi
	@RequestMapping("/tambahTesAktivasi")
	public String tambahTesAktivasi(HttpServletRequest req, Model model) {
		this.doListTestType(model);
		return "/aktivasi akun/activation_add_test";
	}

	@RequestMapping("/lihatPilihanTes")
	public String lihatTes(HttpServletRequest req, Model model) {
		Long id = Long.parseLong(req.getParameter("id"));
		OnlineTestModel otm = new OnlineTestModel();
		otm = this.onlineTestService.searchIdOnlineTest(id);
		model.addAttribute("onlineTestModel", otm);
		return "/aktivasi akun/detail_test";
	}

	@RequestMapping("/dataTes")
	public String doRefreshListTest(HttpServletRequest req, Model model) {
		Long idOnlineTest = Long.parseLong(req.getParameter("id"));
		// System.out.println(idOnlineTest);
		OnlineTestModel otm = this.onlineTestService.searchIdOnlineTest(idOnlineTest);
		List<OnlineTestDetailModel> otdmList = new ArrayList<OnlineTestDetailModel>();
		otdmList = this.onlineTestDetailService.findByOnlineTestIdAndIsNotDelete(otm.getId());
		model.addAttribute("onlineTestDetailList", otdmList);
		this.doListTestType(model);
		return "/aktivasi akun/list_test";
	}

	@RequestMapping("/data")
	public String doData(HttpServletRequest req, Model model) {
		Long idBiodata = Long.parseLong(req.getParameter("id"));
		BiodataModel bm = this.biodataService.searchIdBiodata(idBiodata);
		List<OnlineTestModel> otmList = new ArrayList<OnlineTestModel>();
		otmList = this.onlineTestService.findByBiodataAndIsNotDelete(bm.getId());
		model.addAttribute("bioModel", bm);
		model.addAttribute("onlineTestList", otmList);
		// this.doListNoteType(model);
		return "/aktivasi akun/list";
	}

	@RequestMapping("/dataIdAndPw")
	public String doDataAbuidAndAbpwd(HttpServletRequest req, Model model) {
		Long idBiodata = Long.parseLong(req.getParameter("id"));
		BiodataModel bm = this.biodataService.searchIdBiodata(idBiodata);
		model.addAttribute("bioModel", bm);
		// this.doListNoteType(model);
		return "/aktivasi akun/home";
	}

	@RequestMapping("/hapus")
	public String doHapus(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		OnlineTestModel otm = new OnlineTestModel();
		otm = this.onlineTestService.searchIdOnlineTest(id);
		model.addAttribute("onlineTestModel", otm);
		return "/aktivasi akun/delete";
	}

	@RequestMapping("/confirmdelete")
	public String doConfirmDelete(HttpServletRequest req, Model model) {
		Long id = Long.parseLong(req.getParameter("id"));

		HttpSession session = req.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date deletedOn = ts;

		OnlineTestModel otm = new OnlineTestModel();
		otm = this.onlineTestService.searchIdOnlineTest(id);
		otm.setIsDelete(true);
		otm.setDeletedOn(deletedOn);
		otm.setDeletedBy(userRoleModel.getAddrbookld());
		this.onlineTestService.update(otm);
		return "/aktivasi akun/list";
	}

	@RequestMapping("/icon delete")
	public String doIconDelete(HttpServletRequest req, Model model) {
		Long idOnlineTest = Long.parseLong(req.getParameter("id"));

		HttpSession session = req.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date deletedOn = ts;

		OnlineTestDetailModel otdm = new OnlineTestDetailModel();
		otdm = this.onlineTestDetailService.searchIdOnlineTest(idOnlineTest);
		otdm.setIsDelete(true);
		otdm.setDeletedOn(deletedOn);
		otdm.setDeletedBy(userRoleModel.getAddrbookld());
		if (otdm.getTestOrder() != this.onlineTestDetailService.findMaxOrderByOnlineTestId(otdm.getOnlineTestId())) {
			this.onlineTestDetailService.updateTestOrder(otdm.getOnlineTestId(), otdm.getTestOrder());
		}
		this.onlineTestDetailService.update(otdm);
		return "/aktivasi akun/list";
	}

	private void doListTestType(Model model) {
		List<TestTypeModel> ttList = new ArrayList<TestTypeModel>();
		ttList = this.testTypeService.searchNotDelete();
		model.addAttribute("testTypeModelList", ttList);
	}

	private Integer findMaxTestOrder(Long onlineTestId) {
		Integer maxOrder = this.onlineTestDetailService.findMaxOrderByOnlineTestId(onlineTestId);
		if (maxOrder == null) {
			return maxOrder = 0;
		} else {
			return maxOrder;
		}
	}

	private Boolean isThisBiodataEverActivating(Long idBiodata) {
		BiodataModel bm = new BiodataModel();
		bm = this.biodataService.searchIdBiodata(idBiodata);
		if (bm.getAddrbookId() == null) {
			return false;
		} else {
			return true;
		}
		// List<OnlineTestModel> otmList = new ArrayList<OnlineTestModel>();
		// otmList = this.onlineTestService.findByBiodata(idBiodata);
		// if( otmList.size() > 0) {
		// return true;
		// } else {
		// return false;
		// }
	}

	private String maxPeriodIncrement() {
		StringBuilder lastPeriodCode = new StringBuilder(this.onlineTestService.searchLastPRD());
		lastPeriodCode.delete(0, 3); // PRD
		Integer maxPeriod = Integer.parseInt(lastPeriodCode.toString());
		maxPeriod += 1;
		String maxPeriodString = "PRD" + String.format("%05d", maxPeriod);
		return maxPeriodString;
	}

}
