package com.example.demo.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.BiodataModel;
import com.example.demo.model.EducationLevel;
import com.example.demo.model.RiwayatPendidikanModel;
import com.example.demo.model.UserRoleModel;
import com.example.demo.service.BiodataService;
import com.example.demo.service.EducationLevelService;
import com.example.demo.service.RiwayatPendidikanService;

@Controller
@RequestMapping("/pendidikan")
public class RiwayatPendidikanController {

	@Autowired
	private RiwayatPendidikanService riwayatPendidikanService;
	
	@Autowired
	private EducationLevelService educationLevelService;
	
	@Autowired
	private BiodataService biodataService;
	
//	home
	
	@RequestMapping("/home")
	public String doHome(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(id);
		model.addAttribute("biodataModel", biodataModel);
		return "/pendidikan/home";
	}
	
//	list
	
	@RequestMapping("/data")
	public String doList (HttpServletRequest request, Model model) {
		Long idBiodata = Long.parseLong(request.getParameter("id"));
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);

//		List<RiwayatPendidikanModel> riwayatPendidikanModelList = new ArrayList<RiwayatPendidikanModel>();
		List<RiwayatPendidikanModel> riwayatPendidikanModelList = this.riwayatPendidikanService.findByBiodataAndIsNotDelete(biodataModel.getId());
		
		model.addAttribute("riwayatPendidikanModelList", riwayatPendidikanModelList);
		return "/pendidikan/list";
	}
	
//	add
	
	@RequestMapping("/add")
	public String doTambah (HttpServletRequest request, Model model) {
		Long idBiodata = Long.parseLong(request.getParameter("id"));
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);
		this.doJenjang(model);
		model.addAttribute("biodataModel", biodataModel);
		return "/pendidikan/add";
	}
	
	public void doJenjang(Model model) {
		
		List<EducationLevel> jenjangPendidikan = new ArrayList<EducationLevel>();
		jenjangPendidikan = this.educationLevelService.read();
		model.addAttribute("jenjangPendidikan", jenjangPendidikan);
	}
	
	@RequestMapping("/create")
	public String doCreate(HttpServletRequest request) {
		Long idBiodata = Long.parseLong(request.getParameter("id"));
		String schoolName = request.getParameter("schoolName");
		String city= request.getParameter("city");
		String country = request.getParameter("country");
		Long educationLevelId = Long.parseLong(request.getParameter("educationLevel"));
		String entryYear = request.getParameter("entryYear");
		String graduationYear = request.getParameter("graduationYear");
		String major = request.getParameter("major");
		Double gpa = null;
		String tester = request.getParameter("gpa");
		if (tester.isEmpty() == false) {
			gpa = Double.valueOf(request.getParameter("gpa"));
		}
		String notes = request.getParameter("notes");
		/*
		 * BiodataModel biodataModel = new BiodataModel();
		 * biodataModel = this.searchByAddrBookId;
		 * Long biodataId = biodataModel.getId();
		 */
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date createdOn = ts;
		
		RiwayatPendidikanModel pendidikanModel = new RiwayatPendidikanModel();
		
		pendidikanModel.setSchoolName(schoolName);
		pendidikanModel.setCity(city);
		pendidikanModel.setCountry(country);
		pendidikanModel.setEducationLevelId(educationLevelId);
		pendidikanModel.setEntryYear(entryYear);
		pendidikanModel.setGraduationYear(graduationYear);
		pendidikanModel.setMajor(major);
		pendidikanModel.setGpa(gpa);
		pendidikanModel.setNotes(notes);
		pendidikanModel.setBiodataId(biodataModel.getId());
		pendidikanModel.setIsDelete(false);
		pendidikanModel.setCreatedOn(createdOn);
		pendidikanModel.setCreatedBy(userRoleModel.getAddrbookld());
		
		this.riwayatPendidikanService.save(pendidikanModel);
		
		return "/pendidikan/list";
	}
	
//	edit
	
	@RequestMapping("/ubah")
	public String doUbah (HttpServletRequest request, Model model) {
		Long idPendidikan = Long.parseLong(request.getParameter("id"));
		
		RiwayatPendidikanModel pendidikanModel = new RiwayatPendidikanModel();
		pendidikanModel = this.riwayatPendidikanService.searchIdRiwayatPendidikan(idPendidikan);
		this.doJenjang(model);
		model.addAttribute("pendidikanModel", pendidikanModel);
		
		return "/pendidikan/edit";
	}

	
	@RequestMapping("/doUpdate")
	public String doUpdate(HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		String schoolName = request.getParameter("schoolName");
		String city= request.getParameter("city");
		String country = request.getParameter("country");
		Long educationLevelId = Long.parseLong(request.getParameter("educationLevel"));
		String entryYear = request.getParameter("entryYear");
		String graduationYear = request.getParameter("graduationYear");
		String major = request.getParameter("major");
		
		Double gpa = null;
		String tester = request.getParameter("gpa");
		if (tester.isEmpty() == false) {
			gpa = Double.valueOf(request.getParameter("gpa"));
		}
		
		
		String notes = request.getParameter("notes");

		// Mengatur siapa yang mengubah
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date modifiedOn = ts;
		
		RiwayatPendidikanModel pendidikanModel = new RiwayatPendidikanModel();
		pendidikanModel = this.riwayatPendidikanService.searchIdRiwayatPendidikan(id);
		
		pendidikanModel.setSchoolName(schoolName);
		pendidikanModel.setCity(city);
		pendidikanModel.setCountry(country);
		pendidikanModel.setEducationLevelId(educationLevelId);
		pendidikanModel.setEntryYear(entryYear);
		pendidikanModel.setGraduationYear(graduationYear);
		pendidikanModel.setMajor(major);
		pendidikanModel.setGpa(gpa);
		pendidikanModel.setNotes(notes);
		pendidikanModel.setIsDelete(false);
		pendidikanModel.setModifiedBy(userRoleModel.getAddrbookld());
		pendidikanModel.setModifiedOn(modifiedOn);
		
		this.riwayatPendidikanService.update(pendidikanModel);
		
		return "/pendidikan/list";
	}
	
	
//	delete
	
	@RequestMapping("/hapus")
	public String doHapus(HttpServletRequest request, Model model) {
		Long idPendidikan = Long.parseLong(request.getParameter("id"));
		
		RiwayatPendidikanModel pendidikanModel = new RiwayatPendidikanModel();
		pendidikanModel = this.riwayatPendidikanService.searchIdRiwayatPendidikan(idPendidikan);
		
		model.addAttribute("pendidikanModel", pendidikanModel);
		
		return "/pendidikan/delete";
	}

	
	@RequestMapping("/doHapus")
	public String doDelete(HttpServletRequest request) {
		Long idPendidikan = Long.parseLong(request.getParameter("id"));
		/*
		 * BiodataModel biodataModel = new BiodataModel();
		 * biodataModel = this.searchByAddrBookId;
		 * Long biodataId = biodataModel.getId();
		 */
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date deletedOn = ts;
		
		RiwayatPendidikanModel pendidikanModel = new RiwayatPendidikanModel();
		pendidikanModel = this.riwayatPendidikanService.searchIdRiwayatPendidikan(idPendidikan);
		
		pendidikanModel.setIsDelete(true);
		pendidikanModel.setDeletedBy(userRoleModel.getAddrbookld());
		pendidikanModel.setDeletedOn(deletedOn);
		
		this.riwayatPendidikanService.update(pendidikanModel);
		
		return "/pendidikan/list";
	}
	

	
}
