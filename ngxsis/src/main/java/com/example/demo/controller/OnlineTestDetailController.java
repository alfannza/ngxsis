package com.example.demo.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.BiodataModel;
import com.example.demo.model.OnlineTestDetailModel;
import com.example.demo.model.OnlineTestModel;
import com.example.demo.model.TestTypeModel;
import com.example.demo.model.UserRoleModel;
import com.example.demo.service.BiodataService;
import com.example.demo.service.OnlineTestDetailService;
import com.example.demo.service.OnlineTestService;
import com.example.demo.service.TestTypeService;

@Controller
@RequestMapping("/tes detail")
public class OnlineTestDetailController {

	@Autowired
	OnlineTestService onlineTestService;
	
	@Autowired
	OnlineTestDetailService onlineTestDetailService;
		
	@Autowired
	TestTypeService testTypeService;
	

	@RequestMapping("/data")
	public String doData(HttpServletRequest req, Model model) {
		Long idOnlineTest = Long.parseLong(req.getParameter("id"));
		OnlineTestModel otm = this.onlineTestService.searchIdOnlineTest(idOnlineTest);
		List<OnlineTestDetailModel> otdmList = new ArrayList<OnlineTestDetailModel>();
		otdmList = this.onlineTestDetailService.findByOnlineTestIdAndIsNotDelete(otm.getId());
		model.addAttribute("onlineTestDetailList", otdmList);
		//this.doListTestType(model);
		return "/aktivasi akun/list_test";
	}
	
	public void doListTestType(Model model) {
		List<TestTypeModel> ttList = new ArrayList<TestTypeModel>();
		ttList = this.testTypeService.searchNotDelete();
		model.addAttribute("testTypeModelList", ttList);
	}
	
}
