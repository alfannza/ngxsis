package com.example.demo.controller;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.BiodataModel;
import com.example.demo.model.ScheduleTypeModel;
import com.example.demo.model.Undangan;
import com.example.demo.model.UndanganDetail;
import com.example.demo.model.UserRoleModel;
import com.example.demo.service.BiodataService;
import com.example.demo.service.ScheduleTypeService;
import com.example.demo.service.UndanganDetailService;
import com.example.demo.service.UndanganService;

@Controller
@RequestMapping("/undangan")
public class UndanganController {

	private Integer page = 0;
	private Integer element = 10;
	private Boolean ascdesc = true; 
	
	@Autowired
	private UndanganService undanganService;
	
	@Autowired
	private ScheduleTypeService scheduleTypeService;
	
	@Autowired
	private BiodataService biodataService;
	
	@Autowired
	private UndanganDetailService undanganDetailService;
	
	@RequestMapping("/home")
	public String doHome() {
		return "/undangan/home";
	}
	
	@RequestMapping("/data")
	public String doList(Model model) {
		List<UndanganDetail> undanganDetailList = new ArrayList<UndanganDetail>();
		undanganDetailList = this.undanganDetailService.findIsNotDelete();
		
		model.addAttribute("undanganDetailList", undanganDetailList);
		return "/undangan/list";
	}
	
	@RequestMapping("/tambah")
	public String doTambah(Model model) {
		this.doJadwal(model);
		this.doBiodata(model);
		return "/undangan/tambah";
	}
	
	@RequestMapping("/create")
	public String doCreate(HttpServletRequest request) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		String tanggal = request.getParameter("date");
		String jam = request.getParameter("jam");
		Long biodataId = Long.parseLong(request.getParameter("biodataId"));
		Long jenisUndangan = Long.parseLong(request.getParameter("jenisUndangan"));
		String waktuUndangan = tanggal + ' ' + jam;
		Date invitationDate = format.parse(waktuUndangan);
		
		Long ro;
		Long tro;
		try {
			ro = Long.parseLong(request.getParameter("ro"));
		} catch (Exception e) {
			ro = null;
		}
		
		try {
			tro = Long.parseLong(request.getParameter("tro"));
		} catch (Exception e) {
			tro = null;
		}
		String otherRoTro = request.getParameter("otherRoTro");
		String location = request.getParameter("location");
		String notes = request.getParameter("notes");
		
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date createdOn = ts;
		
		Undangan undangan = new Undangan();
		Undangan undanganTerpilih = new Undangan();
		List<Undangan> undanganList = new ArrayList<Undangan>();
		UndanganDetail undanganDetail = new UndanganDetail();
		
		undangan.setCreatedBy(userRoleModel.getAddrbookld());
		undangan.setCreatedOn(createdOn);
		undangan.setInvitationDate(invitationDate);
		undangan.setRo(ro);
		undangan.setTro(tro);
		undangan.setOtherRoTro(otherRoTro);
		undangan.setLocation(location);
		undangan.setScheduleTypeId(jenisUndangan);
		undangan.setIsDelete(false);
		
		this.undanganService.save(undangan);
		undanganList = this.undanganService.findIsNotDelete();
		
		for (int i = 0; i < undanganList.size(); i++) {
			Date test = undanganList.get(i).getInvitationDate();
			Long undanganTest = undanganList.get(i).getScheduleTypeId();
			if(test == invitationDate && undanganTest == jenisUndangan) {
				undanganTerpilih = undanganList.get(i);
			}
		}
		
		undanganDetail.setBiodataId(biodataId);
		undanganDetail.setUndanganId(undanganTerpilih.getId());
		undanganDetail.setIsDelete(false);
		undanganDetail.setNotes(notes);
		undanganDetail.setCreatedBy(userRoleModel.getAddrbookld());
		undanganDetail.setCreatedOn(createdOn);
		
		this.undanganDetailService.save(undanganDetail);
		return "/undangan/list";
	}
	
	public void doJadwal(Model model) {
		List<ScheduleTypeModel> jadwalList = new ArrayList<ScheduleTypeModel>();
		jadwalList = this.scheduleTypeService.findIsNotDelete();
		model.addAttribute("jadwalList", jadwalList);
	}
	
	public void doBiodata(Model model) {
		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();
		biodataModelList = this.biodataService.read();
		model.addAttribute("biodataModelList", biodataModelList);
	}
	
	@RequestMapping("/ubah")
	public String doUbah(HttpServletRequest request, Model model) throws ParseException {
		Long id = Long.parseLong(request.getParameter("id"));
		
		UndanganDetail undanganDetail = new UndanganDetail();
		WaktuUndangan waktuUndangan = new WaktuUndangan();
		undanganDetail = this.undanganDetailService.searchById(id);
		
		Date tanggalBelumString = undanganDetail.getUndangan().getInvitationDate();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String tanggalSudahString = format.format(tanggalBelumString);
		String[] tanggalTerpisah = tanggalSudahString.split(" ");
		
		java.sql.Date tanggalString = java.sql.Date.valueOf(tanggalTerpisah[0]) /*format1.parse(tanggalTerpisah[0])*/;
		System.out.println(tanggalString);
		
		DateTimeFormatter.ofPattern("HH:mm");
		DateTimeFormatter format2 = DateTimeFormatter.ISO_TIME;
		
		LocalTime jamString =  LocalTime.parse(tanggalTerpisah[1], format2);
		System.out.println(jamString);
		waktuUndangan.setTanggal(tanggalString);
		waktuUndangan.setJam(jamString);
		
		
		model.addAttribute("undanganDetail", undanganDetail);
		model.addAttribute("waktuUndangan", waktuUndangan);
		this.doJadwal(model);
		this.doBiodata(model);
		return "/undangan/ubah";
	}
	
	@RequestMapping("/doUpdate")
	public String doUpdate(HttpServletRequest request) throws ParseException {
		Long idUndanganDetail = Long.parseLong(request.getParameter("idUndanganDetail"));
		Long idUndangan = Long.parseLong(request.getParameter("idUndangan"));
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		String tanggal = request.getParameter("date");
		String jam = request.getParameter("jam");
		Long biodataId = Long.parseLong(request.getParameter("biodataId"));
		Long jenisUndangan = Long.parseLong(request.getParameter("jenisUndangan"));
		String waktuUndangan = tanggal + ' ' + jam;
		Date invitationDate = format.parse(waktuUndangan);
		Long ro;
		Long tro;
		try {
			ro = Long.parseLong(request.getParameter("ro"));
		} catch (Exception e) {
			ro = null;
		}
		
		try {
			tro = Long.parseLong(request.getParameter("tro"));
		} catch (Exception e) {
			tro = null;
		}
		
		String otherRoTro = request.getParameter("otherRoTro");
		String location = request.getParameter("location");
		String notes = request.getParameter("notes");
		
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date modifiedOn = ts;
		
		Undangan undangan = new Undangan();
		UndanganDetail undanganDetail = new UndanganDetail();
		undangan = this.undanganService.searchById(idUndangan);
		
		undangan.setModifiedBy(userRoleModel.getAddrbookld());
		undangan.setModifiedOn(modifiedOn);
		undangan.setInvitationDate(invitationDate);
		undangan.setRo(ro);
		undangan.setTro(tro);
		undangan.setOtherRoTro(otherRoTro);
		undangan.setScheduleTypeId(jenisUndangan);
		undangan.setLocation(location);
		
		this.undanganService.update(undangan);
		
		undanganDetail = this.undanganDetailService.searchById(idUndanganDetail);
		
		undanganDetail.setBiodataId(biodataId);
		undanganDetail.setUndanganId(idUndangan);
		undanganDetail.setNotes(notes);
		undanganDetail.setModifiedBy(userRoleModel.getAddrbookld());
		undanganDetail.setModifiedOn(modifiedOn);
		
		this.undanganDetailService.update(undanganDetail);
		
		return "/undangan/list";
	}
	
	@RequestMapping("/hapus")
	public String doHapus(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		
		UndanganDetail undanganDetail = new UndanganDetail();
		undanganDetail = this.undanganDetailService.searchById(id);

		model.addAttribute("undanganDetail", undanganDetail);
		return "/undangan/hapus";
	}
	
	@RequestMapping("/doHapus")
	public String doDelete(HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date deletedOn = ts;
				
		UndanganDetail undanganDetail = new UndanganDetail();
		undanganDetail = this.undanganDetailService.searchById(id);
		undanganDetail.setIsDelete(true);
		undanganDetail.setDeletedBy(userRoleModel.getAddrbookld());
		undanganDetail.setDeletedOn(deletedOn);
		
		this.undanganDetailService.update(undanganDetail);

		
		Undangan undangan = new Undangan();
		undangan = this.undanganService.searchById(undanganDetail.getUndanganId());
		undangan.setIsDelete(true);
		undangan.setDeletedBy(userRoleModel.getAddrbookld());
		undangan.setDeletedOn(deletedOn);
		
		this.undanganService.update(undangan);
		
		return "/undangan/list";
	}
	
	@RequestMapping("/doAscending")
	public String doAscending(Model model) {
		List<UndanganDetail> undanganDetailListDummy = new ArrayList<UndanganDetail>();
		List<UndanganDetail> undanganDetailList = new ArrayList<UndanganDetail>();
		undanganDetailListDummy = this.undanganDetailService.findAscendingBiodata();
		this.ascdesc = true;
		
		Integer start = this.page * this.element;
		Integer end = start + this.element;
		
		for (int i = start; i < end; i++) {
			try {
				undanganDetailList.add(undanganDetailListDummy.get(i));
			} catch (Exception e) {
				break;
			}
		}
		
		model.addAttribute("undanganDetailList", undanganDetailList);
		return "/undangan/list";
	}
	
	@RequestMapping("/doDescending")
	public String doDescending(Model model) {
		List<UndanganDetail> undanganDetailListDummy = new ArrayList<UndanganDetail>();
		List<UndanganDetail> undanganDetailList = new ArrayList<UndanganDetail>();
		undanganDetailListDummy = this.undanganDetailService.findDescendingBiodata();
		this.ascdesc = false;
		
		Integer start = this.page * this.element;
		Integer end = start + this.element;
		
		for (int i = start; i < end; i++) {
			try {
				undanganDetailList.add(undanganDetailListDummy.get(i));
			} catch (Exception e) {
				break;
			}
		}
		
		model.addAttribute("undanganDetailList", undanganDetailList);
		return "/undangan/list";
	}
	
	@RequestMapping("/jumlahList")
	public String doJumlahList(HttpServletRequest request, Model model) {
		
		Integer jumlahList = Integer.parseInt(request.getParameter("element"));
		this.element = jumlahList;
		
		List<UndanganDetail> undanganDetailListDummy = new ArrayList<UndanganDetail>();
		List<UndanganDetail> undanganDetailList = new ArrayList<UndanganDetail>();
		undanganDetailListDummy = this.undanganDetailService.findAscendingBiodata();
		
		for (int i = 0; i < jumlahList; i++) {
			try {
				undanganDetailList.add(undanganDetailListDummy.get(i));
			} catch (Exception e) {
				break;
			}
		}
		
		model.addAttribute("undanganDetailList", undanganDetailList);
		return "/undangan/list";
	}
	
	@RequestMapping("/search/name")
	public String doSearchDate(HttpServletRequest request, Model model) throws ParseException {
		String name = request.getParameter("name");
		System.out.println(name);
		List<UndanganDetail> undanganDetailList = new ArrayList<UndanganDetail>();
		undanganDetailList = this.undanganDetailService.searchLikeNamePelamar(name);
		System.out.println(undanganDetailList.get(0).getBiodataModel().getFullname());
		
		model.addAttribute("undanganDetailList", undanganDetailList);
		return "/undangan/list";
	}
	
	@RequestMapping("/doBack")
	public String doBack(Model model) {
		
		this.page = this.page - 1;
		if (this.page < 0) {
			this.page = 0;
		}
		
		List<UndanganDetail> undanganDetailListDummy = new ArrayList<UndanganDetail>();
		List<UndanganDetail> undanganDetailList = new ArrayList<UndanganDetail>();
		if(this.ascdesc == true) {
			undanganDetailListDummy = this.undanganDetailService.findAscendingBiodata();
		} else {
			undanganDetailListDummy = this.undanganDetailService.findDescendingBiodata();
		}
		
		Integer start = this.page * this.element;
		Integer end = start + this.element;
		
		for (int i = start; i < end; i++) {
			try {
				undanganDetailList.add(undanganDetailListDummy.get(i));
			} catch (Exception e) {
				break;
			}
		}
		
		model.addAttribute("undanganDetailList", undanganDetailList);
		return "/undangan/list";
	}
	
	@RequestMapping("/doAfter")
	public String doAfter(Model model) {
		this.page = this.page + 1;
		List<UndanganDetail> undanganDetailListDummy = new ArrayList<UndanganDetail>();
		List<UndanganDetail> undanganDetailList = new ArrayList<UndanganDetail>();
		if(this.ascdesc == true) {
			undanganDetailListDummy = this.undanganDetailService.findAscendingBiodata();
		} else {
			undanganDetailListDummy = this.undanganDetailService.findDescendingBiodata();
		}
		
		Integer maxPage = undanganDetailListDummy.size() / this.element;
		
		if (this.page > maxPage) {
			this.page = maxPage;
		}
		
		Integer start = this.page * this.element;
		Integer end = start + this.element;
		
		for (int i = start; i < end; i++) {
			try {
				undanganDetailList.add(undanganDetailListDummy.get(i));
			} catch (Exception e) {
				break;
			}
		}
		
		model.addAttribute("undanganDetailList", undanganDetailList);
		return "/undangan/list";
	}
}

class WaktuUndangan {
	
	private Date tanggal;
	
	private LocalTime jam;

	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(  @DateTimeFormat(pattern = "yyyy-MM-dd") Date tanggal) {
		this.tanggal = tanggal;
	}

	public LocalTime getJam() {
		return jam;
	}

	public void setJam(LocalTime jam) {
		this.jam = jam;
	}

}
