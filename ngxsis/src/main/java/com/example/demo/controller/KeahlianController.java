package com.example.demo.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.BiodataModel;
import com.example.demo.model.KeahlianModel;
import com.example.demo.model.SkillLevelModel;
import com.example.demo.model.UserRoleModel;
import com.example.demo.service.BiodataService;
import com.example.demo.service.KeahlianService;
import com.example.demo.service.SkillLevelService;

@Controller
@RequestMapping("/keahlian")
public class KeahlianController {

	@Autowired
	KeahlianService keahlianService;
	
	@Autowired
	BiodataService biodataService;
	
	@Autowired
	SkillLevelService skillLevelService;
	
	@RequestMapping("/home")
	public String doHome(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(id);
		model.addAttribute("biodataModel", biodataModel);
		
		String page = "/keahlian/home";
		return page;
	}
	
	@RequestMapping("/data")
	public String doData(HttpServletRequest request, Model model) {
		Long idBiodata = Long.parseLong(request.getParameter("id"));
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);
		
		List<KeahlianModel> keahlianModel = new ArrayList<KeahlianModel>();
		keahlianModel = this.keahlianService.findByBiodataAndIsNotDelete(biodataModel.getId());
		model.addAttribute("keahlianModel", keahlianModel);
		
		return "/keahlian/list";
	}
	
	@RequestMapping("/detail")
	public String doDetail(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		KeahlianModel keahlianModel = new KeahlianModel();
		keahlianModel = this.keahlianService.searchIdKeahlian(id);
		model.addAttribute("keahlianModel", keahlianModel);
		return "/keahlian/detail";
	}
	
	public void doSkillLevel(Model model) {
		List<SkillLevelModel> skillLevel = new ArrayList<SkillLevelModel>();
		skillLevel = this.skillLevelService.read();
		model.addAttribute("skillLevel", skillLevel);
	}
	
	@RequestMapping("/add")
	public String doTambah (HttpServletRequest request, Model model) {
		Long idBiodata = Long.parseLong(request.getParameter("id"));
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);
		this.doSkillLevel(model);
		model.addAttribute("biodataModel", biodataModel);
		return "/keahlian/add";
	}
	
	@RequestMapping("/create")
	public String doCreate(HttpServletRequest request) {
		Long idBiodata = Long.parseLong(request.getParameter("id"));
		String skillName = request.getParameter("skillName");
		Long skillLevelId = Long.parseLong(request.getParameter("skillLevelId"));
		String notes = request.getParameter("notes");
		
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);
		Timestamp time = new Timestamp(System.currentTimeMillis());
		Date createdOn = time;
		
		KeahlianModel keahlianModel = new KeahlianModel();
		keahlianModel.setBiodataId(biodataModel.getId());
		keahlianModel.setSkillName(skillName);
		keahlianModel.setSkillLevelId(skillLevelId);
		keahlianModel.setNotes(notes);
		keahlianModel.setIsDelete(false);
		keahlianModel.setCreatedOn(createdOn);
		keahlianModel.setCreatedBy(userRoleModel.getAddrbookld());
		
		this.keahlianService.save(keahlianModel);
		
		return "/keahlian/list";
	}
	
	@RequestMapping("/ubah")
	public String doUbah (HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		
		KeahlianModel keahlianModel = new KeahlianModel();
		keahlianModel = this.keahlianService.searchIdKeahlian(id);
		this.doSkillLevel(model);
		model.addAttribute("keahlianModel", keahlianModel);
		
		return "/keahlian/edit";
	}
	
	@RequestMapping("/update")
	public String doUpdate(HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		String skillName = request.getParameter("skillName");
		Long skillLevelId = Long.parseLong(request.getParameter("skillLevelId"));
		String notes = request.getParameter("notes");
		
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		Timestamp time = new Timestamp(System.currentTimeMillis());
		Date modifiedOn = time;
		
		KeahlianModel keahlianModel = new KeahlianModel();
		keahlianModel = this.keahlianService.searchIdKeahlian(id);
		keahlianModel.setSkillName(skillName);
		keahlianModel.setSkillLevelId(skillLevelId);
		keahlianModel.setNotes(notes);
		keahlianModel.setIsDelete(false);
		keahlianModel.setModifiedOn(modifiedOn);
		keahlianModel.setModifiedBy(userRoleModel.getAddrbookld());
		
		this.keahlianService.update(keahlianModel);
		
		return "/keahlian/list";
	}
	
	@RequestMapping("/hapus")
	public String doHapus(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		
		KeahlianModel keahlianModel = new KeahlianModel();
		keahlianModel = this.keahlianService.searchIdKeahlian(id);
		model.addAttribute("keahlianModel", keahlianModel);
		
		return "/keahlian/delete";
	}
	
	@RequestMapping("/doHapus")
	public String doDelete(HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		Timestamp time = new Timestamp(System.currentTimeMillis());
		Date deletedOn = time;
		
		KeahlianModel keahlianModel = new KeahlianModel();
		keahlianModel = this.keahlianService.searchIdKeahlian(id);
		
		keahlianModel.setIsDelete(true);
		keahlianModel.setDeletedOn(deletedOn);
		keahlianModel.setDeletedBy(userRoleModel.getAddrbookld());
		
		this.keahlianService.update(keahlianModel);
		
		return "/keahlian/list";
	}

}
