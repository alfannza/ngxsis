package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.AddrBookModel;
import com.example.demo.model.BiodataModel;
import com.example.demo.model.CompanyModel;
import com.example.demo.model.MenuTreeModel;
import com.example.demo.model.UserRoleModel;
import com.example.demo.service.AddrBookService;
import com.example.demo.service.BiodataService;
import com.example.demo.service.CompanyService;
import com.example.demo.service.MenuTreeService;
import com.example.demo.service.UserRoleService;

@Controller
@RequestMapping("/masuk")
public class LoginController {

	@Autowired
	private AddrBookService addrBookService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private MenuTreeService menuTreeService;

	@Autowired
	private BiodataService biodataService;

	@RequestMapping("/login")
	public String doLogin() {
		String page = "/login/login";
		return page;
	}

	@RequestMapping("/home")
	public String doHome(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");

		BiodataModel biodataModel = new BiodataModel();
		biodataModel = this.biodataService.searchAddrBookId(userRoleModel.getAddrbookld());

		// dikirim ke html

		this.doMenuTree(model);
		this.doCompany(model);
		model.addAttribute("biodataModel", biodataModel);
		model.addAttribute("userRoleModel", userRoleModel);
		String page = "redirect:/";
		return page;
	}

	@RequestMapping("/pickedRole")
	public void doPickRole(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		AddrBookModel addrBookModel = (AddrBookModel) session.getAttribute("addrBookModel");

		Long roleId = Long.parseLong(request.getParameter("roleId"));
		UserRoleModel userRoleModel = new UserRoleModel();
		userRoleModel = this.userRoleService.findByUserRoleAndAddrBook(addrBookModel.getId(), roleId);
		System.out.println(addrBookModel.getId());
		System.out.println(roleId);
		System.out.println(userRoleModel.getAddrbookld());
		System.out.println(userRoleModel.getRoleId());
		session.removeAttribute("addrBookModel");
		session.setAttribute("userRoleModel", userRoleModel);
	}

	@RequestMapping("/login/akses")
	public String doLogin(HttpServletRequest request, Model model) {
		String email = request.getParameter("email/id");
		String abuid = request.getParameter("email/id");
		String abpwd = request.getParameter("abpwd");
		AddrBookModel addrBookModel = new AddrBookModel();
		addrBookModel = this.addrBookService.loginByUsernamer(email, abuid, abpwd);
		model.addAttribute("addrBookModel", addrBookModel);
		if (addrBookModel == null) {
			String page = "redirect:/masuk/login";
			return page;
		} else {
			HttpSession session = request.getSession();
			session.setAttribute("addrBookModel", addrBookModel);

			List<UserRoleModel> userRoleModelList = new ArrayList<UserRoleModel>();
			BiodataModel biodataModel = new BiodataModel();
			userRoleModelList = this.userRoleService.findByAddrBookId(addrBookModel.getId());
			biodataModel = this.biodataService.searchAddrBookId(addrBookModel.getId());

			// dikirim ke html

			this.doMenuTree(model);
			this.doCompany(model);
			model.addAttribute("userRoleModelList", userRoleModelList);
			model.addAttribute("biodataModel", biodataModel);

			String page = "/selectaccess/select_access";
			return page;
		}
	}

	public void doCompany(Model model) {
		List<CompanyModel> companyModelList = new ArrayList<CompanyModel>();
		companyModelList = this.companyService.read();

		model.addAttribute("companyModelList", companyModelList);
	}

	public void doMenuTree(Model model) {
		List<MenuTreeModel> menuTreeModelList = new ArrayList<MenuTreeModel>();
		menuTreeModelList = this.menuTreeService.read();
		model.addAttribute("menuTreeModelList", menuTreeModelList);
	}

}