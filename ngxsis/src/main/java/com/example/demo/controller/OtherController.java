package com.example.demo.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.model.PeReferensiModel;
import com.example.demo.model.UserRoleModel;
import com.example.demo.repository.BiodataRepository;
import com.example.demo.repository.KeteranganTambahanRepository;
import com.example.demo.repository.PeReferensiRepository;
import com.example.demo.service.BiodataService;
import com.example.demo.service.PeReferensiService;
import com.example.demo.model.AddrBookModel;
import com.example.demo.model.BiodataModel;
import com.example.demo.model.KeteranganTambahanModel;

@Controller
public class OtherController {

	@Autowired
	private HttpSession session;

	@Autowired
	private KeteranganTambahanRepository keteranganTambahanRepository;

	@Autowired
	private PeReferensiRepository peReferensiRepository;

	@Autowired
	private PeReferensiService preferensiService;

	@Autowired
	private BiodataRepository biodataRepository;

	@Autowired
	private BiodataService biodataService;

	@RequestMapping("/lainnya")
	public String doHome(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(id);
		model.addAttribute("biodataModel", biodataModel);
		KeteranganTambahanModel dataTambahan = this.keteranganTambahanRepository
				.getAdditionalInfo(biodataModel.getId());
		model.addAttribute("dataTambahan", dataTambahan);
		return "other/index";
	}

	@RequestMapping("/lainnya/data")
	public String populate(HttpServletRequest request, Model model) {

		Long idBiodata = Long.parseLong(request.getParameter("id"));
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);

		List<PeReferensiModel> referensiModelList = this.preferensiService.getReference(biodataModel.getId());

		model.addAttribute("referensiModelList", referensiModelList);
		System.out.println(idBiodata);
		return "other/list";
	}
	
	@RequestMapping("/lainnya/additional")
	public String populateAdd(HttpServletRequest request, Model model) {

		Long idBiodata = Long.parseLong(request.getParameter("id"));
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);
		
		KeteranganTambahanModel dataTambahan = this.keteranganTambahanRepository
				.getAdditionalInfo(biodataModel.getId());
		model.addAttribute("dataTambahan", dataTambahan);
		System.out.println(idBiodata);
		return "other/additional-info-list";
	}

	@RequestMapping("/lainnya/add_ref")
	public String doAdd(HttpServletRequest request, Model model) {
		Long idBiodata = Long.parseLong(request.getParameter("id"));
		//System.out.println(idBiodata);
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);
		model.addAttribute("biodataModel", biodataModel);
		return "other/add_reference";
	}

	@RequestMapping("/lainnya/simpan")
	public String save(HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		//System.out.println(id);
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(id);
		Date createdOn = new Timestamp(System.currentTimeMillis());
		//System.out.println(biodataModel.getId());
		PeReferensiModel preferensiModel = new PeReferensiModel();
		preferensiModel.setAddressPhone(request.getParameter("addressPhone"));
		preferensiModel.setName(request.getParameter("name"));
		preferensiModel.setRelation(request.getParameter("relation"));
		preferensiModel.setPosition(request.getParameter("position"));
		preferensiModel.setBiodataId(biodataModel.getId());
		preferensiModel.setIsDelete(false);
		preferensiModel.setCreatedBy(userRoleModel.getAddrbookld());
		preferensiModel.setCreatedOn(createdOn);

		this.peReferensiRepository.save(preferensiModel);
		return "other/list";
	}

	@RequestMapping("/lainnya/ubah")
	public String doEdit(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		//Long idBiodata = Long.parseLong(request.getParameter("idBiodata"));
		PeReferensiModel preferensiModel = this.peReferensiRepository.searchPeRefrensiById(id);
		model.addAttribute("preferensiModel", preferensiModel);
		//BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);
		//model.addAttribute("biodataModel", biodataModel);

		return "other/edit_reference";
	}

	@RequestMapping("/lainnya/update")
	public String update(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Long id = Long.parseLong(request.getParameter("id"));
		Date modifiedOn = ts;
		
		PeReferensiModel preferensiModel = this.peReferensiRepository.searchPeRefrensiById(id);
		preferensiModel.setAddressPhone(request.getParameter("addressPhone"));
		preferensiModel.setName(request.getParameter("name"));
		preferensiModel.setRelation(request.getParameter("relation"));
		preferensiModel.setPosition(request.getParameter("position"));
		preferensiModel.setModifiedBy(userRoleModel.getAddrbookld());
		preferensiModel.setModifiedOn(modifiedOn);
	
		this.preferensiService.save(preferensiModel);
		return "other/list";
	}

	@RequestMapping("/lainnya/hapus")
	public String hapus(HttpServletRequest request, Model model) {
		String name = request.getParameter("name");

		PeReferensiModel preferensiModel = new PeReferensiModel();
		preferensiModel = this.peReferensiRepository.getReference(name);

		model.addAttribute("preferensiModel", preferensiModel);

		return "/other/delete";
	}

	@RequestMapping("lainnya/delete")
	public String doHapus(HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		Date deletedOn = new Timestamp(System.currentTimeMillis());

		PeReferensiModel preferensiModel = new PeReferensiModel();
		preferensiModel = this.peReferensiRepository.searchPeRefrensiById(id);

		//preferensiModel.setBiodataId(biodataModel.getId());
		preferensiModel.setIsDelete(true);
		preferensiModel.setDeletedBy(userRoleModel.getId());
		preferensiModel.setDeletedOn(deletedOn);

		this.peReferensiRepository.save(preferensiModel);

		return "/other/list";
	}

	@RequestMapping(value = "lainnya/viewinfo")
	public String view(HttpServletRequest request, Model model) {
		Long idBiodata = Long.parseLong(request.getParameter("id"));
		session = request.getSession(false);

		if (session == null) {
			return "/login/login";
		} else {
			UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);

			KeteranganTambahanModel dataTambahan = this.keteranganTambahanRepository.getAdditionalInfo(idBiodata);
			model.addAttribute("dataTambahan", dataTambahan);
			return "other/additional_info_view";
		}
	}

	@RequestMapping("/lainnya/editinfo")
	public String doEditInfo(HttpServletRequest request, Model model) {
		Long idBiodata = Long.parseLong(request.getParameter("id"));
		session = request.getSession(false);

		if (session == null) {
			return "/login/login";
		} else {
			UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);

			KeteranganTambahanModel dataTambahan = this.keteranganTambahanRepository.getAdditionalInfo(idBiodata);
			model.addAttribute("dataTambahan", dataTambahan);
			return "other/additional_info_edit";
		}
	}

	@RequestMapping(value = "lainnya/updateinfo")
	public String tambah(HttpServletRequest request, Model model) {
		Long idBiodata = Long.parseLong(request.getParameter("id"));
		session = request.getSession(false);

		if (session == null) {
			return "/login/login";
		} else {
			UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);
			Date modifiedOn = ts;

			KeteranganTambahanModel dataTambahan = this.keteranganTambahanRepository.getAdditionalInfo(idBiodata);
			model.addAttribute("dataTambahan", dataTambahan);

			//dataTambahan.setBiodataId(idBiodata);
			dataTambahan.setDiseaseName(request.getParameter("diseaseName"));
			dataTambahan.setExpectedSalary(request.getParameter("expectedSalary"));
			dataTambahan.setIsNegotiable(Boolean.parseBoolean(request.getParameter("isNegotiable")));
			dataTambahan.setStartWorking(request.getParameter("startWorking"));
			dataTambahan.setIsReadyToOutoftown(Boolean.parseBoolean(request.getParameter("isReadyToOutoftown")));
			dataTambahan.setEmergencyContactName(request.getParameter("emergencyContactName"));
			dataTambahan.setEmergencyContactPhone(request.getParameter("emergencyContactPhone"));
			dataTambahan.setIsApplyOtherPlace(Boolean.parseBoolean(request.getParameter("isApplyOtherPlace")));
			dataTambahan.setApplyPlace(request.getParameter("applyPlace"));
			dataTambahan.setSelectionPhase(request.getParameter("selectionPhase"));
			dataTambahan.setIsEverBadlySick(Boolean.parseBoolean(request.getParameter("isEverBadlySick")));
			dataTambahan.setDiseaseName(request.getParameter("diseaseName"));
			dataTambahan.setDiseaseName(request.getParameter("diseaseTime"));
			dataTambahan.setIsEverPsychotest(Boolean.parseBoolean(request.getParameter("isEverPsychotest")));
			dataTambahan.setPsychotestNeeds(request.getParameter("psychotestNeeds"));
			dataTambahan.setPyschotestTime(request.getParameter("pyschotestTime"));
			dataTambahan.setOtherNotes(request.getParameter("otherNotes"));
			dataTambahan.setModifiedBy(userRoleModel.getId());
			dataTambahan.setModifiedOn(modifiedOn);
			dataTambahan.setIsDelete(false);

			this.keteranganTambahanRepository.save(dataTambahan);

			return "other/additional_info_edit";
		}
	}

}
