package com.example.demo.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.configuration.SmtpMailSender;
import com.example.demo.model.BiodataModel;
import com.example.demo.service.BiodataService;

@Controller
@RequestMapping("/token")
public class KirimTokenController {

	@Autowired
	BiodataService biodataService;
	@Autowired
	SmtpMailSender smtpMailSender;

	@RequestMapping("/home")
	public String doHome(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(id);
		model.addAttribute("biodataModel", biodataModel);
		String page = "/kirim token/token";
		return page;
	}

	@RequestMapping("/kirim")
	public void doKirim(HttpServletRequest request, Model model) throws ParseException, MessagingException {
		Long id = Long.parseLong(request.getParameter("id"));
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(id);
		String expired = request.getParameter("expiredToken");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date token = format.parse(expired);
		biodataModel.setExpiredToken(token);
		this.biodataService.update(biodataModel);

		smtpMailSender.send(biodataModel.getEmail(), "Anda Mendapatkan Token", "Hi " + biodataModel.getNickName() + " anda telah mendapatkan token "
				+ biodataModel.getToken() + " yang berlaku sampai dengan pada tanggal " + DateFormat.getDateInstance().format(biodataModel.getExpiredToken()));
	}
}
