package com. example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.BiodataModel;
import com.example.demo.service.BiodataService;

@Controller
@RequestMapping("/pelamar/proses")
public class ProsesPelamarController {
	
	@Autowired
	private BiodataService biodataService;

	@RequestMapping("/home")
	public String doHome() {
		return "/pelamar/proses/index";
	}
	
	@RequestMapping("/data")
	public String doList(Model model) {
		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();
		biodataModelList = this.biodataService.read();
		model.addAttribute("biodataModelList", biodataModelList);

		return "/pelamar/proses/list";
	}
}
