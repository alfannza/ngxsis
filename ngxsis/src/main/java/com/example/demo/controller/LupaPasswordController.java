package com.example.demo.controller;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.configuration.SmtpMailSender;
import com.example.demo.model.AddrBookModel;
import com.example.demo.service.AddrBookService;

@Controller
@RequestMapping("/lupa")
public class LupaPasswordController {

	@Autowired
	AddrBookService addrBookService;
	
	@Autowired
	SmtpMailSender smtpMailSender;

	@RequestMapping("/password")
	public String doLupa() {
		String page = "/lupa password/search email";
		return page;
	}

	@RequestMapping("/cek/email")
	public String doSearchMail(HttpServletRequest request, Model model) throws MessagingException {
		String email = request.getParameter("email/id");
		String abuid = request.getParameter("email/id");
		AddrBookModel addrBookModel = new AddrBookModel();
		addrBookModel = this.addrBookService.cekUser(email, abuid);
		model.addAttribute("addrBookModel", addrBookModel);
		HttpSession session = request.getSession();
		session.setAttribute("addrBookModel", addrBookModel);
		session.setMaxInactiveInterval(3*60);
		/*
		 * if (addrBookModel == null) { result.rejectValue("email", null,
		 * "Email/Username Tidak ditemukan!"); return "/lupa password/search email"; }
		 * else {
		 */
		
		smtpMailSender.send(addrBookModel.getEmail(), "Reset Password", "http://localhost:8585/lupa/reset");
		
		return "/lupa password/cek email";

	}
	
	@RequestMapping("/reset")
	public String doLupaPassByEmail(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session == null) {
			return "/lupa password/404";
		}
		String page = "/lupa password/reset";
		return page;
	}
	
	@RequestMapping("/reset/update")
	public String doLupResetPass(HttpServletRequest request, Model model) {
		HttpSession session=request.getSession(false);
		AddrBookModel addrBookModel = (AddrBookModel)session.getAttribute("addrBookModel");
		Long id = addrBookModel.getId();
		String abpwd = request.getParameter("newPassword");
		AddrBookModel addrBookModel2 = new AddrBookModel();
		addrBookModel2 = this.addrBookService.findById(id);
		addrBookModel2.setAbpwd(abpwd);
		this.addrBookService.update(addrBookModel2);
		request.getSession().invalidate();
		
		String page = "/login/login";
		return page;
		
	}

}
