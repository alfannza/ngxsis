package com.example.demo.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.BiodataModel;
import com.example.demo.model.RiwayatPekerjaanModel;
import com.example.demo.model.RiwayatProyekModel;
import com.example.demo.model.TimePeriodModel;
import com.example.demo.model.UserRoleModel;
import com.example.demo.service.RiwayatPekerjaanService;
import com.example.demo.service.RiwayatProyekService;
import com.example.demo.service.TimePeriodService;

@Controller
@RequestMapping("/proyek")
public class RiwayatProyekController {

	@Autowired
	RiwayatProyekService riwayatProyekService;
	
	@Autowired
	TimePeriodService timePeriodService;
	
	@Autowired
	RiwayatPekerjaanService riwayatPekerjaanService;
	
	@RequestMapping("/home")
	public String doHome() {
		return "/proyek/home";
	}
	
	@RequestMapping("/tambah")
	public String doTambah(HttpServletRequest req, Model model) {
		Long idPekerjaan = Long.parseLong(req.getParameter("id"));
		RiwayatPekerjaanModel rpm =this.riwayatPekerjaanService.searchIdPekerjaan(idPekerjaan);
		this.doListTimePeriod(model);
		model.addAttribute("pekerjaanModel", rpm);
		return "/proyek/add";
	}
	
	@RequestMapping("/create") // belum selesai, createdBy dan biodataId masih manual
	public String doCreate(HttpServletRequest req) {
		Long idPekerjaan = Long.parseLong(req.getParameter("id"));
		String startMonth = req.getParameter("startMonth");
		String startYear = req.getParameter("startYear");
		String projectName = req.getParameter("projectName");
		
		String projectDurationSTR = req.getParameter("projectDuration");
		//System.out.println(projectDurationSTR.length()); // if "" then 0
		//System.out.println(projectDurationSTR.isEmpty()); // if "" then true
		Integer projectDuration;	
		if (projectDurationSTR.length() == 0) {	// if user inputting a null value of string "projectDuration"
			projectDuration = null;				// Integer.parse can't resolve string if null
		} else {
			projectDuration = Integer.parseInt(projectDurationSTR);
		}
		String timePeriodIdSTR = req.getParameter("timePeriodId");
		Long timePeriodId;
		if (timePeriodIdSTR.length() == 0) {	// same goes for Long.parse
			timePeriodId = null;
		} else {
			timePeriodId =Long.parseLong(req.getParameter("timePeriodId"));
		}
		
		String client = req.getParameter("client");
		String projectPosition = req.getParameter("projectPosition");
		String description = req.getParameter("description");
		
		HttpSession session = req.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		RiwayatPekerjaanModel pekerjaanModel = this.riwayatPekerjaanService.searchIdPekerjaan(idPekerjaan);
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date createdOn = ts;
		
		RiwayatProyekModel rpm = new RiwayatProyekModel();
		rpm.setRiwayatPekerjaanId(pekerjaanModel.getId()); // berdasarkan pekerjaanmodel
		rpm.setStartMonth(startMonth);
		rpm.setStartYear(startYear);
		rpm.setProjectName(projectName);
		rpm.setProjectDuration(projectDuration);
		rpm.setTimePeriodId(timePeriodId);
		rpm.setClient(client);
		rpm.setProjectPosition(projectPosition);
		rpm.setDescription(description);
		rpm.setCreatedOn(createdOn);
		rpm.setCreatedBy(userRoleModel.getAddrbookld());
		rpm.setIsDelete(false);
		this.riwayatProyekService.create(rpm);
		return "/pekerjaan/list"; // list, tiap create proyekrefresh halaman pekerjaan
	}

	@RequestMapping("/data")
	public String doData(Model model) {
		List<RiwayatProyekModel> rpmList = new ArrayList<RiwayatProyekModel>();
		rpmList = this.riwayatProyekService.searchNotDelete(); //searchNotDelete
		model.addAttribute("riwayatProyekModelList", rpmList);
		//this.doListNoteType(model);
		return "/proyek/list";
	}
		
	@RequestMapping("/ubah")
	public String doUbah(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		//String id = request.getParameter("id");
		RiwayatProyekModel rpm = new RiwayatProyekModel();
		rpm = this.riwayatProyekService.searchIdProyek(id);
		model.addAttribute("proyekModel", rpm);
		doListTimePeriod(model);
		return "/proyek/edit";
	}
	
	@RequestMapping("/update") // belum selesai, modifiedBy masih manual
	public String doUpdate(HttpServletRequest req, Model model) {
		Long id = Long.parseLong(req.getParameter("id"));
		String startMonth = req.getParameter("startMonth");
		String startYear = req.getParameter("startYear");
		String projectName = req.getParameter("projectName");
		String projectDurationSTR = req.getParameter("projectDuration");
		String timePeriodIdSTR = req.getParameter("timePeriodId");
		Integer projectDuration;
		Long timePeriodId;
		if (projectDurationSTR.length() == 0) {	// if user inputting a null value of string "projectDuration"
			projectDuration = null;				// Integer.parse can't resolve string if null
		} else {
			projectDuration = Integer.parseInt(projectDurationSTR);
		}
		if (timePeriodIdSTR.length() == 0) {	// same goes for Long.parse
			timePeriodId = null;
		} else {
			timePeriodId =Long.parseLong(req.getParameter("timePeriodId"));
		}
		String client = req.getParameter("client");
		String projectPosition = req.getParameter("projectPosition");
		String description = req.getParameter("description");
		
		HttpSession session = req.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date createdOn = ts;
		
		RiwayatProyekModel rpm = new RiwayatProyekModel();
		rpm = this.riwayatProyekService.searchIdProyek(id);
		rpm.setStartMonth(startMonth);
		rpm.setStartYear(startYear);
		rpm.setProjectName(projectName);
		rpm.setProjectDuration(projectDuration);
		rpm.setTimePeriodId(timePeriodId);
		rpm.setClient(client);
		rpm.setProjectPosition(projectPosition);
		rpm.setDescription(description);
		rpm.setModifiedOn(createdOn);
		rpm.setModifiedBy(userRoleModel.getAddrbookld());
		this.riwayatProyekService.update(rpm);
		return "/pekerjaan/list";
	}
	
	@RequestMapping("/hapus")
	public String doHapus(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		RiwayatProyekModel rpm = new RiwayatProyekModel();
		rpm = this.riwayatProyekService.searchIdProyek(id);
		model.addAttribute("proyekModel", rpm);
		return "/proyek/delete";
	}

	@RequestMapping("/confirmdelete")
	public String doConfirmDelete(HttpServletRequest req, Model model) {
		Long idProyek = Long.parseLong(req.getParameter("id"));
		
		HttpSession session = req.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date deletedOn = ts;
		
		RiwayatProyekModel rpm = new RiwayatProyekModel();
		rpm = this.riwayatProyekService.searchIdProyek(idProyek);
		rpm.setIsDelete(true);
		rpm.setDeletedOn(deletedOn);
		rpm.setDeletedBy(userRoleModel.getAddrbookld());
		
		this.riwayatProyekService.update(rpm);
		return "/pekerjaan/list";
	}
	
	public void doListTimePeriod(Model model) {
		List<TimePeriodModel> tpList = new ArrayList<TimePeriodModel>();
		tpList = this.timePeriodService.read();
		model.addAttribute("timePeriodModelList", tpList);
	}
	
}
