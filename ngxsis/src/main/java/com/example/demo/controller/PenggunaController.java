package com.example.demo.controller;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.model.AddrBookModel;
import com.example.demo.model.BiodataModel;
import com.example.demo.model.RoleModel;
import com.example.demo.model.UserRoleModel;
import com.example.demo.repository.AddrBookRepository;
import com.example.demo.repository.BiodataRepository;
import com.example.demo.repository.PenggunaRepository;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRoleRepository;
import com.example.demo.service.BiodataService;
import com.example.demo.service.PenggunaService;

@Controller
public class PenggunaController {

	@Autowired
	private RoleRepository role;

	@Autowired
	private BiodataRepository data;

	@Autowired
	private AddrBookRepository addressbook;

	@Autowired
	private BiodataService biodataService;
	
	@Autowired
	private UserRoleRepository userRole;
	
	@Autowired
	private PenggunaService penggunaService;
	
	private PenggunaRepository user;

	@RequestMapping("/pengguna")
	public String doLoadUserPage(Model model) {
		List<RoleModel> roleModel = this.role.findIsNotDelete();
		model.addAttribute("roleModel", roleModel);
		return "/pengguna/index";
	}

	@RequestMapping(value="/pengguna/search",  method=RequestMethod.GET)
	public String search(HttpServletRequest request, Model model) {
		String search=request.getParameter("search");
		System.out.println(search);
		List<BiodataModel>  biodata= this.data.searchUser(search);
		model.addAttribute("biodata", biodata);
		return "/pengguna/dropdown-list";
	}
	
	

	@RequestMapping("/pengguna/simpan")
	public String update(HttpServletRequest request) throws NoSuchAlgorithmException {
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
		Long id = Long.parseLong(request.getParameter("id"));
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(id);
		List<UserRoleModel> userrole = new ArrayList<UserRoleModel>();
		Integer idCount = this.role.countRole();
		String fullname = request.getParameter("fullname");
		String email = request.getParameter("email");
		String abpwd = encryptPassword(request.getParameter("abpwd"));
		Boolean[] roleList = new Boolean[idCount];
		
		for (int i = 0; i < roleList.length; i++) {
			int j=1;
			roleList[i] = Boolean.parseBoolean(request.getParameter(String.valueOf(j)));
			
			j++;
		}
		setAddressBook(session, request, id, email, abpwd);
		setRole(session, id, roleList, email);
		return "";
	}

	public void setAddressBook(HttpSession session, HttpServletRequest request, Long id, String email, String abpwd)
			throws NoSuchAlgorithmException {
		UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(id);
		Date date = new Timestamp(System.currentTimeMillis());
		AddrBookModel user = new AddrBookModel();
		user.setEmail(email);
		user.setAbuid(email);
		user.setAbpwd(abpwd);
		user.setCreatedBy(userRoleModel.getId());
		user.setCreatedOn(date);
		user.setIsDelete(false);
		user.setIsLocked(false);
		this.addressbook.save(user);
	}

	public void setRole(HttpSession session, Long id, Boolean[] rolelist, String email) {
		UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(id);
		Date date = new Timestamp(System.currentTimeMillis());
		for (int i = 0; i < rolelist.length; i++) {
			Long j=1L;
			UserRoleModel uRole = this.userRole.findByUserRoleAndAddrBook(biodataModel.getAddrbookId(), j);
			uRole.setIsDelete(rolelist[i]);
			uRole.setModifiedBy(userRoleModel.getId());
			uRole.setModifiedOn(date);
			if (rolelist[i]==true) {
				uRole.setDeletedBy(userRoleModel.getId());
				uRole.setDeletedOn(date);
			} else {
				uRole.setDeletedBy(null);
				uRole.setDeletedOn(null);
			}
			j++;
		}
		
	}

	public String encryptPassword(String password) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] hashInBytes = md.digest(password.getBytes(StandardCharsets.UTF_8));

		StringBuilder sb = new StringBuilder();
		for (byte b : hashInBytes) {
			sb.append(String.format("%02x", b));
		}
		return sb.toString();
	}

}
