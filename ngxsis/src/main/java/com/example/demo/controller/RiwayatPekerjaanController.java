package com.example.demo.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.BiodataModel;
import com.example.demo.model.RiwayatPekerjaanModel;
import com.example.demo.model.RiwayatProyekModel;
import com.example.demo.model.TimePeriodModel;
import com.example.demo.model.UserRoleModel;
import com.example.demo.service.BiodataService;
import com.example.demo.service.RiwayatPekerjaanService;
import com.example.demo.service.RiwayatProyekService;
import com.example.demo.service.TimePeriodService;

@Controller
@RequestMapping("/pekerjaan")
public class RiwayatPekerjaanController {

	@Autowired
	RiwayatPekerjaanService riwayatPekerjaanService;
	
	@Autowired
	RiwayatProyekService riwayatProyekService;
	
	@Autowired
	BiodataService biodataService;

	@RequestMapping("/home")
	public String doHome(HttpServletRequest req, Model model) {
		Long id = Long.parseLong(req.getParameter("id"));
		BiodataModel bm = this.biodataService.searchIdBiodata(id);
		model.addAttribute("biodataModel", bm);
		return "/pekerjaan/home";
	}

	@RequestMapping("/tambah")
	public String doTambah(HttpServletRequest req, Model model) {
		Long idBiodata = Long.parseLong(req.getParameter("id"));
		BiodataModel bm = this.biodataService.searchIdBiodata(idBiodata);
		model.addAttribute("biodataModel", bm);
		return "/pekerjaan/add";
	}

	@RequestMapping("/create") // belum selesai, createdBy dan biodataId masih manual
	public String doCreate(HttpServletRequest req) {
		Long idBiodata = Long.parseLong(req.getParameter("id"));
		String companyName = req.getParameter("companyName");
		String city = req.getParameter("city");
		String country = req.getParameter("country");
		String joinMonth = req.getParameter("joinMonth");
		String joinYear = req.getParameter("joinYear");
		String resignMonth = req.getParameter("resignMonth");
		String resignYear = req.getParameter("resignYear");
		String lastPosition = req.getParameter("lastPosition");
		String income = req.getParameter("income");
		Boolean isItRelated = Boolean.parseBoolean(req.getParameter("isItRelated"));
		String aboutJob = req.getParameter("aboutJob");
		String exitReason = req.getParameter("exitReason");
		String notes = req.getParameter("notes");
		
		HttpSession session = req.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date createdOn = ts;

		RiwayatPekerjaanModel rpm = new RiwayatPekerjaanModel();
		rpm.setBiodataId(biodataModel.getId()); // berdasarkan biodataId
		rpm.setCompanyName(companyName);
		rpm.setCity(city);
		rpm.setCountry(country);
		rpm.setJoinMonth(joinMonth);
		rpm.setJoinYear(joinYear);
		rpm.setResignMonth(resignMonth);
		rpm.setResignYear(resignYear);
		rpm.setLastPosition(lastPosition);
		rpm.setIncome(income);
		rpm.setIsItRelated(isItRelated);
		rpm.setAboutJob(aboutJob);
		rpm.setExitReason(exitReason);
		rpm.setNotes(notes);
		rpm.setCreatedOn(createdOn);
		rpm.setCreatedBy(userRoleModel.getAddrbookld());
		rpm.setIsDelete(false);
		this.riwayatPekerjaanService.create(rpm);
		return "/pekerjaan/list"; // list
	}

	@RequestMapping("/data")
	public String doData(HttpServletRequest req, Model model) {
		Long idBiodata = Long.parseLong(req.getParameter("id"));
		BiodataModel bm = this.biodataService.searchIdBiodata(idBiodata);
		List<RiwayatPekerjaanModel> rpmList = new ArrayList<RiwayatPekerjaanModel>();
		rpmList = this.riwayatPekerjaanService.findByBiodataAndIsNotDelete(bm.getId());; // searchNotDelete
		this.doListProyek(model);
		model.addAttribute("pekerjaanList", rpmList);
		return "/pekerjaan/list";
	}

	public void doList(Model model) {
		List<RiwayatPekerjaanModel> omList = new ArrayList<RiwayatPekerjaanModel>();
		omList = this.riwayatPekerjaanService.read(); // searchNotDelete
		model.addAttribute("pekerjaanList", omList);
	}

	@RequestMapping("/detail")
	public String doDetail(HttpServletRequest req, Model model) {
		Long id = Long.parseLong(req.getParameter("id"));
		RiwayatPekerjaanModel om = new RiwayatPekerjaanModel();
		om = this.riwayatPekerjaanService.searchIdPekerjaan(id);
		model.addAttribute("pekerjaanModel", om);
		return "/pekerjaan/detail";
	}

	@RequestMapping("/ubah")
	public String doUbah(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		// String id = request.getParameter("id");
		RiwayatPekerjaanModel rpm = new RiwayatPekerjaanModel();
		rpm = this.riwayatPekerjaanService.searchIdPekerjaan(id);
		model.addAttribute("pekerjaanModel", rpm);
		return "/pekerjaan/edit";
	}

	@RequestMapping("/update") // belum selesai, modifiedBy masih manual
	public String doUpdate(HttpServletRequest req, Model model) {
		Long id = Long.parseLong(req.getParameter("id"));
		String companyName = req.getParameter("companyName");
		String city = req.getParameter("city");
		String country = req.getParameter("country");
		String joinMonth = req.getParameter("joinMonth");
		String joinYear = req.getParameter("joinYear");
		String resignMonth = req.getParameter("resignMonth");
		String resignYear = req.getParameter("resignYear");
		String lastPosition = req.getParameter("lastPosition");
		String income = req.getParameter("income");
		String isItRelatedSTR = req.getParameter("isItRelated");  // String yg didapat dapat berupa "true", "on", "null"
		Boolean isItRelated = Boolean.parseBoolean(req.getParameter("isItRelated"));
		if (isItRelatedSTR == "on") { // catch modified checkbox in html
			isItRelated = true;
		}
		//System.out.println(isItRelatedSTR);
		//System.out.println(isItRelated); 
		String aboutJob = req.getParameter("aboutJob");
		String exitReason = req.getParameter("exitReason");
		String notes = req.getParameter("notes");
		
		HttpSession session = req.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date modifiedOn = ts;

		RiwayatPekerjaanModel rpm = new RiwayatPekerjaanModel();
		rpm = this.riwayatPekerjaanService.searchIdPekerjaan(id);
		rpm.setCompanyName(companyName);
		rpm.setCity(city);
		rpm.setCountry(country);
		rpm.setJoinMonth(joinMonth);
		rpm.setJoinYear(joinYear);
		rpm.setResignMonth(resignMonth);
		rpm.setResignYear(resignYear);
		rpm.setLastPosition(lastPosition);
		rpm.setIncome(income);
		rpm.setIsItRelated(isItRelated);
		rpm.setAboutJob(aboutJob);
		rpm.setExitReason(exitReason);
		rpm.setNotes(notes);
		rpm.setModifiedOn(modifiedOn);
		rpm.setModifiedBy(userRoleModel.getAddrbookld());
		this.riwayatPekerjaanService.update(rpm);
		return "/pekerjaan/list";
	}

	@RequestMapping("/hapus")
	public String doHapus(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		RiwayatPekerjaanModel rpm = new RiwayatPekerjaanModel();
		rpm = this.riwayatPekerjaanService.searchIdPekerjaan(id);
		model.addAttribute("pekerjaanModel", rpm);
		return "/pekerjaan/delete";
	}

	@RequestMapping("/confirmdelete")
	public String doConfirmDelete(HttpServletRequest req, Model model) {
		Long id = Long.parseLong(req.getParameter("id"));
	
		HttpSession session = req.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date deletedOn = ts;
		
		RiwayatPekerjaanModel om = new RiwayatPekerjaanModel();
		om = this.riwayatPekerjaanService.searchIdPekerjaan(id);
		om.setIsDelete(true);
		om.setDeletedOn(deletedOn);
		om.setDeletedBy(userRoleModel.getAddrbookld());

		this.riwayatPekerjaanService.update(om);
		return "/pekerjaan/list";
	}
	
	public void doListProyek(Model model) {
		List<RiwayatProyekModel> rpmList = new ArrayList<RiwayatProyekModel>();
		rpmList = this.riwayatProyekService.searchNotDelete();
		model.addAttribute("proyekModel", rpmList);
	}
	

}
