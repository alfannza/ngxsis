package com.example.demo.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.BiodataModel;
import com.example.demo.model.RiwayatSertifikasiModel;
import com.example.demo.model.UserRoleModel;
import com.example.demo.service.BiodataService;
import com.example.demo.service.RiwayatSertifikasiService;

@Controller
@RequestMapping("/sertifikasi")
public class RiwayatSertifikasiController {
	
	@Autowired
	private RiwayatSertifikasiService riwayatSertifikasiService;

	@Autowired
	private BiodataService biodataService;
	
	@RequestMapping("/home")
	public String doHome(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(id);
		model.addAttribute("biodataModel", biodataModel);
		System.out.println(id);
		String page = "/sertifikasi/cert-index";
		return page;
	}
	
	@RequestMapping("/data")
	public String doData (HttpServletRequest request, Model model) {	
		Long idBiodata = Long.parseLong(request.getParameter("id"));
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);
		
		List<RiwayatSertifikasiModel> sertifikasiModel = new ArrayList<RiwayatSertifikasiModel>();
		sertifikasiModel = this.riwayatSertifikasiService.findByBiodataAndIsNotDelete(biodataModel.getId());
		model.addAttribute("sertifikasiModel", sertifikasiModel);
		System.out.println(idBiodata);
		
		String page ="/sertifikasi/cert-list";
		return page;
	}

	@RequestMapping("/add")
	public String doTambah(HttpServletRequest request,Model model) {
		Long idBiodata = Long.parseLong(request.getParameter("id"));
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);
		model.addAttribute("biodataModel", biodataModel);
		String page ="/sertifikasi/cert-add";
		return page;
	}

	@RequestMapping("/create")
	public String doCreate(HttpServletRequest request) {
		Long idBiodata = Long.parseLong(request.getParameter("id"));
		
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);
		Timestamp time = new Timestamp(System.currentTimeMillis());
		Date createdOn = time;
		
		RiwayatSertifikasiModel riwayatSertifikasiModel = new RiwayatSertifikasiModel();
		riwayatSertifikasiModel.setBiodataId(biodataModel.getId());
		riwayatSertifikasiModel.setCertificateName(request.getParameter("certificateName"));
		riwayatSertifikasiModel.setPublisher(request.getParameter("publisher"));
		riwayatSertifikasiModel.setValidStartMonth(request.getParameter("validStartMonth"));
		riwayatSertifikasiModel.setValidStartYear(request.getParameter("validStartYear"));
		riwayatSertifikasiModel.setUntilMonth(request.getParameter("untilMonth"));
		riwayatSertifikasiModel.setUntilYear(request.getParameter("untilYear"));
		riwayatSertifikasiModel.setNotes(request.getParameter("notes"));
		riwayatSertifikasiModel.setIsDelete(false);
		riwayatSertifikasiModel.setCreatedOn(createdOn);
		riwayatSertifikasiModel.setCreatedBy(userRoleModel.getAddrbookld());
		
		this.riwayatSertifikasiService.save(riwayatSertifikasiModel);
		
		String page = "/sertifikasi/cert-list";
		return page;
	}
	
	@RequestMapping("/ubah")
	public String doUbah (HttpServletRequest request, Model model) {	
		Long id = Long.valueOf(request.getParameter("id"));
		
		RiwayatSertifikasiModel riwayatsertifikasiModel = new RiwayatSertifikasiModel();
		riwayatsertifikasiModel = this.riwayatSertifikasiService.searchIdRiwayatSertifikasi(id);
		model.addAttribute("sertifikasiModel", riwayatsertifikasiModel);
		
		String page = "sertifikasi/cert-edit";
		return page;
	}
	
	@RequestMapping("/update")
	public String doUpdate(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Long id = Long.parseLong(request.getParameter("id"));
		Date modifiedOn = ts;
		
		RiwayatSertifikasiModel riwayatSertifikasiModel = new RiwayatSertifikasiModel();
		riwayatSertifikasiModel = this.riwayatSertifikasiService.searchIdRiwayatSertifikasi(id);
		String validStartMonth = request.getParameter("validStartMonth");
		String validStartYear = request.getParameter("validStartYear");
		String untilMonth = request.getParameter("untilMonth");
		String untilYear = request.getParameter("untilYear");
		if(validStartMonth.equalsIgnoreCase("-")) {
			untilMonth="-";
			untilYear="-";
		}
		riwayatSertifikasiModel.setCertificateName(request.getParameter("certificateName"));
		riwayatSertifikasiModel.setPublisher(request.getParameter("publisher"));
		riwayatSertifikasiModel.setValidStartMonth(validStartMonth);
		riwayatSertifikasiModel.setValidStartYear(validStartYear);
		riwayatSertifikasiModel.setUntilMonth(untilMonth);
		riwayatSertifikasiModel.setUntilYear(untilYear);
		riwayatSertifikasiModel.setNotes(request.getParameter("notes"));
		riwayatSertifikasiModel.setModifiedBy(userRoleModel.getAddrbookld());
		riwayatSertifikasiModel.setModifiedOn(modifiedOn);
		
		this.riwayatSertifikasiService.update(riwayatSertifikasiModel);
		
		String page = "/sertifikasi/cert-list";
		return page;
	}
	
	@RequestMapping("/hapus")
	public String doHapus(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		
		RiwayatSertifikasiModel riwayatsertifikasiModel = new RiwayatSertifikasiModel();
		riwayatsertifikasiModel = this.riwayatSertifikasiService.searchIdRiwayatSertifikasi(id);
		model.addAttribute("sertifikasiModel", riwayatsertifikasiModel);
		
		String page = "/sertifikasi/cert-delete";
		return page;
	}
	
	@RequestMapping("/delete")
	public String doDelete(HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		Timestamp time = new Timestamp(System.currentTimeMillis());
		Date deletedOn = time;
		
		RiwayatSertifikasiModel riwayatsertifikasiModel = new RiwayatSertifikasiModel();
		riwayatsertifikasiModel = this.riwayatSertifikasiService.searchIdRiwayatSertifikasi(id);
		riwayatsertifikasiModel.setIsDelete(true);
		riwayatsertifikasiModel.setDeletedBy(userRoleModel.getAddrbookld());
		riwayatsertifikasiModel.setDeletedOn(deletedOn);
		
		this.riwayatSertifikasiService.update(riwayatsertifikasiModel);
		
		String page = "/sertifikasi/cert-list";
		return page;
	}
	
	
	
}
