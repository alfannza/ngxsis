package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/rencana")
public class RencanaController {

	@RequestMapping("/home")
	public String getHome() {
		return "/rencana/index";
	}
}
