package com.example.demo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.SumberLokerModel;

@Controller
@RequestMapping("/sumberloker")
public class SumberLokerController {

	@RequestMapping("/home")
	public String doHome() {
		SumberLokerModel sumberLokerModel = new SumberLokerModel();
		
		
		
		return "/sumberloker/home";
	}
	
	@RequestMapping("/create")
	public String doCreate(HttpServletRequest request) {
		return "/sumberloker/home";
	}
}
