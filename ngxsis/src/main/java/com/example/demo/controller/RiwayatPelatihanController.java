package com.example.demo.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.model.BiodataModel;
//import com.example.demo.model.BiodataModel;
import com.example.demo.model.RiwayatPelatihanModel;
import com.example.demo.model.TimePeriodModel;
import com.example.demo.model.UserRoleModel;
import com.example.demo.service.BiodataService;
//import com.example.demo.repository.RiwayatPelatihanRepository;
//import com.example.demo.repository.TimePeriodRepository;
//import com.example.demo.model.TimePeriod;
//import com.example.demo.repository.BiodataRepository;
import com.example.demo.service.RiwayatPelatihanService;
import com.example.demo.service.TimePeriodService;

@Controller
@RequestMapping("/pelatihan")
public class RiwayatPelatihanController {
	
	@Autowired
	private RiwayatPelatihanService riwayatPelatihanService;

	@Autowired
	private TimePeriodService timePeriodService;
	
	@Autowired
	private BiodataService biodataService;
	
	@RequestMapping("/home")
	public String doHome(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(id);
		model.addAttribute("biodataModel", biodataModel);
		
		String page = "/pelatihan/home";
		return page;
	}
	
//	@RequestMapping(value="/detail/{id}")
//	public String doDetail (Model model, @PathVariable(name="id") Long id) {
	
	@RequestMapping("/data")
	public String doData (HttpServletRequest request, Model model) {
//		BiodataModel item = biodataRepository.findById(id).orElse(null);
//		model.addAttribute("item", item);
		
		Long idBiodata = Long.parseLong(request.getParameter("id"));
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);
		
		List<RiwayatPelatihanModel> pelatihanModel = new ArrayList<RiwayatPelatihanModel>();
		pelatihanModel = this.riwayatPelatihanService.findByBiodataAndIsNotDelete(biodataModel.getId());
		model.addAttribute("pelatihanModel", pelatihanModel);
		
		String page ="/pelatihan/list";
		return page;
	}
	
	@RequestMapping("/detail")
	public String doDetail(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		RiwayatPelatihanModel riwayatPelatihanModel = new RiwayatPelatihanModel();
		riwayatPelatihanModel = this.riwayatPelatihanService.searchIdRiwayatPelatihan(id);
		model.addAttribute("pelatihanModel", riwayatPelatihanModel);
		
		return "/pelatihan/detail";
	}
	
//	@RequestMapping(value="/tambah/{id}")
//	public String doTambah(Model model, @PathVariable(name="id") Long id) {
	@RequestMapping("/add")
	public String doTambah(HttpServletRequest request,Model model) {
		/*
		 * List<TimePeriod> data1 = new ArrayList<TimePeriod>(); data1 =
		 * this.riwayatPelatihanService.caridata1(); model.addAttribute("data1", data1);
		 * 
		 * BiodataModel item = biodataRepository.findById(id).orElse(null);
		 * model.addAttribute("item", item);
		 * 
		 * List<BiodataModel> data2 = new ArrayList<BiodataModel>(); data2 =
		 * this.riwayatPelatihanService.caridata2(); model.addAttribute("data2", data2);
		 */
		Long idBiodata = Long.parseLong(request.getParameter("id"));
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);
		
		this.doTimePeriod(model);
		
		model.addAttribute("biodataModel", biodataModel);
		String page ="/pelatihan/add";
		return page;
	}
	
	public void doTimePeriod(Model model) {
		List<TimePeriodModel> timePeriod = new ArrayList<TimePeriodModel>();
		timePeriod = this.timePeriodService.read();
		model.addAttribute("timePeriod", timePeriod);
	}
	
//	@RequestMapping(value="/create", method=RequestMethod.POST)
	@RequestMapping("/create")
	public String doCreate(HttpServletRequest request) {
		Long idBiodata = Long.parseLong(request.getParameter("id"));
		String trainingName = request.getParameter("trainingName");
		String organizer = request.getParameter("organizer");
		String trainingYear = request.getParameter("trainingYear");
		String trainingMonth = request.getParameter("trainingMonth");
		Integer trainingDuration = Integer.parseInt(request.getParameter("trainingDuration"));
		Long timePeriodId = Long.parseLong(request.getParameter("timePeriodId"));
		String city = request.getParameter("city");
		String country = request.getParameter("country");
		String notes = request.getParameter("notes");
		
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);
		Timestamp time = new Timestamp(System.currentTimeMillis());
		Date createdOn = time;
		
		RiwayatPelatihanModel riwayatPelatihanModel = new RiwayatPelatihanModel();
		riwayatPelatihanModel.setBiodataId(biodataModel.getId());
		riwayatPelatihanModel.setTrainingName(trainingName);
		riwayatPelatihanModel.setOrganizer(organizer);
		riwayatPelatihanModel.setTrainingYear(trainingYear);
		riwayatPelatihanModel.setTrainingMonth(trainingMonth);
		riwayatPelatihanModel.setTrainingDuration(trainingDuration);
		riwayatPelatihanModel.setTimePeriodId(timePeriodId);
		riwayatPelatihanModel.setCity(city);
		riwayatPelatihanModel.setCountry(country);
		riwayatPelatihanModel.setNotes(notes);
		riwayatPelatihanModel.setIsDelete(false);
		riwayatPelatihanModel.setCreatedOn(createdOn);
		riwayatPelatihanModel.setCreatedBy(userRoleModel.getAddrbookld());
		
		this.riwayatPelatihanService.save(riwayatPelatihanModel);
		
		String page = "/pelatihan/list";
		return page;
	}
	
//	@RequestMapping(value="/edit/{id}", method = RequestMethod.GET)
//	public String doEdit(Model model, @PathVariable(name = "id") Long id) {
	
	@RequestMapping("/ubah")
	public String doUbah (HttpServletRequest request, Model model) {
//		RiwayatPelatihanModel item = riwayatPelatihanRepository.findAllById(id).erElse(null);
//		model.addAttribute("item", item);
		
		Long id = Long.valueOf(request.getParameter("id"));
		
		RiwayatPelatihanModel riwayatPelatihanModel = new RiwayatPelatihanModel();
		riwayatPelatihanModel = this.riwayatPelatihanService.searchIdRiwayatPelatihan(id);
		this.doTimePeriod(model);
		model.addAttribute("pelatihanModel", riwayatPelatihanModel);
		
		String page = "pelatihan/edit";
		return page;
	}
	
	@RequestMapping("/update")
	public String doUpdate(HttpServletRequest request) {
		Long id = Long.valueOf(request.getParameter("id"));
		String trainingName = request.getParameter("trainingName");
		String organizer = request.getParameter("organizer");
		String trainingYear = request.getParameter("trainingYear");
		String trainingMonth = request.getParameter("trainingMonth");
		Integer trainingDuration = Integer.valueOf(request.getParameter("trainingDuration"));
		Long timePeriodId = Long.valueOf(request.getParameter("timePeriodId"));
		String city = request.getParameter("city");
		String country = request.getParameter("country");
		String notes = request.getParameter("notes");
		
		//input manual
		
//		Long biodataId = (long) 1;
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		Timestamp time = new Timestamp(System.currentTimeMillis());
		Date modifiedOn = time;
		
		RiwayatPelatihanModel riwayatPelatihanModel = new RiwayatPelatihanModel();
		riwayatPelatihanModel = this.riwayatPelatihanService.searchIdRiwayatPelatihan(id);
		riwayatPelatihanModel.setTrainingName(trainingName);
		riwayatPelatihanModel.setOrganizer(organizer);
		riwayatPelatihanModel.setTrainingYear(trainingYear);
		riwayatPelatihanModel.setTrainingMonth(trainingMonth);
		riwayatPelatihanModel.setTrainingDuration(trainingDuration);
		riwayatPelatihanModel.setTimePeriodId(timePeriodId);
		riwayatPelatihanModel.setCity(city);
		riwayatPelatihanModel.setCountry(country);
		riwayatPelatihanModel.setNotes(notes);
		riwayatPelatihanModel.setIsDelete(false);
		riwayatPelatihanModel.setModifiedBy(userRoleModel.getAddrbookld());
		riwayatPelatihanModel.setModifiedOn(modifiedOn);
		
		this.riwayatPelatihanService.update(riwayatPelatihanModel);
		
		String page = "/pelatihan/list";
		return page;
	}
	
	@RequestMapping("/hapus")
	public String doHapus(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		
		RiwayatPelatihanModel riwayatPelatihanModel = new RiwayatPelatihanModel();
		riwayatPelatihanModel = this.riwayatPelatihanService.searchIdRiwayatPelatihan(id);
		model.addAttribute("pelatihanModel", riwayatPelatihanModel);
		
		String page = "/pelatihan/delete";
		return page;
	}
	
	@RequestMapping("/doHapus")
	public String doDelete(HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		Timestamp time = new Timestamp(System.currentTimeMillis());
		Date deletedOn = time;
		
		RiwayatPelatihanModel riwayatPelatihanModel = new RiwayatPelatihanModel();
		riwayatPelatihanModel = this.riwayatPelatihanService.searchIdRiwayatPelatihan(id);
		riwayatPelatihanModel.setIsDelete(true);
		riwayatPelatihanModel.setDeletedBy(userRoleModel.getAddrbookld());
		riwayatPelatihanModel.setDeletedOn(deletedOn);
		
		this.riwayatPelatihanService.update(riwayatPelatihanModel);
		
		String page = "/pelatihan/list";
		return page;
	}
	
	
	
}
