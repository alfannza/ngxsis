package com.example.demo.controller;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.model.AddressModel;
import com.example.demo.model.BiodataModel;
import com.example.demo.model.IdentityTypeModel;
import com.example.demo.model.MaritalStatusModel;
import com.example.demo.model.ReligionModel;
import com.example.demo.model.RoleModel;
import com.example.demo.model.UserRoleModel;
import com.example.demo.service.AddressService;
import com.example.demo.service.BiodataService;
import com.example.demo.service.IdentityTypeService;
import com.example.demo.service.MaritalStatusService;
import com.example.demo.service.ReligionService;

@Controller
@RequestMapping("/pelamar")
public class PelamarController {
	
	private Integer page = 0;
	private Integer element = 10;
	private Boolean ascdesc = true;

	@Autowired
	private BiodataService biodataService;

	@Autowired
	private AddressService addressService;

	@Autowired
	private ReligionService religionService;

	@Autowired
	private MaritalStatusService maritalStatusService;

	@Autowired
	private IdentityTypeService identityTypeService;

	@RequestMapping("/home")
	public String doHome() {
		return "/pelamar/index";
	}

	@RequestMapping("/data")
	public String doList(Model model) {
		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();
		biodataModelList = this.biodataService.searchNotDelete();
		model.addAttribute("biodataModelList", biodataModelList);

		return "/pelamar/list";
	}

	@RequestMapping("/tambah")
	public String doAdd(Model model) {
		this.doListAgama(model);
		this.doListStatus(model);
		this.doListIdentitas(model);
		return "/pelamar/add";
	}
	
	@RequestMapping("/proses")
	public String doProcess(Model model) {
		this.doListAgama(model);
		this.doListStatus(model);
		this.doListIdentitas(model);
		return "/pelamar/process";
	}

	@RequestMapping("/add-pelamar")
	public @ResponseBody String doCreate(HttpServletRequest req, Model model) throws ParseException {
		String fullname = req.getParameter("fullname");
		String nickName = req.getParameter("nickName");
		String pob = req.getParameter("pob");
		String dob = req.getParameter("dob");
		Boolean gender = Boolean.parseBoolean(req.getParameter("gender"));
		Long religionId = Long.parseLong(req.getParameter("religionId"));
		Integer high = Integer.parseInt(req.getParameter("high"));
		Integer weight = Integer.parseInt(req.getParameter("weight"));
		String nationality = req.getParameter("nationality");
		String ethnic = req.getParameter("ethnic");
		String hobby = req.getParameter("hobby");
		Long identityTypeId = Long.parseLong(req.getParameter("identityTypeId"));
		String identityNo = req.getParameter("identityNo");
		String email = req.getParameter("email");
		String phoneNumber1 = req.getParameter("phoneNumber1");
		String phoneNumber2 = req.getParameter("phoneNumber2");
		String parentPhoneNumber = req.getParameter("parentPhoneNumber");
		String childSequence = req.getParameter("childSequence");
		String howManyBrothers = req.getParameter("howManyBrothers");
		Long maritalStatusId = Long.parseLong(req.getParameter("maritalStatusId"));
		String marriageYear = req.getParameter("marriageYear");

		String address1 = req.getParameter("address1");
		String postalCode1 = req.getParameter("postalCode1");
		String rt1 = req.getParameter("rt1");
		String rw1 = req.getParameter("rw1");
		String kelurahan1 = req.getParameter("kelurahan1");
		String kecamatan1 = req.getParameter("kecamatan1");
		String region1 = req.getParameter("region1");

		String address2 = req.getParameter("address2");
		String postalCode2 = req.getParameter("postalCode2");
		String rt2 = req.getParameter("rt2");
		String rw2 = req.getParameter("rw2");
		String kelurahan2 = req.getParameter("kelurahan2");
		String kecamatan2 = req.getParameter("kecamatan2");
		String region2 = req.getParameter("region2");

		BiodataModel biodataModel = new BiodataModel();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date1 = format.parse(dob);
		
		HttpSession session = req.getSession(false); UserRoleModel userRoleModel =
				  (UserRoleModel)session.getAttribute("userRoleModel");
				  
				  Timestamp ts = new Timestamp(System.currentTimeMillis()); Date createdOn =
				  ts;

		biodataModel.setFullname(fullname);
		biodataModel.setNickName(nickName);
		biodataModel.setPob(pob);
		biodataModel.setDob(date1);
		biodataModel.setGender(gender);
		biodataModel.setReligionId(religionId);
		biodataModel.setHigh(high);
		biodataModel.setWeight(weight);
		biodataModel.setNationality(nationality);
		biodataModel.setEthnic(ethnic);
		biodataModel.setHobby(hobby);
		biodataModel.setIdentityTypeId(identityTypeId);
		biodataModel.setIdentityNo(identityNo);
		biodataModel.setEmail(email);
		biodataModel.setPhoneNumber1(phoneNumber1);
		biodataModel.setPhoneNumber2(phoneNumber2);
		biodataModel.setParentPhoneNumber(parentPhoneNumber);
		biodataModel.setChildSequence(childSequence);
		biodataModel.setHowManyBrothers(howManyBrothers);
		biodataModel.setMaritalStatusId(maritalStatusId);
		biodataModel.setMarriageYear(marriageYear);

		biodataModel.setCreatedBy((long) 1);
		biodataModel.setCreatedOn(createdOn);
		biodataModel.setIsDelete(false);
		biodataModel.setCompanyId((long) 1);

		AddressModel addressModel = new AddressModel();
		addressModel.setAddress1(address1);
		addressModel.setPostalCode1(postalCode1);
		addressModel.setRt1(rt1);
		addressModel.setRw1(rw1);
		addressModel.setKelurahan1(kelurahan1);
		addressModel.setKecamatan1(kecamatan1);
		addressModel.setRegion1(region1);

		addressModel.setAddress1(address2);
		addressModel.setPostalCode1(postalCode2);
		addressModel.setRt2(rt2);
		addressModel.setRw2(rw2);
		addressModel.setKelurahan2(kelurahan2);
		addressModel.setKecamatan2(kecamatan2);
		addressModel.setRegion2(region2);

		addressModel.setCreatedBy((long) 1);
		addressModel.setCreatedOn(createdOn);
		addressModel.setIsDelete(false);
		addressModel.setBiodataId((long) 1);
		addressModel.setName("a");

		this.addressService.create(addressModel);

		String message = " ";
		message = this.biodataService.create(biodataModel);
		String page = " ";
		if (message.equals(" ")) {
			page = "/pelamar/home";

		} else {
			page = "/pelamar/add";
			model.addAttribute("message", message); 
		}

		return message;
	}

	public void doListAgama(Model model) {
		List<ReligionModel> religionModelList = new ArrayList<ReligionModel>();
		religionModelList = this.religionService.read();
		model.addAttribute("religionModelList", religionModelList);
	}

	public void doListStatus(Model model) {
		List<MaritalStatusModel> maritalStatusModelList = new ArrayList<MaritalStatusModel>();
		maritalStatusModelList = this.maritalStatusService.read();
		model.addAttribute("maritalStatusModelList", maritalStatusModelList);
	}

	public void doListIdentitas(Model model) {
		List<IdentityTypeModel> identityTypeModelList = new ArrayList<IdentityTypeModel>();
		identityTypeModelList = this.identityTypeService.read();
		model.addAttribute("identityTypeModelList", identityTypeModelList);
	}

	@RequestMapping("/search/nama")
	public String doSearchNama(HttpServletRequest request, Model model) {
		String fullname = request.getParameter("fullname");
		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();
		biodataModelList = this.biodataService.searchNamaPelamar(fullname);
		model.addAttribute("biodataModelList", biodataModelList);

		return "/pelamar/list";
	}
	
	@RequestMapping("/doAscending")
	public String ascPelamar(HttpServletRequest req, Model model) {
		List<BiodataModel> biodataModelListDummy = new ArrayList<BiodataModel>();
		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();		
		biodataModelListDummy = this.biodataService.ascPelamar();
		this.ascdesc = true;
		
		Integer start = this.page * this.element;
		Integer end = start + this.element;

		for (int i = start; i < end; i++) {
			try {
				biodataModelList.add(biodataModelListDummy.get(i));

			} catch (Exception e) {
				// TODO: handle exception
				break;
			}

		}

		model.addAttribute("biodataModelList", biodataModelList);
		return "/pelamar/list";
	}
	
	@RequestMapping("/doDescending")
	public String descPelamar(HttpServletRequest req, Model model) {
		List<BiodataModel> biodataModelListDummy = new ArrayList<BiodataModel>();
		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();		
		biodataModelListDummy = this.biodataService.descPelamar();
		this.ascdesc = false;
		
		Integer start = this.page * this.element;
		Integer end = start + this.element;

		for (int i = start; i < end; i++) {
			try {
				biodataModelList.add(biodataModelListDummy.get(i));

			} catch (Exception e) {
				// TODO: handle exception
				break;
			}

		}

		model.addAttribute("biodataModelList", biodataModelList);
		return "/pelamar/list";
	}
	
	@RequestMapping("/jumlahList")
	public String doJumlahList(HttpServletRequest request, Model model) {

		Integer jumlahList = Integer.parseInt(request.getParameter("element"));
		this.element = jumlahList;
		List<BiodataModel> biodataModelListDummy = new ArrayList<BiodataModel>();
		List<BiodataModel> biodataModelList = new ArrayList<BiodataModel>();
		biodataModelListDummy = this.biodataService.ascPelamar();

		for (int i = 0; i < jumlahList; i++) {
			try {
				biodataModelList.add(biodataModelListDummy.get(i));

			} catch (Exception e) {
				// TODO: handle exception
				break;
			}

		}

		model.addAttribute("biodataModelList", biodataModelList);
		return "/pelamar/list";
	}
	
//	do detail
	
	@RequestMapping("/doDetail")
	public String doDetail(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(id);
		model.addAttribute("biodataModel", biodataModel);
		
		return "/pelamar/detailPelamar";
	}
	
	
}
