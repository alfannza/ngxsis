package com.example.demo.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.BiodataModel;
import com.example.demo.model.CatatanModel;
import com.example.demo.model.NoteTypeModel;
import com.example.demo.model.UserRoleModel;
import com.example.demo.service.BiodataService;
import com.example.demo.service.CatatanService;
import com.example.demo.service.NoteTypeService;

@Controller
@RequestMapping("/catatan")
public class CatatanController {

	@Autowired
	CatatanService catatanService;
	
	@Autowired
	BiodataService biodataService;
	
	@Autowired
	NoteTypeService noteTypeService;
	
	@RequestMapping("/home")
	public String doHome(HttpServletRequest req, Model model) {
		Long id = Long.parseLong(req.getParameter("id"));
		BiodataModel bm = this.biodataService.searchIdBiodata(id);
		model.addAttribute("biodataModel", bm);
		return "/catatan/home";
	}
	
	@RequestMapping("/add") //tambah
	public String doTambah(HttpServletRequest req, Model model) {
		Long idBiodata = Long.parseLong(req.getParameter("id"));
		BiodataModel bm = this.biodataService.searchIdBiodata(idBiodata);
		this.doListNoteType(model);
		model.addAttribute("biodataModel", bm);
		return "/catatan/add";
	}
	
	@RequestMapping("/create") // belum selesai, createdBy dan biodataId masih manual
	public String doCreate(HttpServletRequest req) {
		Long idBiodata = Long.parseLong(req.getParameter("id"));
		String title = req.getParameter("title");
		String notes = req.getParameter("notes");
		Long noteTypeId = Long.parseLong(req.getParameter("noteTypeId"));
		
		HttpSession session = req.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(idBiodata);
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date createdOn = ts;
		
		CatatanModel cm = new CatatanModel();
		cm.setBiodataId(biodataModel.getId());
		cm.setTitle(title);
		cm.setNotes(notes);
		cm.setNoteTypeId(noteTypeId);
		cm.setIsDelete(false);
		cm.setCreatedOn(createdOn);
		cm.setCreatedBy(userRoleModel.getAddrbookld());
		this.catatanService.create(cm);
		return "/catatan/list"; //list home
	}

	@RequestMapping("/data")
	public String doData(HttpServletRequest req, Model model) {
		Long idBiodata = Long.parseLong(req.getParameter("id"));
		BiodataModel bm = this.biodataService.searchIdBiodata(idBiodata);
		List<CatatanModel> cmList = new ArrayList<CatatanModel>();
		cmList = this.catatanService.findByBiodataAndIsNotDelete(bm.getId());
		model.addAttribute("catatanModelList", cmList);
		//this.doListNoteType(model);
		return "/catatan/list";
	}
	
	public void doList(Model model) {
		List<CatatanModel> cmList = new ArrayList<CatatanModel>();
		cmList = this.catatanService.read(); //searchNotDelete
		model.addAttribute("catatanModelList", cmList);
	}
		
	@RequestMapping("/detail")
	public String doDetail(HttpServletRequest req, Model model) {
		Long id = Long.parseLong(req.getParameter("id"));
		CatatanModel cm = new CatatanModel();
		cm = this.catatanService.searchIdCatatan(id);
		model.addAttribute("catatanModel", cm);
		return "/catatan/detail";
	}
	
	@RequestMapping("/ubah")
	public String doUbah(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		//String id = request.getParameter("id");
		CatatanModel cm = new CatatanModel();
		cm = this.catatanService.searchIdCatatan(id);
		this.doListNoteType(model);
		model.addAttribute("catatanModel", cm);
		return "/catatan/edit";
	}
	
	@RequestMapping("/update") // belum selesai, modifiedBy masih manual
	public String doUpdate(HttpServletRequest req, Model model) {
		Long id = Long.parseLong(req.getParameter("id"));
		Long noteTypeId = Long.parseLong(req.getParameter("noteTypeId"));
		String title = req.getParameter("title");
		String notes = req.getParameter("notes");
		
		HttpSession session = req.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date modifiedOn = ts;
		
		CatatanModel cm = new CatatanModel();
		cm = this.catatanService.searchIdCatatan(id);
		cm.setNoteTypeId(noteTypeId);
		cm.setTitle(title);
		cm.setNotes(notes);
		cm.setModifiedOn(modifiedOn);
		cm.setModifiedBy(userRoleModel.getAddrbookld());
		this.catatanService.update(cm);
		return "/catatan/list";
	}
	
	@RequestMapping("/hapus")
	public String doHapus(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		CatatanModel cm = new CatatanModel();
		cm = this.catatanService.searchIdCatatan(id);
		model.addAttribute("catatanModel", cm);
		return "/catatan/delete";
	}

	@RequestMapping("/confirmdelete")
	public String doConfirmDelete(HttpServletRequest req, Model model) {
		Long id = Long.parseLong(req.getParameter("id"));

		HttpSession session = req.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date deletedOn = ts;
		
		CatatanModel cm = new CatatanModel();
		cm = this.catatanService.searchIdCatatan(id);
		cm.setIsDelete(true);
		cm.setDeletedOn(deletedOn);
		cm.setDeletedBy(userRoleModel.getAddrbookld());
		
		this.catatanService.update(cm);
		return "/catatan/list";
	}
	
	public void doListNoteType(Model model) {
		List<NoteTypeModel> ntList = new ArrayList<NoteTypeModel>();
		ntList = this.noteTypeService.read();
		model.addAttribute("noteTypeModelList", ntList);
	}
	
}
