package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.BiodataAttachmentModel;
import com.example.demo.model.BiodataModel;
import com.example.demo.model.KeahlianModel;
import com.example.demo.model.PeReferensiModel;
import com.example.demo.model.RiwayatPekerjaanModel;
import com.example.demo.model.RiwayatPelatihanModel;
import com.example.demo.model.RiwayatPendidikanModel;
import com.example.demo.model.RiwayatSertifikasiModel;
import com.example.demo.model.SkillLevelModel;
import com.example.demo.service.BiodataAttachmentService;
import com.example.demo.service.BiodataService;
import com.example.demo.service.KeahlianService;
import com.example.demo.service.PeReferensiService;
import com.example.demo.service.RiwayatPekerjaanService;
import com.example.demo.service.RiwayatPelatihanService;
import com.example.demo.service.RiwayatPendidikanService;
import com.example.demo.service.RiwayatSertifikasiService;
import com.example.demo.service.SkillLevelService;

@Controller
@RequestMapping("/lihat")
public class ProfilControl {

	@Autowired
	BiodataService biodataService;
	
	@Autowired
	PeReferensiService peReferensiService;
	
	@Autowired
	RiwayatPendidikanService riwayatPendidikanService;
	
	@Autowired
	RiwayatPekerjaanService riwayatPekerjaanService;
	
	@Autowired
	KeahlianService keahlianService;
	
	@Autowired
	SkillLevelService skillLevelService;
	
	@Autowired
	BiodataAttachmentService biodataAttachmentService;
	
	@Autowired
	RiwayatPelatihanService riwayatPelatihanService;
	
	@Autowired
	RiwayatSertifikasiService riwayatSertifikasiService;

	@RequestMapping("/pelamar")
	public String doProfil(HttpServletRequest request, Model model) {
		Long biodataId = Long.parseLong(request.getParameter("id"));
		BiodataModel biodataModel = this.biodataService.searchIdBiodata(biodataId);
		model.addAttribute("biodataModel", biodataModel);
		this.doAddPekerjaan(request, model);
		this.doAddPendidikanTerahir(request, model);
		this.doAddPendidikanList(request, model);
		this.doAddReferensi(request, model);
		this.doAddKeahlian1(request, model);
		this.doAddKeahlian2(request, model);
		this.doAddKeahlian3(request, model);
		this.doAddSkillLevel(request, model);
		this.doAddBiodataAttachment(request, model);
		this.doAddPelatihan(request, model);
		this.doAddSertifikasi(request, model);
		String page = "/profil/profil";
		return page;
	}

	public void doAddReferensi(HttpServletRequest request, Model model) {
		PeReferensiModel peReferensiModel = new PeReferensiModel();
		Long biodataId = Long.parseLong(request.getParameter("id"));
		peReferensiModel = this.peReferensiService.searchOnePeReferensiByBiodataId(biodataId);
		model.addAttribute("peReferensiModel", peReferensiModel);
	}
	
	public void doAddPendidikanTerahir(HttpServletRequest request, Model model) {
		List<RiwayatPendidikanModel> riwayatPendidikanModelLast = new ArrayList<RiwayatPendidikanModel>();
		Long biodataId = Long.parseLong(request.getParameter("id"));
		riwayatPendidikanModelLast = this.riwayatPendidikanService.searchIdPendidikanTerahir(biodataId);
		model.addAttribute("riwayatPendidikanModelLast", riwayatPendidikanModelLast.get(0));
	}

	public void doAddPendidikanList(HttpServletRequest request, Model model) {
		List<RiwayatPendidikanModel> riwayatPendidikanModels = new ArrayList<RiwayatPendidikanModel>();
		Long biodataId = Long.parseLong(request.getParameter("id"));
		riwayatPendidikanModels = this.riwayatPendidikanService.searchPendidikanByBiodataId(biodataId);
		model.addAttribute("riwayatPendidikanModels", riwayatPendidikanModels);
	}

	public void doAddPekerjaan(HttpServletRequest request, Model model) {
		List<RiwayatPekerjaanModel> riwayatPekerjaanModels = new ArrayList<RiwayatPekerjaanModel>();
		Long biodataId = Long.parseLong(request.getParameter("id"));
		riwayatPekerjaanModels = this.riwayatPekerjaanService.searchPekerjaanByBiodataId(biodataId);
		model.addAttribute("riwayatPekerjaanModels", riwayatPekerjaanModels);
	}

	public void doAddKeahlian1(HttpServletRequest request, Model model) {
		List<KeahlianModel> keahlianModels = new ArrayList<KeahlianModel>();
		Long biodataId = Long.parseLong(request.getParameter("id"));
		Long skillLevelId= (long) 1;
		keahlianModels = this.keahlianService.searchKeahlianByBiodataId(biodataId, skillLevelId);
		model.addAttribute("keahlianModels", keahlianModels);
	}

	public void doAddKeahlian2(HttpServletRequest request, Model model) {
		List<KeahlianModel> keahlianModelss = new ArrayList<KeahlianModel>();
		Long biodataId = Long.parseLong(request.getParameter("id"));
		Long skillLevelId= (long) 2;
		keahlianModelss = this.keahlianService.searchKeahlianByBiodataId(biodataId, skillLevelId);
		model.addAttribute("keahlianModelss", keahlianModelss);
	}

	public void doAddKeahlian3(HttpServletRequest request, Model model) {
		List<KeahlianModel> keahlianModelsss = new ArrayList<KeahlianModel>();
		Long biodataId = Long.parseLong(request.getParameter("id"));
		Long skillLevelId= (long) 3;
		keahlianModelsss = this.keahlianService.searchKeahlianByBiodataId(biodataId, skillLevelId);
		model.addAttribute("keahlianModelsss", keahlianModelsss);
	}

	public void doAddSkillLevel(HttpServletRequest request, Model model) {
		List<SkillLevelModel> skillLevelModels = new ArrayList<SkillLevelModel>();
		skillLevelModels = this.skillLevelService.read();
		model.addAttribute("skillLevelModels", skillLevelModels);
	}

	public void doAddBiodataAttachment(HttpServletRequest request, Model model) {
		BiodataAttachmentModel biodataAttachmentModel = new BiodataAttachmentModel();
		Long biodataId = Long.parseLong(request.getParameter("id"));
		biodataAttachmentModel = this.biodataAttachmentService.searchByBiodataId(biodataId);
		model.addAttribute("biodataAttachmentModel", biodataAttachmentModel);
	}

	public void doAddPelatihan(HttpServletRequest request, Model model) {
		List<RiwayatPelatihanModel> riwayatPelatihanModels = new ArrayList<RiwayatPelatihanModel>();
		Long biodataId = Long.parseLong(request.getParameter("id"));
		riwayatPelatihanModels = this.riwayatPelatihanService.searchRiwayatPelatihanByBiodataId(biodataId);
		model.addAttribute("riwayatPelatihanModels", riwayatPelatihanModels);
	}

	public void doAddSertifikasi(HttpServletRequest request, Model model) {
		List<RiwayatSertifikasiModel> riwayatSertifikasiModels = new ArrayList<RiwayatSertifikasiModel>();
		Long biodataId = Long.parseLong(request.getParameter("id"));
		model.addAttribute("riwayatSertifikasiModels", riwayatSertifikasiModels);
	}

}
