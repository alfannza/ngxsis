package com.example.demo.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.AddrBookModel;
import com.example.demo.model.UserRoleModel;
import com.example.demo.service.AddrBookService;

@Controller
@RequestMapping("/ubah")
public class UbahPasswordController {

	@Autowired
	private AddrBookService addrBookService;
	
	@RequestMapping("/pass")
	public String doLogin() {
		String page = "/ubahPassword/ubahSandi";
		return page;
	}
	
	@RequestMapping("/update")
	public String doUpdate(HttpServletRequest request) {
		HttpSession session=request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
		Long id = userRoleModel.getAddrbookld();
		String abpwd = request.getParameter("newPassword");
		AddrBookModel addrBookModel = new AddrBookModel();
		addrBookModel = this.addrBookService.findById(id);
		addrBookModel.setAbpwd(abpwd);
		this.addrBookService.update(addrBookModel);
		request.getSession().invalidate();
		String page = "/login/login";
		return page;	
	}
}
