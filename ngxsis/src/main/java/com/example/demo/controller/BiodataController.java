package com.example.demo.controller;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.example.demo.model.AddressModel;
import com.example.demo.model.BiodataModel;
import com.example.demo.model.IdentityTypeModel;
import com.example.demo.model.MaritalStatusModel;
import com.example.demo.model.ReligionModel;
import com.example.demo.service.AddressService;
import com.example.demo.service.BiodataService;
import com.example.demo.service.IdentityTypeService;
import com.example.demo.service.MaritalStatusService;
import com.example.demo.service.ReligionService;

@Controller
@RequestMapping("/biodata")

public class BiodataController {
	@Autowired 
	private BiodataService biodataService;
	@Autowired
	private AddressService addressService;
	@Autowired
	private ReligionService religionService;
	@Autowired
	private MaritalStatusService maritalStatusService;
	@Autowired
	private IdentityTypeService identityTypeService;
	@RequestMapping("/home")
	public String doHome()
	{
		String page="/biodata/home";
		return page;
	}
	
	@RequestMapping("/ubah")
	public String doUbah(HttpServletRequest request,Model model) {
		Long id = Long.parseLong(request.getParameter("id")) ;
		BiodataModel biodataModel= new BiodataModel();
		biodataModel = this.biodataService.searchIdBiodata(id);
		model.addAttribute("biodataModel", biodataModel);
		
		AddressModel addressModel= new AddressModel();
		addressModel = this.addressService.searchIdBiodata(id);
		
		model.addAttribute("addressModel", addressModel);
		this.doListAgama(model);
		this.doListStatus(model);
		this.doListIdentitas(model);
		
		String page ="/biodata/edit";
		return page;
	}
	
	@RequestMapping("/detail")
	public String doDetail(HttpServletRequest request,Model model) {
		Long id = Long.parseLong(request.getParameter("id")) ;
		//Long id = (long) 1;
		//long id = Long.valueOf(request.getParameter("id"));
		BiodataModel biodataModel= new BiodataModel();
		biodataModel = this.biodataService.searchIdBiodata(id);
		this.doDataidentityType(model);
		this.doDataAgama(model);
		this.doDataMaritalStatus(model);
		model.addAttribute("biodataModel", biodataModel);
		String page ="/biodata/detail";
		return page;
	}
	public void doDataAgama(Model model) {
		ReligionModel religionModel = new ReligionModel();
		Long id = (long) 1;
		religionModel=this.religionService.searchIdReligion(id);
		model.addAttribute("religionModel", religionModel);
		
	}	
	public void doDataMaritalStatus(Model model) {
		MaritalStatusModel maritalStatusModel = new MaritalStatusModel();
		Long id = (long) 1;
		maritalStatusModel=this.maritalStatusService.searchIdMaritalStatus(id);
		model.addAttribute("maritalStatusModel", maritalStatusModel);
		
	}	
	public void doDataidentityType(Model model) {
		IdentityTypeModel identityTypeModel = new IdentityTypeModel();
		Long id = (long) 1;
		identityTypeModel=this.identityTypeService.searchIdIdentityType(id);
		model.addAttribute("identityTypeModel", identityTypeModel);
		
	}
	@RequestMapping("/update")
	public String doUpdate(HttpServletRequest request) throws ParseException {
		Long id = Long.parseLong(request.getParameter("id")) ;
		String fullname = request.getParameter("fullname");
		String nickName = request.getParameter("nickName");
		String pob = request.getParameter("pob");
		SimpleDateFormat format = new SimpleDateFormat ("yyyy-MM-dd");
		Date dateDob= format.parse("dob");
		Boolean gender = Boolean.parseBoolean(request.getParameter("gender")) ;
		Long religionId = Long.parseLong(request.getParameter("religionId")) ;
		Integer high = Integer.parseInt(request.getParameter("high"));
		Integer weight = Integer.parseInt(request.getParameter("weight"));
		String nationality = request.getParameter("nationality");
		String ethnic = request.getParameter("ethnic");
		String hobby = request.getParameter("hobby");
		Long identityTypeId = Long.parseLong(request.getParameter("identityTypeId")) ;
		String identityNo = request.getParameter("identityNo");
		String email = request.getParameter("email");
		String phoneNumber1 = request.getParameter("phoneNumber1");
		String phoneNumber2 = request.getParameter("phoneNumber2");
		String parentPhoneNumber = request.getParameter("parentPhoneNumber");
		String childSequence = request.getParameter("childSequence");
		String howManyBrothers = request.getParameter("howManyBrothers");
		Long maritalStatusId = Long.parseLong(request.getParameter("maritalStatusId")) ;
		Long addrbookId = Long.parseLong(request.getParameter("addrbookId")) ;
		String token = request.getParameter("token");
		Date dateToken= format.parse("expiredToken");
		
		String marriageYear = request.getParameter("marriageYear");
		BiodataModel biodataModel= new BiodataModel();
		biodataModel = this.biodataService.searchIdBiodata(id);
		biodataModel.setId(id);
		biodataModel.setFullname(fullname);
		biodataModel.setNickName(nickName);
		biodataModel.setPob(pob);
		biodataModel.setDob(dateDob);
		biodataModel.setGender(gender);
		biodataModel.setReligionId(religionId);
		biodataModel.setHigh(high);
		biodataModel.setWeight(weight);
		biodataModel.setNationality(nationality);
		biodataModel.setEthnic(ethnic);
		biodataModel.setHobby(hobby);
		biodataModel.setIdentityTypeId(identityTypeId);
		biodataModel.setIdentityNo(identityNo);
		biodataModel.setEmail(email);
		biodataModel.setPhoneNumber1(phoneNumber1);
		biodataModel.setPhoneNumber2(phoneNumber2);
		biodataModel.setParentPhoneNumber(parentPhoneNumber);
		biodataModel.setChildSequence(childSequence);
		biodataModel.setHowManyBrothers(howManyBrothers);
		biodataModel.setMaritalStatusId(maritalStatusId);
		biodataModel.setAddrbookId(addrbookId);
		biodataModel.setToken(token);
		biodataModel.setExpiredToken(dateToken);
		biodataModel.setMarriageYear(marriageYear);
		String address1 = request.getParameter("address1");
		String postalCode1 = request.getParameter("postalCode1");
		String rt1 = request.getParameter("rt1");
		String rw1 = request.getParameter("rw1");
		String kelurahan1 = request.getParameter("kelurahan1");
		String kecamatan1 = request.getParameter("kecamatan1");
		String region1 = request.getParameter("region1");
		String address2 = request.getParameter("address2");
		String postalCode2 = request.getParameter("postalCode2");
		String rt2 = request.getParameter("rt2");
		String rw2 = request.getParameter("rw2");
		String kelurahan2 = request.getParameter("kelurahan2");
		String kecamatan2 = request.getParameter("kecamatan2");
		String region2 = request.getParameter("region2");
		AddressModel addressModel= new AddressModel();
		addressModel = this.addressService.searchIdBiodata(id);
		addressModel.setAddress1(address1);
		addressModel.setPostalCode1(postalCode1);
		addressModel.setRt1(rt1);
		addressModel.setRw1(rw1);
		addressModel.setKelurahan1(kelurahan1);
		addressModel.setKecamatan1(kecamatan1);
		addressModel.setRegion1(region1);
		addressModel.setAddress2(address2);
		addressModel.setPostalCode2(postalCode2);
		addressModel.setRt2(rt2);
		addressModel.setRw2(rw2);
		addressModel.setKelurahan2(kelurahan2);
		addressModel.setKecamatan2(kecamatan2);
		addressModel.setRegion2(region2);
		this.biodataService.update(biodataModel);
	
	return "/biodata";
	}
	
	public void doListAgama(Model model) {
		List<ReligionModel> religionModelList = new ArrayList<ReligionModel>();
		religionModelList = this.religionService.read();
		model.addAttribute("religionModelList", religionModelList);
	}

	public void doListStatus(Model model) {
		List<MaritalStatusModel> maritalStatusModelList = new ArrayList<MaritalStatusModel>();
		maritalStatusModelList = this.maritalStatusService.read();
		model.addAttribute("maritalStatusModelList", maritalStatusModelList);
	}

	public void doListIdentitas(Model model) {
		List<IdentityTypeModel> identityTypeModelList = new ArrayList<IdentityTypeModel>();
		identityTypeModelList = this.identityTypeService.read();
		model.addAttribute("identityTypeModelList", identityTypeModelList);
	}
}