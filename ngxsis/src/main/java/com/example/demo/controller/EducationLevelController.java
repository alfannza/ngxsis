package com.example.demo.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.EducationLevel;
import com.example.demo.service.EducationLevelService;

@Controller
@RequestMapping("/education")
public class EducationLevelController {

	@Autowired
	private EducationLevelService educationLevelService;
	
	@RequestMapping("/home")
	public String doHome(){
		return "/education/home";
	}
	
	@RequestMapping("/data")
	public String doList(Model model){
		List<EducationLevel> educationLevelList = new ArrayList<EducationLevel>();
		educationLevelList = this.educationLevelService.read();
		
		model.addAttribute("educationLevelList", educationLevelList);
		
		return "/education/list";
	}

	@RequestMapping("/add")
	public String doAdd(){
		return "/education/add";
	}

	@RequestMapping("/create")
	public String doCreate(HttpServletRequest request) {
		String name = request.getParameter("name");
		String description = request.getParameter("description");
		
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date createdOn = ts;
		Long createdBy = (long) 001;
		
		EducationLevel educationLevel = new EducationLevel();
		educationLevel.setName(name);
		educationLevel.setDescription(description);
		educationLevel.setIsDelete(false);
		educationLevel.setCreatedBy(createdBy);
		educationLevel.setCreatedOn(createdOn);
		
		
		this.educationLevelService.create(educationLevel);
		
		return "/education/list";
	}
	
//	edit
	
	@RequestMapping("/ubah")
	public String doUbah(HttpServletRequest request, Model model){
		Long id = Long.parseLong(request.getParameter("id"));
		
		EducationLevel educationLevel = new EducationLevel();
		educationLevel = this.educationLevelService.serachById(id);
		
		model.addAttribute("educationLevel", educationLevel);
		
		return "/education/edit";
	}

	@RequestMapping("/doUpdate")
	public String doUpdate(HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		
		String name = request.getParameter("name");
		String description = request.getParameter("description");
		
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date modifiedOn = ts;
		Long modifiedBy = (long) 001;
		
		EducationLevel educationLevel = new EducationLevel();
		educationLevel = this.educationLevelService.serachById(id);
		
		educationLevel.setName(name);
		educationLevel.setDescription(description);
		educationLevel.setIsDelete(false);
		educationLevel.setModifiedBy(modifiedBy);
		educationLevel.setModifiedOn(modifiedOn);
		
		this.educationLevelService.update(educationLevel);
		
		return "/education/list";
	}
	
	@RequestMapping("/hapus")
	public String doHapus(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		
		EducationLevel educationLevel = new EducationLevel();
		educationLevel = this.educationLevelService.serachById(id);
		
		model.addAttribute("educationLevel", educationLevel);
						
		return "/education/delete";
	}
	
	public String doDelete(HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date deletedOn = ts;
		Long deletedBy = (long) 001;
		
		EducationLevel educationLevel = new EducationLevel();
		educationLevel = this.educationLevelService.serachById(id);
		
		educationLevel.setDeletedBy(deletedBy);
		educationLevel.setDeletedOn(deletedOn);
		educationLevel.setIsDelete(true);
		
		this.educationLevelService.update(educationLevel);
		
		return "/pendidikan/list";
	}
	
}
