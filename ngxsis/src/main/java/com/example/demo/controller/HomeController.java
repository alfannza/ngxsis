package com.example.demo.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.model.AddrBookModel;
import com.example.demo.model.BiodataModel;
import com.example.demo.model.MenuTreeModel;
import com.example.demo.model.UserRoleModel;
import com.example.demo.repository.BiodataRepository;
import com.example.demo.repository.MenuTreeRepository;
import com.example.demo.service.BiodataService;

@Controller
public class HomeController {

	@Autowired
	private BiodataRepository biodataRepository;
	
	@Autowired
	private BiodataService biodataService;
	
	@Autowired
	private MenuTreeRepository menuTreeRepository;
	
	@Autowired
	private HttpSession session;

	@RequestMapping("/")
	public String getHomepage(HttpServletRequest request, Model model) {
		session = request.getSession(false);
		
		if (session == null) {
			return "/login/login";
		} else {
			UserRoleModel userRoleModel = (UserRoleModel)session.getAttribute("userRoleModel");
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			BiodataModel biodataModel = biodataService.searchAddrBookId(userRoleModel.getAddrbookld());
			System.out.println(userRoleModel.getAddrbookld());
			model.addAttribute("biodataModel",biodataModel);
			model.addAttribute("userRoleModel", userRoleModel);
			List<MenuTreeModel> menuList = this.menuTreeRepository.getMenuList(userRoleModel.getRoleId());
			List<MenuTreeModel> navbarList = this.menuTreeRepository.getNavbarList(userRoleModel.getRoleId());
			List<MenuTreeModel> submenuList = this.menuTreeRepository.getSubMenuList(userRoleModel.getRoleId());
			List<MenuTreeModel> subnavbarList = this.menuTreeRepository.getSubNavbarList(userRoleModel.getRoleId());
			model.addAttribute("navbarList",navbarList);
			model.addAttribute("menuList",menuList);
			model.addAttribute("submenuList",submenuList);
			model.addAttribute("subnavbarList",subnavbarList);
			Date createdOn = ts;
			String page = "index";
			return page;
		}
	}
	
	@RequestMapping("/logout")
	public String doLogout(HttpServletRequest request) {
		request.getSession().invalidate();
		return "/login/login";
	}
}
