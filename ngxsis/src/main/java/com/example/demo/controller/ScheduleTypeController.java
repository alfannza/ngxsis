package com.example.demo.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.ScheduleTypeModel;
import com.example.demo.model.UserRoleModel;
import com.example.demo.service.ScheduleTypeService;

@Controller
@RequestMapping("/schedule")
public class ScheduleTypeController {
	private Integer page = 0;
	private Integer element = 10;
	private Boolean ascdesc = true; 

	@Autowired
	private ScheduleTypeService scheduleTypeService;
	
	@RequestMapping("/home")
	public String doHome() {
		return "/schedule/home";
	}
	
	@RequestMapping("/tambah")
	public String doTambah (){
		
		return "/schedule/tambah";
	}
	
	@RequestMapping("/create")
	public String doCreate(HttpServletRequest request) {
		String name = request.getParameter("name");
		String description = request.getParameter("description");
		
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date createdOn = ts;
		
		ScheduleTypeModel scheduleTypeModel = new ScheduleTypeModel();
		scheduleTypeModel.setCreatedBy(userRoleModel.getAddrbookld());
		scheduleTypeModel.setCreatedOn(createdOn);
		scheduleTypeModel.setName(name);
		scheduleTypeModel.setDescription(description);
		
		this.scheduleTypeService.save(scheduleTypeModel);
		
		return "/schedule/list";
	}
	
	@RequestMapping("/list")
	public String doList(Model model) {
		List<ScheduleTypeModel> scheduleTypeModelList = new ArrayList<ScheduleTypeModel>();
		scheduleTypeModelList = this.scheduleTypeService.findIsNotDelete();
		
		model.addAttribute("scheduleTypeModelList", scheduleTypeModelList);
		
		return "/schedule/list";
	}
	
	@RequestMapping("/ubah")
	public String doUbah(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		ScheduleTypeModel scheduleTypeModel = new ScheduleTypeModel();
		scheduleTypeModel = this.scheduleTypeService.searchById(id);
		
		model.addAttribute("scheduleTypeModel", scheduleTypeModel);
		return "/schedule/ubah";
	}
	
	@RequestMapping("/doUpdate")
	public String doUpdate(HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		String name = request.getParameter("name");
		String description = request.getParameter("description");
		
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date modifiedOn = ts;
		
		ScheduleTypeModel scheduleTypeModel = new ScheduleTypeModel();
		scheduleTypeModel = this.scheduleTypeService.searchById(id);
		
		scheduleTypeModel.setModifiedBy(userRoleModel.getAddrbookld());
		scheduleTypeModel.setModifiedOn(modifiedOn);
		scheduleTypeModel.setName(name);
		scheduleTypeModel.setDescription(description);
		
		this.scheduleTypeService.update(scheduleTypeModel);
		
		return "/schedule/list";
	}
	
	@RequestMapping("/hapus")
	public String doHapus(HttpServletRequest request, Model model) {
		Long id = Long.parseLong(request.getParameter("id"));
		ScheduleTypeModel scheduleTypeModel = new ScheduleTypeModel();
		scheduleTypeModel = this.scheduleTypeService.searchById(id);
		
		model.addAttribute("scheduleTypeModel", scheduleTypeModel);
		return "/schedule/hapus";
	}
	
	@RequestMapping("/doHapus")
	public String doDelete(HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date deletedOn = ts;
		
		ScheduleTypeModel scheduleTypeModel = new ScheduleTypeModel();
		scheduleTypeModel = this.scheduleTypeService.searchById(id);
		
		scheduleTypeModel.setDelete(true);
		scheduleTypeModel.setDeletedBy(userRoleModel.getAddrbookld());
		scheduleTypeModel.setDeletedOn(deletedOn);
		
		this.scheduleTypeService.update(scheduleTypeModel);
		
		return "/schedule/list";
	}
	
	@RequestMapping("/doAscending")
	public String doAscending(HttpServletRequest request, Model model) {
		List<ScheduleTypeModel> scheduleTypeModelDummy = new ArrayList<ScheduleTypeModel>();
		List<ScheduleTypeModel> scheduleTypeModelList = new ArrayList<ScheduleTypeModel>();
		scheduleTypeModelDummy = this.scheduleTypeService.findAscending();
		this.ascdesc = true;
		
		Integer start = this.page * this.element;
		Integer end = start + this.element;
		
		for (int i = start; i < end; i++) {
			try {
				scheduleTypeModelList.add(scheduleTypeModelDummy.get(i));
			} catch (Exception e) {
				break;
			}
		}
		
		model.addAttribute("scheduleTypeModelList", scheduleTypeModelList);
		return "/schedule/list";
	}
	
	@RequestMapping("/doDescending")
	public String doDescending(HttpServletRequest request, Model model) {
		List<ScheduleTypeModel> scheduleTypeModelDummy = new ArrayList<ScheduleTypeModel>();
		List<ScheduleTypeModel> scheduleTypeModelList = new ArrayList<ScheduleTypeModel>();
		scheduleTypeModelDummy = this.scheduleTypeService.findDescending();
		this.ascdesc = false;
		
		Integer start = this.page * this.element;
		Integer end = start + this.element;
		
		for (int i = start; i < end; i++) {
			try {
				scheduleTypeModelList.add(scheduleTypeModelDummy.get(i));
			} catch (Exception e) {
				break;
			}
		}
		
		model.addAttribute("scheduleTypeModelList", scheduleTypeModelList);
		return "/schedule/list";
	}
	
	@RequestMapping("/jumlahList")
	public String doJumlahList(HttpServletRequest request, Model model) {
		List<ScheduleTypeModel> scheduleTypeModelDummy = new ArrayList<ScheduleTypeModel>();
		List<ScheduleTypeModel> scheduleTypeModelList = new ArrayList<ScheduleTypeModel>();
		scheduleTypeModelDummy = this.scheduleTypeService.findAscending();
		
		Integer jumlahList = Integer.parseInt(request.getParameter("element"));
		this.element = jumlahList;
		
		for (int i = 0; i < this.element; i++) {
			try {
				scheduleTypeModelList.add(scheduleTypeModelDummy.get(i));
			} catch (Exception e) {
				break;
			}
		}
		
		model.addAttribute("scheduleTypeModelList", scheduleTypeModelList);
		return "/schedule/list";
	}
	
	@RequestMapping("/search/nama")
	public String doSearchNama(HttpServletRequest request, Model model) {
		List<ScheduleTypeModel> scheduleTypeModelList = new ArrayList<ScheduleTypeModel>();
		
		String name = request.getParameter("name");
		scheduleTypeModelList = this.scheduleTypeService.searchLikeName(name);

		model.addAttribute("scheduleTypeModelList", scheduleTypeModelList);
		return "/schedule/list";
	}
	
	@RequestMapping("/doBack")
	public String doBack(HttpServletRequest request, Model model) {
		
		this.page = this.page - 1;
		if (this.page < 0) {
			this.page = 0;
		}
		
		List<ScheduleTypeModel> scheduleTypeModelDummy = new ArrayList<ScheduleTypeModel>();
		List<ScheduleTypeModel> scheduleTypeModelList = new ArrayList<ScheduleTypeModel>();
		if (this.ascdesc == true) {
			scheduleTypeModelDummy = this.scheduleTypeService.findAscending();
		} else {
			scheduleTypeModelDummy = this.scheduleTypeService.findDescending();
		}
		
		Integer start = this.page * this.element;
		Integer end = start + this.element;
		
		for (int i = start; i < end; i++) {
			try {
				scheduleTypeModelList.add(scheduleTypeModelDummy.get(i));
			} catch (Exception e) {
				break;
			}
		}
		
		model.addAttribute("scheduleTypeModelList", scheduleTypeModelList);
		return "/schedule/list";
	}
	
	@RequestMapping("/doAfter")
	public String doAfter(HttpServletRequest request, Model model) {
		
		this.page = this.page + 1;
		
		List<ScheduleTypeModel> scheduleTypeModelDummy = new ArrayList<ScheduleTypeModel>();
		List<ScheduleTypeModel> scheduleTypeModelList = new ArrayList<ScheduleTypeModel>();
		if (this.ascdesc == true) {
			scheduleTypeModelDummy = this.scheduleTypeService.findAscending();
		} else {
			scheduleTypeModelDummy = this.scheduleTypeService.findDescending();
		}
		
		Integer maxPage = scheduleTypeModelDummy.size() / this.element;
		
		if (this.page > maxPage) {
			this.page = maxPage;
		}
		
		Integer start = this.page * this.element;
		Integer end = start + this.element;
		
		for (int i = start; i < end; i++) {
			try {
				scheduleTypeModelList.add(scheduleTypeModelDummy.get(i));
			} catch (Exception e) {
				break;
			}
		}
		
		model.addAttribute("scheduleTypeModelList", scheduleTypeModelList);
		return "/schedule/list";
	}
	
}
