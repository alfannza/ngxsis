package com.example.demo.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.RoleModel;
import com.example.demo.model.UserRoleModel;
import com.example.demo.service.RoleService;

@Controller
@RequestMapping("/role")
public class RoleController {
	private Integer page = 0;
	private Integer element = 10;
	private Boolean ascdesc = true; // true = asc; desc = false;

	@Autowired
	private RoleService roleService;

	@RequestMapping("/home")
	public String doHome() {

		String page = "/role/home";
		return page;
	}

	@RequestMapping("/data")
	public String doList(Model model) {
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		roleModelList = this.roleService.read();

		model.addAttribute("roleModelList", roleModelList);
		return "/role/list";
	}

	@RequestMapping("/add")
	public String doTambah(Model model) {
		String page = "/role/add";
		return page;
	}

	@RequestMapping("/create")
	public String doCreate(HttpServletRequest request) {
		String code = request.getParameter("code");
		String name = request.getParameter("name");
		String page = "/role/list";
		RoleModel roleModel = new RoleModel();

		// created by syntax
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date createdOn = ts;

		roleModel.setCreatedBy(userRoleModel.getAddrbookld());
		roleModel.setCreatedOn(createdOn);
		roleModel.setCode(code);
		roleModel.setName(name);
		roleModel.setIsDelete(false);
		this.roleService.create(roleModel);
		return page;
	}

	@RequestMapping("/ubah")
	public String doPageUpdate(HttpServletRequest request, Model model) {
		String code = request.getParameter("code");
		RoleModel roleModel = new RoleModel();
		roleModel = this.roleService.findByCode(code);

		model.addAttribute("roleModel", roleModel);

		return "/role/edit";
	}

	@RequestMapping("/doUpdate")
	public String doUpdate(HttpServletRequest request, Model model) {
		String code = request.getParameter("code");
		String name = request.getParameter("name");

		// modify by syntax
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date modifiedOn = ts;

		RoleModel roleModel = new RoleModel();
		roleModel = this.roleService.findByCode(code);
		roleModel.setCode(code);
		roleModel.setName(name);
		roleModel.setModifiedBy(userRoleModel.getAddrbookld());
		roleModel.setModifiedOn(modifiedOn);

		this.roleService.update(roleModel);

		return "/role/list";
	}

	@RequestMapping("/hapus")
	public String doPageDelete(HttpServletRequest request, Model model) {
		String name = request.getParameter("name");

		RoleModel roleModel = new RoleModel();
		roleModel = this.roleService.findByName(name);

		model.addAttribute("roleModel", roleModel);

		return "/role/delete";
	}

	@RequestMapping("/doHapus")
	public String doDelete(HttpServletRequest request) {
		String name = request.getParameter("name");

		// deleted by syntax
		HttpSession session = request.getSession(false);
		UserRoleModel userRoleModel = (UserRoleModel) session.getAttribute("userRoleModel");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date deletedOn = ts;

		RoleModel roleModel = new RoleModel();
		roleModel = this.roleService.findByName(name);
		roleModel.setIsDelete(true);
		roleModel.setDeletedBy(userRoleModel.getAddrbookld());
		roleModel.setDeletedOn(deletedOn);

		this.roleService.update(roleModel);

		return "/role/list";
	}

	@RequestMapping("/doAscending")
	public String doAscending(HttpServletRequest request, Model model) {
		List<RoleModel> roleModelListDummy = new ArrayList<RoleModel>();
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		roleModelListDummy = this.roleService.findAscending();
		this.ascdesc = true;

		Integer start = this.page * this.element;
		Integer end = start + this.element;

		for (int i = start; i < end; i++) {
			try {
				roleModelList.add(roleModelListDummy.get(i));

			} catch (Exception e) {
				// TODO: handle exception
				break;
			}

		}

		model.addAttribute("roleModelList", roleModelList);
		return "/role/list";
	}

	@RequestMapping("/doDescending")
	public String doDescending(HttpServletRequest request, Model model) {
		List<RoleModel> roleModelListDummy = new ArrayList<RoleModel>();
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		roleModelListDummy = this.roleService.findDescending();
		this.ascdesc = false;

		Integer start = this.page * this.element;
		Integer end = start + this.element;

		for (int i = start; i < end; i++) {
			try {
				roleModelList.add(roleModelListDummy.get(i));

			} catch (Exception e) {
				// TODO: handle exception
				break;
			}

		}

		model.addAttribute("roleModelList", roleModelList);
		return "/role/list";
	}

	@RequestMapping("/jumlahList")
	public String doJumlahList(HttpServletRequest request, Model model) {

		Integer jumlahList = Integer.parseInt(request.getParameter("element"));
		this.element = jumlahList;
		List<RoleModel> roleModelListDummy = new ArrayList<RoleModel>();
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		roleModelListDummy = this.roleService.findAscending();

		for (int i = 0; i < jumlahList; i++) {
			try {
				roleModelList.add(roleModelListDummy.get(i));

			} catch (Exception e) {
				// TODO: handle exception
				break;
			}

		}

		model.addAttribute("roleModelList", roleModelList);
		return "/role/list";
	}

	@RequestMapping("/search/nama")
	public String doSearchNama(HttpServletRequest request, Model model) {
		String name = request.getParameter("name");

		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		roleModelList = this.roleService.searchLikeName(name);
		model.addAttribute("roleModelList", roleModelList);

		String page = "/role/list";
		return page;
	}

	@RequestMapping("/doBack")
	public String doBack(HttpServletRequest request, Model model) {

		this.page = this.page - 1;
		if (this.page < 0) {
			this.page = 0;
		}

		List<RoleModel> roleModelListDummy = new ArrayList<RoleModel>();
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		if (this.ascdesc == true) {
			roleModelListDummy = this.roleService.findAscending();
		} else {
			roleModelListDummy = this.roleService.findDescending();
		}

		Integer start = this.page * this.element;
		Integer end = start + this.element;

		for (int i = start; i < end; i++) {
			try {
				roleModelList.add(roleModelListDummy.get(i));

			} catch (Exception e) {
				// TODO: handle exception
				break;
			}

		}

		model.addAttribute("roleModelList", roleModelList);

		return "/role/list";
	}

	@RequestMapping("/doAfter")
	public String doForward(Model model) {
		this.page = this.page + 1;
		List<RoleModel> roleModelListDummy = new ArrayList<RoleModel>();
		List<RoleModel> roleModelList = new ArrayList<RoleModel>();
		if (this.ascdesc == true) {
			roleModelListDummy = this.roleService.findAscending();
		} else {
			roleModelListDummy = this.roleService.findDescending();
		}
		
		Integer maxPage = roleModelListDummy.size() / this.element;
		
		if (this.page > maxPage) {
			this.page = maxPage;
		}

		Integer start = this.page * this.element;
		Integer end = start + this.element;

		for (int i = start; i < end; i++) {
			try {
				roleModelList.add(roleModelListDummy.get(i));

			} catch (Exception e) {
				// TODO: handle exception
				break;
			}
		}

		model.addAttribute("roleModelList", roleModelList);

		return "/role/list";
	}

}
