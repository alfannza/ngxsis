package com.example.demo.controller;




import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.AddrBookModel;
import com.example.demo.model.CompanyModel;
import com.example.demo.model.MenuTreeModel;
import com.example.demo.model.UserRoleModel;
import com.example.demo.service.CompanyService;
import com.example.demo.service.MenuTreeService;
import com.example.demo.service.UserRoleService;

@Controller
@RequestMapping("/access")
public class MenuAccessController {

	@Autowired
	private UserRoleService userRoleService;
	
	@Autowired
	private CompanyService companyService;
	
	private MenuTreeService menuTreeService;
	
	@RequestMapping("/index")
	public String doSelectAccess(HttpServletRequest request, Model model) {
//		HttpSession session = request.getSession(false);
//		AddrBookModel addrBookModel = (AddrBookModel)session.getAttribute("addrBookModel");
//		System.out.println(addrBookModel.getId());
//		List<UserRoleModel> userRoleModelList = new ArrayList<UserRoleModel>();
//		userRoleModelList = this.userRoleService.findByAddrBookId(addrBookModel.getId());
//		
//		// dikirim ke html
//		
//		this.doMenuTree(model);
//		this.doCompany(model);
//		model.addAttribute("userRoleModelList", userRoleModelList);

		String page = "/selectaccess/select_access";
		return page;
	}

	public void doCompany(Model model) {
		List<CompanyModel> companyModelList = new ArrayList<CompanyModel>();
		companyModelList = this.companyService.read();
		
		model.addAttribute("companyModelList", companyModelList);
	}
	
	public void doMenuTree(Model model) {
		List<MenuTreeModel> menuTreeModelList = new ArrayList<MenuTreeModel>();
		menuTreeModelList = this.menuTreeService.read();
		
		model.addAttribute("menuTreeModelList", menuTreeModelList);
	}
	
}
