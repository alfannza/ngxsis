package com.example.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="X_RIWAYAT_PROYEK")
public class RiwayatProyekModel {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(nullable=false, name="ID", length=11)
	private Long id;
	
	@Column(nullable=false, name="CREATED_BY", length=11)
	private Long createdBy;
	
	@Column(nullable=false, name="CREATED_ON")
	private Date createdOn;
	
	@Column(nullable=false, name="IS_DELETE")
	private Boolean isDelete;
	
	@Column(nullable=false, name="RIWAYAT_PEKERJAAN_ID", length=11)
	private Long riwayatPekerjaanId;
	
	@ManyToOne
	@JoinColumn(name = "RIWAYAT_PEKERJAAN_ID", referencedColumnName="ID", nullable = true, updatable = false, insertable = false)
	private RiwayatPekerjaanModel riwayatPekerjaanModel;
	
	@Column(name="MODIFIED_BY", length=11, nullable=true)
	private Long modifiedBy;
	
	@Column(name="MODIFIED_ON", nullable=true)
	private Date modifiedOn;

	@Column(name="DELETED_BY", length=11, nullable=true)
	private Long deletedBy;
	
	@Column(name="DELETED_ON", nullable=true)
	private Date deletedOn;
	
	@Column(name="START_YEAR", length=10, nullable=true)
	private String startYear;
	
	@Column(name="START_MONTH", length=10, nullable=true)
	private String startMonth;
	
	@Column(name="PROJECT_NAME", length=50, nullable=true)
	private String projectName;
	
	@Column(name="PROJECT_DURATION", nullable=true)
	private Integer projectDuration;
	
	@Column(name="TIME_PERIOD_ID", length=11, nullable=true)
	private Long timePeriodId;
	
	@ManyToOne
	@JoinColumn(name = "TIME_PERIOD_ID", referencedColumnName="ID", nullable = true, updatable = false, insertable = false)
	private TimePeriodModel timePeriodModel;
	
	@Column(name="CLIENT", length=100, nullable=true)
	private String client;
	
	@Column(name="PROJECT_POSITION", length=100, nullable=true)
	private String projectPosition;
	
	@Column(name="DESCRIPTION", length=1000, nullable=true)
	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getRiwayatPekerjaanId() {
		return riwayatPekerjaanId;
	}

	public void setRiwayatPekerjaanId(Long riwayatPekerjaanId) {
		this.riwayatPekerjaanId = riwayatPekerjaanId;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public String getStartYear() {
		return startYear;
	}

	public void setStartYear(String startYear) {
		this.startYear = startYear;
	}

	public String getStartMonth() {
		return startMonth;
	}

	public void setStartMonth(String startMonth) {
		this.startMonth = startMonth;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Integer getProjectDuration() {
		return projectDuration;
	}

	public void setProjectDuration(Integer projectDuration) {
		this.projectDuration = projectDuration;
	}

	public Long getTimePeriodId() {
		return timePeriodId;
	}

	public void setTimePeriodId(Long timePeriodId) {
		this.timePeriodId = timePeriodId;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getProjectPosition() {
		return projectPosition;
	}

	public void setProjectPosition(String projectPosition) {
		this.projectPosition = projectPosition;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public RiwayatPekerjaanModel getRiwayatPekerjaanModel() {
		return riwayatPekerjaanModel;
	}

	public void setRiwayatPekerjaanModel(RiwayatPekerjaanModel riwayatPekerjaanModel) {
		this.riwayatPekerjaanModel = riwayatPekerjaanModel;
	}

	public TimePeriodModel getTimePeriodModel() {
		return timePeriodModel;
	}

	public void setTimePeriodModel(TimePeriodModel timePeriodModel) {
		this.timePeriodModel = timePeriodModel;
	}
	
}
