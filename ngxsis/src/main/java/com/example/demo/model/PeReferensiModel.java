package com.example.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.example.demo.configuration.Auditable;


@Entity
@Table(name="X_PE_REFERENSI")
public class PeReferensiModel extends Auditable<Long>{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(nullable=false, name="ID", length=11)
	private Long id;
	
	@CreatedBy
	@Column(nullable=false, name="CREATED_BY", length=11)
	private Long createdBy;
	
	@CreatedDate
	@Column(nullable=false, name="CREATED_ON")
	private Date createdOn;
	
	@LastModifiedBy
	@Column(name="MODIFIED_BY", length=11, nullable=true)
	private Long modifiedBy;
	
	@LastModifiedDate
	@Column(name="MODIFIED_ON", nullable=true)
	private Date modifiedOn;
	
	@Column(name="DELETED_BY", length=11, nullable=true)
	private Long deletedBy;
	
	@Column(name="DELETED_ON", nullable=true)
	private Date deletedOn;
	
	@Column(nullable=false, name="IS_DELETE")
	private Boolean isDelete;
	
	@Column(nullable=false, name="BIODATA_ID")
	private Long biodataId;
	
	@ManyToOne
	@JoinColumn(name = "BIODATA_ID", insertable=false, updatable=false )
	private BiodataModel biodata;
	
	@Column(name="NAME", length=100, nullable=true)
	private String name;
	
	@Column(name="POSITION", length=100, nullable=true)
	private String position;
	
	@Column(name="ADDRESS_PHONE", length=1000, nullable=true)
	private String addressPhone;
	
	@Column(name="RELATION", length=100, nullable=true)
	private String relation;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getAddressPhone() {
		return addressPhone;
	}

	public void setAddressPhone(String addressPhone) {
		this.addressPhone = addressPhone;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}
	
	
}
