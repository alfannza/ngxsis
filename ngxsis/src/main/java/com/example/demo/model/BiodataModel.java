package com.example.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "X_BIODATA")
public class BiodataModel {
	
	private Integer usia;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, name = "ID", length = 11)
	private Long id;

	@Column(nullable = false, name = "CREATED_BY", length = 11)
	private Long createdBy;

	@Column(nullable = false, name = "CREATED_ON")
	private Date createdOn;

	@Column(name = "MODIFIED_BY", length = 11, nullable = true)
	private Long modifiedBy;

	@Column(name = "MODIFIED_ON", nullable = true)
	private Date modifiedOn;

	@Column(name = "DELETED_BY", length = 11, nullable = true)
	private Long deletedBy;

	@Column(name = "DELETED_ON", nullable = true)
	private Date deletedOn;

	@Column(nullable = false, name = "IS_DELETE")
	private Boolean isDelete;

	@Column(nullable = false, name = "FULLNAME", length = 255)
	private String fullname;

	@Column(nullable = false, name = "NICK_NAME", length = 100)
	private String nickName;

	@Column(nullable = false, name = "POB", length = 100)
	private String pob;

	@Column(nullable = false, name = "DOB")
	private Date dob;

	@Column(nullable = false, name = "GENDER")
	private Boolean gender;

	@Column(name = "RELIGION_ID", length = 11, nullable = false)
	private Long religionId;

	@Column(name = "HIGH", nullable = true)
	private Integer high;

	@Column(name = "WEIGHT", nullable = true)
	private Integer weight;

	@Column(nullable = true, name = "NATIONALITY", length = 100)
	private String nationality;

	@Column(nullable = true, name = "ETHNIC", length = 50)
	private String ethnic;

	@Column(nullable = true, name = "HOBBY", length = 255)
	private String hobby;

	@Column(name = "IDENTITY_TYPE_ID", length = 11, nullable = false)
	private Long identityTypeId;

	@Column(nullable = true, name = "IDENTITY_NO", length = 50)
	private String identityNo;

	@Column(nullable = false, name = "EMAIL", length = 100)
	private String email;

	@Column(nullable = false, name = "PHONE_NUMBER1", length = 50)
	private String phoneNumber1;

	@Column(nullable = true, name = "PHONE_NUMBER2", length = 50)
	private String phoneNumber2;

	@Column(nullable = false, name = "PARENT_PHONE_NUMBER", length = 50)
	private String parentPhoneNumber;

	@Column(nullable = true, name = "CHILD_SEQUENCE", length = 5)
	private String childSequence;

	@Column(nullable = true, name = "HOW_MANY_BROTHERS", length = 5)
	private String howManyBrothers;

	@Column(name = "MARITAL_STATUS_ID", length = 11, nullable = false)
	private Long maritalStatusId;

	@Column(name = "ADDR_BOOK_ID", length = 11, nullable = true)
	private Long addrbookId;

	@Column(nullable = true, name = "TOKEN", length = 10)
	private String token;

	@Column(nullable = true, name = "EXPIRED_TOKEN")
	private Date expiredToken;

	@Column(nullable = true, name = "MARRIAGE_YEAR", length = 10)
	private String marriageYear;

	@Column(nullable = false, name = "COMPANY_ID", length = 11)
	private Long companyId;
	
	@ManyToOne
	@JoinColumn(name = "COMPANY_ID", referencedColumnName="ID", nullable = true, updatable = false, insertable = false)
	private CompanyModel companyModel;

	@ManyToOne
	@JoinColumn(name = "RELIGION_ID", referencedColumnName="ID", nullable = true, updatable = false, insertable = false)
	private ReligionModel religionModel;

	@ManyToOne
	@JoinColumn(name = "MARITAL_STATUS_ID", referencedColumnName="ID", nullable = true, updatable = false, insertable = false)
	private MaritalStatusModel maritalStatusModel;
	
	@ManyToOne
	@JoinColumn(name = "ADDR_BOOK_ID", referencedColumnName="ID", nullable = true, updatable = false, insertable = false)
	private AddrBookModel addrBookModel;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getPob() {
		return pob;
	}

	public void setPob(String pob) {
		this.pob = pob;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Boolean getGender() {
		return gender;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	public Long getReligionId() {
		return religionId;
	}

	public void setReligionId(Long religionId) {
		this.religionId = religionId;
	}

	public Integer getHigh() {
		return high;
	}

	public void setHigh(Integer high) {
		this.high = high;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getEthnic() {
		return ethnic;
	}

	public void setEthnic(String ethnic) {
		this.ethnic = ethnic;
	}

	public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}

	public Long getIdentityTypeId() {
		return identityTypeId;
	}

	public void setIdentityTypeId(Long identityTypeId) {
		this.identityTypeId = identityTypeId;
	}

	public String getIdentityNo() {
		return identityNo;
	}

	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber1() {
		return phoneNumber1;
	}

	public void setPhoneNumber1(String phoneNumber1) {
		this.phoneNumber1 = phoneNumber1;
	}

	public String getPhoneNumber2() {
		return phoneNumber2;
	}

	public void setPhoneNumber2(String phoneNumber2) {
		this.phoneNumber2 = phoneNumber2;
	}

	public String getParentPhoneNumber() {
		return parentPhoneNumber;
	}

	public void setParentPhoneNumber(String parentPhoneNumber) {
		this.parentPhoneNumber = parentPhoneNumber;
	}

	public String getChildSequence() {
		return childSequence;
	}

	public void setChildSequence(String childSequence) {
		this.childSequence = childSequence;
	}

	public String getHowManyBrothers() {
		return howManyBrothers;
	}

	public void setHowManyBrothers(String howManyBrothers) {
		this.howManyBrothers = howManyBrothers;
	}

	public Long getMaritalStatusId() {
		return maritalStatusId;
	}

	public void setMaritalStatusId(Long maritalStatusId) {
		this.maritalStatusId = maritalStatusId;
	}

	public Long getAddrbookId() {
		return addrbookId;
	}

	public void setAddrbookId(Long addrbookId) {
		this.addrbookId = addrbookId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getExpiredToken() {
		return expiredToken;
	}

	public void setExpiredToken(Date expiredToken) {
		this.expiredToken = expiredToken;
	}

	public String getMarriageYear() {
		return marriageYear;
	}

	public void setMarriageYear(String marriageYear) {
		this.marriageYear = marriageYear;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public ReligionModel getReligionModel() {
		return religionModel;
	}

	public void setReligionModel(ReligionModel religionModel) {
		this.religionModel = religionModel;
	}

	public MaritalStatusModel getMaritalStatusModel() {
		return maritalStatusModel;
	}

	public void setMaritalStatusModel(MaritalStatusModel maritalStatusModel) {
		this.maritalStatusModel = maritalStatusModel;
	}

	public Integer getUsia() {
		return usia;
	}

	public void setUsia(Integer usia) {
		this.usia = usia;
	}

	public CompanyModel getCompanyModel() {
		return companyModel;
	}

	public void setCompanyModel(CompanyModel companyModel) {
		this.companyModel = companyModel;
	}

	public AddrBookModel getAddrBookModel() {
		return addrBookModel;
	}

	public void setAddrBookModel(AddrBookModel addrBookModel) {
		this.addrBookModel = addrBookModel;
	}
	
	

}
