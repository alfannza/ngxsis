package com.example.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "X_UNDANGAN")
public class Undangan {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, name = "ID", length = 11)
	private Long id;

	@Column(nullable = false, name = "CREATED_BY", length = 11)
	private Long createdBy;

	@Column(nullable = false, name = "CREATED_ON")
	private Date createdOn;

	@Column(nullable = true, name = "MODIFIED_BY", length = 11)
	private Long modifiedBy;

	@Column(nullable = true, name = "MODIFIED_ON")
	private Date modifiedOn;

	@Column(nullable = true, name = "DELETED_BY", length = 11)
	private Long deletedBy;

	@Column(nullable = true, name = "DELETED_ON")
	private Date deletedOn;

	@Column(nullable = false, name = "IS_DELETE")
	private Boolean isDelete;

	@Column(nullable = false, name = "INVITATION_DATE")
	private Date invitationDate;

	@Column(nullable = true, name = "RO", length = 11)
	private Long ro;

	@Column(nullable = true, name = "TRO", length = 11)
	private Long tro;

	@Column(nullable = true, name = "OTHER_RO_TRO", length = 100)
	private String otherRoTro;

	@Column(nullable = true, name = "LOCATION", length = 100)
	private String location;

	@Column(nullable = true, name = "STATUS", length = 50)
	private String status;
	
	@Column(nullable = true, name = "SCHEDULE_TYPE_ID", length = 11)
	private Long scheduleTypeId;
	
	@ManyToOne
	@JoinColumn(name = "SCHEDULE_TYPE_ID", referencedColumnName="ID", nullable = true, updatable = false, insertable = false)
	private ScheduleTypeModel scheduleTypeModel;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getScheduleTypeId() {
		return scheduleTypeId;
	}

	public void setScheduleTypeId(Long scheduleTypeId) {
		this.scheduleTypeId = scheduleTypeId;
	}

	public Date getInvitationDate() {
		return invitationDate;
	}

	public void setInvitationDate(Date invitationDate) {
		this.invitationDate = invitationDate;
	}

	public Long getRo() {
		return ro;
	}

	public void setRo(Long ro) {
		this.ro = ro;
	}

	public Long getTro() {
		return tro;
	}

	public void setTro(Long tro) {
		this.tro = tro;
	}

	public String getOtherRoTro() {
		return otherRoTro;
	}

	public void setOtherRoTro(String otherRoTro) {
		this.otherRoTro = otherRoTro;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ScheduleTypeModel getScheduleTypeModel() {
		return scheduleTypeModel;
	}

	public void setScheduleTypeModel(ScheduleTypeModel scheduleTypeModel) {
		this.scheduleTypeModel = scheduleTypeModel;
	}

	
	
}
