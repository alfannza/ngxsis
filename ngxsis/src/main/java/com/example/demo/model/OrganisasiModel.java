package com.example.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="X_ORGANISASI")
public class OrganisasiModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(nullable=false, name="ID", length=11)
	private Long id;
	
	@Column(nullable=false, name="CREATED_BY", length=11)
	private Long createdBy;
	
	@Column(nullable=false, name="CREATED_ON")
	private Date createdOn;
	
	@Column(name="MODIFIED_BY", length=11, nullable=true)
	private Long modifiedBy;
	
	@Column(name="MODIFIED_ON", nullable=true)
	private Date modifiedOn;
	
	@Column(name="DELETED_BY", length=11, nullable=true)
	private Long deletedBy;
	
	@Column(name="DELETED_ON", nullable=true)
	private Date deletedOn;
	
	@Column(nullable=false, name="IS_DELETE")
	private Boolean isDelete;
	
	@Column(nullable=false, name="BIODATA_ID")
	private Long biodataId;
	
	@Column(name="NAME", nullable=true, length=100)
	private String name;
	
	@Column(name="POSITION", nullable=true, length=100)
	private String position;
	
	@Column(name="ENTRY_YEAR", nullable=true, length=10)
	private String entryYear;
	
	@Column(name="EXIT_YEAR", nullable=true, length=10)
	private String exitYear;
	
	@Column(name="RESPONSIBILITY", nullable=true, length=100)
	private String responsibility;
	
	@Column(name="NOTES", nullable=true, length=1000)
	private String notes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getEntryYear() {
		return entryYear;
	}

	public void setEntryYear(String entryYear) {
		this.entryYear = entryYear;
	}

	public String getExitYear() {
		return exitYear;
	}

	public void setExitYear(String exitYear) {
		this.exitYear = exitYear;
	}

	public String getResponsibility() {
		return responsibility;
	}

	public void setResponsibility(String responsibility) {
		this.responsibility = responsibility;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	
}
