package com.example.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="X_ONLINE_TEST_DETAIL")
public class OnlineTestDetailModel {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", length=11, nullable=false)
	private Long id;
	
	@Column(nullable=false, name="CREATED_BY", length=11)
	private Long createdBy;
	
	@Column(nullable=false, name="CREATED_ON")
	private Date createdOn;
	
	@Column(name="MODIFIED_BY", length=11, nullable=true)
	private Long modifiedBy;
	
	@Column(name="MODIFIED_ON", nullable=true)
	private Date modifielOn;
	
	@Column(name="DELETED_BY", length=11, nullable=true)
	private Long deletedBy;
	
	@Column(name="DELETED_ON", nullable=true)
	private Date deletedOn;
	
	@Column(nullable=false, name="IS_DELETE")
	private Boolean isDelete;
	
	@Column(name="ONLINE_TEST_ID", length=11, nullable=true)
	private Long onlineTestId;
	
	@ManyToOne
	@JoinColumn(name = "ONLINE_TEST_ID", referencedColumnName="ID", nullable = true, updatable = false, insertable = false)
	private OnlineTestModel onlineTestModel;
	
	@Column(name="TEST_TYPE_ID", length=11, nullable=true)
	private Long testTypeId;
	
	@ManyToOne
	@JoinColumn(name = "TEST_TYPE_ID", referencedColumnName="ID", nullable = true, updatable = false, insertable = false)
	private TestTypeModel testTypeModel;
	
	@Column(name="TEST_ORDER", nullable=true)
	private Integer testOrder;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifielOn() {
		return modifielOn;
	}

	public void setModifielOn(Date modifielOn) {
		this.modifielOn = modifielOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getOnlineTestId() {
		return onlineTestId;
	}

	public void setOnlineTestId(Long onlineTestId) {
		this.onlineTestId = onlineTestId;
	}

	public Long getTestTypeId() {
		return testTypeId;
	}

	public void setTestTypeId(Long testTypeId) {
		this.testTypeId = testTypeId;
	}

	public Integer getTestOrder() {
		return testOrder;
	}

	public void setTestOrder(Integer testOrder) {
		this.testOrder = testOrder;
	}

	public OnlineTestModel getOnlineTestModel() {
		return onlineTestModel;
	}

	public void setOnlineTestModel(OnlineTestModel onlineTestModel) {
		this.onlineTestModel = onlineTestModel;
	}

	public TestTypeModel getTestTypeModel() {
		return testTypeModel;
	}

	public void setTestTypeModel(TestTypeModel testTypeModel) {
		this.testTypeModel = testTypeModel;
	}
	
}
