package com.example.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "X_UNDANGAN_DETAIL")
public class UndanganDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, name = "ID", length = 11)
	private Long id;

	@Column(nullable = false, name = "CREATED_BY", length = 11)
	private Long createdBy;

	@Column(nullable = false, name = "CREATED_ON")
	private Date createdOn;

	@Column(nullable = true, name = "MODIFIED_BY", length = 11)
	private Long modifiedBy;

	@Column(nullable = true, name = "MODIFIED_ON")
	private Date modifiedOn;

	@Column(nullable = true, name = "DELETED_BY", length = 11)
	private Long deletedBy;

	@Column(nullable = true, name = "DELETED_ON")
	private Date deletedOn;

	@Column(nullable = false, name = "IS_DELETE")
	private Boolean isDelete;

	@Column(nullable = false, name = "UNDANGAN_ID", length = 11)
	private Long undanganId;
	
	@ManyToOne
	@JoinColumn(name = "UNDANGAN_ID", referencedColumnName="ID", nullable = true, updatable = false, insertable = false)
	private Undangan undangan;

	@Column(nullable = false, name = "BIODATA_ID", length = 11)
	private Long biodataId;
	
	@ManyToOne
	@JoinColumn(name = "BIODATA_ID", referencedColumnName="ID", nullable = true, updatable = false, insertable = false)
	private BiodataModel biodataModel;

	@Column(nullable = true, name = "NOTES", length = 1000)
	private String notes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getUndanganId() {
		return undanganId;
	}

	public void setUndanganId(Long undanganId) {
		this.undanganId = undanganId;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Undangan getUndangan() {
		return undangan;
	}

	public void setUndangan(Undangan undangan) {
		this.undangan = undangan;
	}

	public BiodataModel getBiodataModel() {
		return biodataModel;
	}

	public void setBiodataModel(BiodataModel biodataModel) {
		this.biodataModel = biodataModel;
	}

	
	
}
