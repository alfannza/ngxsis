package com.example.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "X_MENU_ACCESS")
public class MenuAccessModel {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(nullable=false, name="ID", length=11)
	private Long id;
	
	@Column(nullable=false, name="MENUTREE_ID", length=11)
	private Long menutreeId;
	
	@ManyToOne
	@JoinColumn(name="MENUTREE_ID", referencedColumnName="ID", nullable = true, updatable = false, insertable = false)
	private MenuTreeModel MenuTreeModel;
	
	@Column(nullable=false, name="ROLE_ID", length=11)
	private Long roleId;
	
	@ManyToOne
	@JoinColumn(name="ROLE_ID", referencedColumnName="ID", nullable = true, updatable = false, insertable = false)
	private RoleModel roleModel;
	
	@Column(nullable=false, name="CREATED_BY", length=11)
	private Long createdBy;
	
	@Column(nullable=false, name="CREATED_ON")
	private Date createdOn;	
	
	@Column(name="MODIFIED_BY", length=11, nullable=true)
	private Long modifiedBy;
	
	@Column(name="MODIFIED_ON", nullable=true)
	private Date modifiedOn;

	@Column(name="DELETED_BY", length=11, nullable=true)
	private Long deletedBy;
	
	@Column(name="DELETED_ON", nullable=true)
	private Date deletedOn;
	
	@Column(nullable=false, name="IS_DELETE")
	private Boolean isDelete;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMenutreeId() {
		return menutreeId;
	}

	public void setMenutreeId(Long menutreeId) {
		this.menutreeId = menutreeId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public MenuTreeModel getMenuTreeModel() {
		return MenuTreeModel;
	}

	public void setMenuTreeModel(MenuTreeModel menuTreeModel) {
		MenuTreeModel = menuTreeModel;
	}

	public RoleModel getRoleModel() {
		return roleModel;
	}

	public void setRoleModel(RoleModel roleModel) {
		this.roleModel = roleModel;
	}	
	
	
}
