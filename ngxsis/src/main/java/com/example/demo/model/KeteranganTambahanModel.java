package com.example.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Table(name="X_KETERANGAN_TAMBAHAN")
public class KeteranganTambahanModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(nullable=false, name="ID", length=11)
	private Long id;
	
	@CreatedBy
	@Column(nullable=false, name="CREATED_BY", length=11)
	private Long createdBy;
	
	@CreatedDate
	@Column(nullable=false, name="CREATED_ON")
	private Date createdOn;
	
	@LastModifiedBy
	@Column(name="MODIFIED_BY", length=11, nullable=true)
	private Long modifiedBy;
	
	@LastModifiedDate
	@Column(name="MODIFIED_ON", nullable=true)
	private Date modifiedOn;
	
	@Column(name="DELETED_BY", length=11, nullable=true)
	private Long deletedBy;
	
	@Column(name="DELETED_ON", nullable=true)
	private Date deletedOn;
	
	@Column(nullable=false, name="IS_DELETE")
	private Boolean isDelete;
	
	@Column(nullable=false, name="BIODATA_ID")
	private Long biodataId;
	
	@ManyToOne
	@JoinColumn(name = "BIODATA_ID", insertable=false, updatable=false )
	private BiodataModel biodata;
	
	@Column(name="EMERGENCY_CONTACT_NAME", nullable=true, length=100)
	private String emergencyContactName;
	
	@Column(name="EMERGENCY_CONTACT_PHONE", nullable=true, length=50)
	private String emergencyContactPhone;
	
	@Column(name="EXPECTED_SALARY", nullable=true, length=20)
	private String expectedSalary;
	
	@Column(name="IS_NEGOTIABLE", nullable=true)
	private Boolean isNegotiable;
	
	@Column(name="START_WORKING", nullable=true, length=10)
	private String startWorking;
	
	@Column(name="IS_READY_TO_OUTOFTOWN", nullable=true)
	private Boolean isReadyToOutoftown;
	
	@Column(name="IS_APPLY_OTHER_PLACE", nullable=true)
	private Boolean isApplyOtherPlace;
	
	@Column(name="APPLY_PLACE", nullable=true, length=100)
	private String applyPlace;
	
	@Column(name="SELECTION_PHASE", nullable=true, length=100)
	private String selectionPhase;
	
	@Column(name="IS_EVER_BADLY_SICK", nullable=true)
	private Boolean isEverBadlySick;
	
	@Column(name="DISEASE_NAME", nullable=true, length=100)
	private String diseaseName;
	
	@Column(name="DISEASE_TIME", nullable=true, length=100)
	private String diseaseTime;
	
	@Column(name="IS_EVER_PSYCHOTEST", nullable=true)
	private Boolean isEverPsychotest;
	
	@Column(name="PSYCHOTEST_NEEDS", nullable=true, length=100)
	private String psychotestNeeds;
	
	@Column(name="PSYCHOTEST_TIME", nullable=true, length=100)
	private String pyschotestTime;
	
	@Column(name="REQUIREMENTES_REQUIRED", nullable=true, length=500)
	private String requirementesRequired;
	
	@Column(name="OTHER_NOTES", nullable=true, length=1000)
	private String otherNotes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public String getEmergencyContactName() {
		return emergencyContactName;
	}

	public void setEmergencyContactName(String emergencyContactName) {
		this.emergencyContactName = emergencyContactName;
	}

	public String getEmergencyContactPhone() {
		return emergencyContactPhone;
	}

	public void setEmergencyContactPhone(String emergencyContactPhone) {
		this.emergencyContactPhone = emergencyContactPhone;
	}

	public String getExpectedSalary() {
		return expectedSalary;
	}

	public void setExpectedSalary(String expectedSalary) {
		this.expectedSalary = expectedSalary;
	}

	public Boolean getIsNegotiable() {
		return isNegotiable;
	}

	public void setIsNegotiable(Boolean isNegotiable) {
		this.isNegotiable = isNegotiable;
	}

	public String getStartWorking() {
		return startWorking;
	}

	public void setStartWorking(String startWorking) {
		this.startWorking = startWorking;
	}

	public Boolean getIsReadyToOutoftown() {
		return isReadyToOutoftown;
	}

	public void setIsReadyToOutoftown(Boolean isReadyToOutoftown) {
		this.isReadyToOutoftown = isReadyToOutoftown;
	}

	public Boolean getIsApplyOtherPlace() {
		return isApplyOtherPlace;
	}

	public void setIsApplyOtherPlace(Boolean isApplyOtherPlace) {
		this.isApplyOtherPlace = isApplyOtherPlace;
	}

	public String getApplyPlace() {
		return applyPlace;
	}

	public void setApplyPlace(String applyPlace) {
		this.applyPlace = applyPlace;
	}

	public String getSelectionPhase() {
		return selectionPhase;
	}

	public void setSelectionPhase(String selectionPhase) {
		this.selectionPhase = selectionPhase;
	}

	public Boolean getIsEverBadlySick() {
		return isEverBadlySick;
	}

	public void setIsEverBadlySick(Boolean isEverBadlySick) {
		this.isEverBadlySick = isEverBadlySick;
	}

	public String getDiseaseName() {
		return diseaseName;
	}

	public void setDiseaseName(String diseaseName) {
		this.diseaseName = diseaseName;
	}

	public String getDiseaseTime() {
		return diseaseTime;
	}

	public void setDiseaseTime(String diseaseTime) {
		this.diseaseTime = diseaseTime;
	}

	public Boolean getIsEverPsychotest() {
		return isEverPsychotest;
	}

	public void setIsEverPsychotest(Boolean isEverPsychotest) {
		this.isEverPsychotest = isEverPsychotest;
	}

	public String getPsychotestNeeds() {
		return psychotestNeeds;
	}

	public void setPsychotestNeeds(String psychotestNeeds) {
		this.psychotestNeeds = psychotestNeeds;
	}

	public String getPyschotestTime() {
		return pyschotestTime;
	}

	public void setPyschotestTime(String pyschotestTime) {
		this.pyschotestTime = pyschotestTime;
	}

	public String getRequirementesRequired() {
		return requirementesRequired;
	}

	public void setRequirementesRequired(String requirementesRequired) {
		this.requirementesRequired = requirementesRequired;
	}

	public String getOtherNotes() {
		return otherNotes;
	}

	public void setOtherNotes(String otherNotes) {
		this.otherNotes = otherNotes;
	}
	
	
}
