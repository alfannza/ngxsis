package com.example.demo.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "X_EDUCATION_LEVEL")
public class EducationLevel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, name = "ID", length = 11)
	private Long id;

	@Column(nullable = false, name = "CREATED_BY", length = 11)
	private Long createdBy;

	@Column(nullable = false, name = "CREATED_ON")
	private Date createdOn;

	@Column(nullable = true, name = "MODIFIED_BY", length = 11)
	private Long modifiedBy;

	@Column(nullable = true, name = "MODIFIED_ON")
	private Date modifiedOn;

	@Column(nullable = true, name = "DELETED_BY", length = 11)
	private Long deletedBy;

	@Column(nullable = true, name = "DELETED_ON")
	private Date deletedOn;

	@Column(nullable = false, name = "IS_DELETE")
	private Boolean isDelete;

	@Column(nullable = false, name = "NAME", length = 50)
	private String name;

	@Column(nullable = true, name = "DESCRIPTION", length = 100)
	private String description;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
