package com.example.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="X_CATATAN")
public class CatatanModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", length=11, nullable=false)
	private Long id;
	
	@Column(nullable=false, name="CREATED_BY", length=11)
	private Long createdBy;
	// nama kolom model harus sama
	@ManyToOne
	@JoinColumn(name = "CREATED_BY", referencedColumnName="ID", nullable = true, updatable = false, insertable = false)
	private AddrBookModel addrBookModel;
	
	@Column(nullable=false, name="CREATED_ON")
	private Date createdOn;
	
	@Column(name="MODIFIED_BY", length=11, nullable=true)
	private Long modifiedBy;
	
	@Column(name="MODIFIED_ON", nullable=true)
	private Date modifiedOn;
	
	@Column(name="DELETED_BY", length=11, nullable=true)
	private Long deletedBy;
	
	@Column(name="DELETED_ON", nullable=true)
	private Date deletedOn;
	
	@Column(nullable=false, name="IS_DELETE")
	private Boolean isDelete;
	
	@Column(name="BIODATA_ID", length=11, nullable=false)
	private Long biodataId;
	
	@ManyToOne
	@JoinColumn(name = "BIODATA_ID", referencedColumnName="ID", nullable = true, updatable = false, insertable = false)
	private BiodataModel biodataModel;
	
	@Column(name="TITLE", nullable= true, length=100)
	private String title;
	
	@Column(name="NOTE_TYPE_ID", length=11, nullable=true)
	private Long noteTypeId;
	
	// nama kolom model harus sama
	@ManyToOne
	@JoinColumn(name = "NOTE_TYPE_ID", referencedColumnName="ID", nullable = true, updatable = false, insertable = false)
	private NoteTypeModel noteTypeModel;
	
	@Column(name="NOTES", length=1000, nullable=true)
	private String notes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifielOn) {
		this.modifiedOn = modifielOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getNoteTypeId() {
		return noteTypeId;
	}

	public void setNoteTypeId(Long noteTypeId) {
		this.noteTypeId = noteTypeId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public NoteTypeModel getNoteTypeModel() {
		return noteTypeModel;
	}

	public void setNoteTypeModel(NoteTypeModel noteTypeModel) {
		this.noteTypeModel = noteTypeModel;
	}

	public AddrBookModel getAddrBookModel() {
		return addrBookModel;
	}

	public void setAddrBookModel(AddrBookModel addrBookModel) {
		this.addrBookModel = addrBookModel;
	}

	public BiodataModel getBiodataModel() {
		return biodataModel;
	}

	public void setBiodataModel(BiodataModel biodataModel) {
		this.biodataModel = biodataModel;
	}
	
}
