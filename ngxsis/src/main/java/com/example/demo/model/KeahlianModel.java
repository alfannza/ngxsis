package com.example.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="X_KEAHLIAN")
public class KeahlianModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(nullable=false, name="ID", length=11)
	private Long id;
	
	@Column(nullable=false, name="CREATED_BY", length=11)
	private Long createdBy;
	
	@Column(nullable=false, name="CREATED_ON")
	private Date createdOn;
	
	@Column(name="MODIFIED_BY", length=11, nullable=true)
	private Long modifiedBy;
	
	@Column(name="MODIFIED_ON", nullable=true)
	private Date modifiedOn;
	
	@Column(name="DELETED_BY", length=11, nullable=true)
	private Long deletedBy;
	
	@Column(name="DELETED_ON", nullable=true)
	private Date deletedOn;
	
	@Column(nullable=false, name="IS_DELETE")
	private Boolean isDelete;
	
	@Column(nullable=false, name="BIODATA_ID")
	private Long biodataId;
	
	@ManyToOne
	@JoinColumn(name="BIODATA_ID", nullable = true, updatable = false, insertable = false)
	private BiodataModel biodataModel;
	
	@Column(name="SKILL_NAME", length=100, nullable=true)
	private String skillName;
	
	@Column(name="SKILL_LEVEL_ID", length=11, nullable=true)
	private Long skillLevelId;
	
	@ManyToOne
	@JoinColumn(name="SKILL_LEVEL_ID", referencedColumnName="ID", nullable = true, updatable = false, insertable = false)
	private SkillLevelModel skillLevelModel;
	

	@Column(name="NOTES", length=1000, nullable=true)
	private String notes;
	
	public BiodataModel getBiodataModel() {
		return biodataModel;
	}

	public void setBiodataModel(BiodataModel biodataModel) {
		this.biodataModel = biodataModel;
	}

	public SkillLevelModel getSkillLevelModel() {
		return skillLevelModel;
	}

	public void setSkillLevelModel(SkillLevelModel skillLevelModel) {
		this.skillLevelModel = skillLevelModel;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public String getSkillName() {
		return skillName;
	}

	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}

	public Long getSkillLevelId() {
		return skillLevelId;
	}

	public void setSkillLevelId(Long skillLevelId) {
		this.skillLevelId = skillLevelId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	
	
}
