package com.example.demo.model;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "X_ADDRESS")
public class AddressModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, name = "ID", length = 11)
	private Long id;

	@Column(nullable = false, name = "CREATED_BY", length = 11)
	private Long createdBy;

	@Column(nullable = false, name = "CREATED_ON")
	private Date createdOn;

	@Column(nullable = false, name = "IS_DELETE")
	private Boolean isDelete;

	@Column(name = "MODIFIED_BY", length = 11, nullable = true)
	private Long modifiedBy;

	@Column(name = "MODIFIED_ON", nullable = true)
	private Date modifiedOn;

	@Column(name = "DELETED_BY", length = 11, nullable = true)
	private Long deletedBy;

	@Column(name = "DELETED_ON", nullable = true)
	private Date deletedOn;

	@Column(name = "NAME", length = 50, nullable = false)
	private String name;

	@Column(name = "DESCRIPTION", length = 100, nullable = true)
	private String description;

	@Column(name = "BIODATA_ID", length = 11, nullable = false)
	private Long biodataId;

	@Column(name = "ADDRESS1", length = 1000, nullable = true)
	private String address1;

	@Column(name = "POSTALCODE1", length = 20, nullable = true)
	private String postalCode1;

	@Column(name = "RT1", length = 5, nullable = true)
	private String rt1;

	@Column(name = "RW1", length = 5, nullable = true)
	private String rw1;

	@Column(name = "KELURAHAN1", length = 100, nullable = true)
	private String kelurahan1;

	@Column(name = "KECAMATAN1", length = 100, nullable = true)
	private String kecamatan1;

	@Column(name = "REGION1", length = 100, nullable = true)
	private String region1;

	@Column(name = "ADDRESS2", length = 1000, nullable = true)
	private String address2;

	@Column(name = "POSTALCODE2", length = 20, nullable = true)
	private String postalCode2;

	@Column(name = "RT2", length = 5, nullable = true)
	private String rt2;

	@Column(name = "RW2", length = 5, nullable = true)
	private String rw2;

	@Column(name = "KELURAHAN2", length = 100, nullable = true)
	private String kelurahan2;

	@Column(name = "KECAMATAN2", length = 100, nullable = true)
	private String kecamatan2;

	@Column(name = "REGION2", length = 100, nullable = true)
	private String region2;
	@ManyToOne
	@JoinColumn(name = "BIODATA_ID", referencedColumnName="ID", nullable = true, updatable = false, insertable = false)
	private BiodataModel biodataModel;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getPostalCode1() {
		return postalCode1;
	}

	public void setPostalCode1(String postalCode1) {
		this.postalCode1 = postalCode1;
	}

	public String getRt1() {
		return rt1;
	}

	public void setRt1(String rt1) {
		this.rt1 = rt1;
	}

	public String getRw1() {
		return rw1;
	}

	public void setRw1(String rw1) {
		this.rw1 = rw1;
	}

	public String getKelurahan1() {
		return kelurahan1;
	}

	public void setKelurahan1(String kelurahan1) {
		this.kelurahan1 = kelurahan1;
	}

	public String getKecamatan1() {
		return kecamatan1;
	}

	public void setKecamatan1(String kecamatan1) {
		this.kecamatan1 = kecamatan1;
	}

	public String getRegion1() {
		return region1;
	}

	public void setRegion1(String region1) {
		this.region1 = region1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getPostalCode2() {
		return postalCode2;
	}

	public void setPostalCode2(String postalCode2) {
		this.postalCode2 = postalCode2;
	}

	public String getRt2() {
		return rt2;
	}

	public void setRt2(String rt2) {
		this.rt2 = rt2;
	}

	public String getRw2() {
		return rw2;
	}

	public void setRw2(String rw2) {
		this.rw2 = rw2;
	}

	public String getKelurahan2() {
		return kelurahan2;
	}

	public void setKelurahan2(String kelurahan2) {
		this.kelurahan2 = kelurahan2;
	}

	public String getKecamatan2() {
		return kecamatan2;
	}

	public void setKecamatan2(String kecamatan2) {
		this.kecamatan2 = kecamatan2;
	}

	public String getRegion2() {
		return region2;
	}

	public void setRegion2(String region2) {
		this.region2 = region2;
	}

}

