package com.example.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="X_KELUARGA")
public class KeluargaModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(nullable=false, name="ID", length=11)
	private Long id;
	
	@Column(nullable=false, name="CREATED_BY", length=11)
	private Long createdBy;
	
	@Column(nullable=false, name="CREATED_ON")
	private Date createdOn;
	
	@Column(name="MODIFIED_BY", length=11, nullable=true)
	private Long modifiedBy;
	
	@Column(name="MODIFIED_ON", nullable=true)
	private Date modifiedOn;
	
	@Column(name="DELETED_BY", length=11, nullable=true)
	private Long deletedBy;
	
	@Column(name="DELETED_ON", nullable=true)
	private Date deletedOn;
	
	@Column(nullable=false, name="IS_DELETE")
	private Boolean isDelete;
	
	@Column(nullable=false, name="BIODATA_ID")
	private Long biodataId;
	
	@Column(name="FAMILY_TREE_TYPE_ID", nullable=true)
	private Long familyTreeTypeId;
	
	@ManyToOne
	@JoinColumn(name = "FAMILY_TREE_TYPE_ID", referencedColumnName="ID", nullable = true, updatable = false, insertable = false)
	private FamilyTreeTypeModel familyTreeTypeModel;
	
	@Column(name="FAMILY_RELATION_ID", nullable=true)
	private Long familyRelationId;
	
	@ManyToOne
	@JoinColumn(name = "FAMILY_RELATION_ID", referencedColumnName="ID", nullable = true, updatable = false, insertable = false)
	private FamilyRelationModel familyRelationModel;
	
	@Column(name="NAME", nullable=true, length=100)
	private String name;
	
	@Column(name="GENDER", nullable=false)
	private Boolean gender;
	
	@Column(name="DOB", nullable=false)
	private Date dob;
	
	@Column(name="EDUCATION_LEVEL_ID", nullable=true, length=11)
	private Long educationLevelId;
	
	@ManyToOne
	@JoinColumn(name = "EDUCATION_LEVEL_ID", referencedColumnName="ID", nullable = true, updatable = false, insertable = false)
	private EducationLevel educationLevel;

	@Column(name="JOB", nullable=true, length=100)
	private String job;
	
	@Column(name="NOTES", nullable=true, length=1000)
	private String notes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public Long getFamilyTreeTypeId() {
		return familyTreeTypeId;
	}

	public void setFamilyTreeTypeId(Long familyTreeTypeId) {
		this.familyTreeTypeId = familyTreeTypeId;
	}

	public Long getFamilyRelationId() {
		return familyRelationId;
	}

	public void setFamilyRelationId(Long familyRelationId) {
		this.familyRelationId = familyRelationId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getGender() {
		return gender;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Long getEducationLevelId() {
		return educationLevelId;
	}

	public void setEducationLevelId(Long educationLevelId) {
		this.educationLevelId = educationLevelId;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public FamilyTreeTypeModel getFamilyTreeTypeModel() {
		return familyTreeTypeModel;
	}

	public void setFamilyTreeTypeModel(FamilyTreeTypeModel familyTreeTypeModel) {
		this.familyTreeTypeModel = familyTreeTypeModel;
	}

	public FamilyRelationModel getFamilyRelationModel() {
		return familyRelationModel;
	}

	public void setFamilyRelationModel(FamilyRelationModel familyRelationModel) {
		this.familyRelationModel = familyRelationModel;
	}

	public EducationLevel getEducationLevel() {
		return educationLevel;
	}

	public void setEducationLevel(EducationLevel educationLevel) {
		this.educationLevel = educationLevel;
	}
	
	
}
