package com.example.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Table(name="X_SCHEDULE_TYPE")
public class ScheduleTypeModel {
	
	@Id
	@Column(name="id", length=11, nullable=false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@CreatedBy
	@Column(name="CREATED_BY", length=11, nullable=false)
	private long createdBy;
	
	@CreatedDate
	@Column(name="CREATED_ON", nullable=false)
	private Date createdOn;
	
	@LastModifiedBy
	@Column(name="MODIFIED_BY", length=11, nullable=true)
	private long modifiedBy;
	
	@LastModifiedDate
	@Column(name="MODIFIED_ON", nullable=true)
	private Date modifiedOn;
	
	@Column(name="DELETED_BY", length=11, nullable=true)
	private long deletedBy;
	
	@Column(name="DELETED_ON", nullable=true)
	private Date deletedOn;
	
	@Column(name="IS_DELETE", nullable=false)
	private boolean isDelete;
	
	@Column(name="NAME", length=50, nullable=false)
	private String name;
	
	@Column(name="DESCRIPTION", length=100, nullable=true)
	private String description;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
