package com.example.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "X_SERTIFIKASI")
public class RiwayatSertifikasiModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, name = "ID", length = 11)
	private Long id;

	@Column(nullable = false, name = "CREATED_BY", length = 11)
	private Long createdBy;

	@Column(nullable = false, name = "CREATED_ON")
	private Date createdOn;

	@Column(nullable = false, name = "IS_DELETE")
	private Boolean isDelete;

	@Column(nullable = false, name = "BIODATA_ID")
	private Long biodataId;
	
	@ManyToOne
	@JoinColumn(name="BIODATA_ID", nullable = true, updatable = false, insertable = false)
	private BiodataModel biodataModel;

	@Column(name = "MODIFIED_BY", length = 11, nullable = true)
	private Long modifiedBy;

	@Column(name = "MODIFIED_ON", nullable = true)
	private Date modifiedOn;

	@Column(name = "DELETED_BY", length = 11, nullable = true)
	private Long deletedBy;

	@Column(name = "DELETED_ON", nullable = true)
	private Date deletedOn;

	@Column(name = "CERTIFICATE_NAME", length = 200)
	private String certificateName;

	@Column(name = "PUBLISHER", length = 100)
	private String publisher;

	@Column(name = "VALID_START_YEAR", length = 10)
	private String validStartYear;

	@Column(name = "VALID_START_MONTH", length = 10)
	private String validStartMonth;

	@Column(name = "UNTIL_YEAR", length = 10)
	private String untilYear;

	@Column(name = "UNTIL_MONTH", length = 10)
	private String untilMonth;

	@Column(name = "NOTES", length = 1000)
	private String notes;

	public BiodataModel getBiodataModel() {
		return biodataModel;
	}

	public void setBiodataModel(BiodataModel biodataModel) {
		this.biodataModel = biodataModel;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public String getCertificateName() {
		return certificateName;
	}

	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getValidStartYear() {
		return validStartYear;
	}

	public void setValidStartYear(String validStartYear) {
		this.validStartYear = validStartYear;
	}

	public String getValidStartMonth() {
		return validStartMonth;
	}

	public void setValidStartMonth(String validStartMonth) {
		this.validStartMonth = validStartMonth;
	}

	public String getUntilYear() {
		return untilYear;
	}

	public void setUntilYear(String untilYear) {
		this.untilYear = untilYear;
	}

	public String getUntilMonth() {
		return untilMonth;
	}

	public void setUntilMonth(String untilMonth) {
		this.untilMonth = untilMonth;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

}
