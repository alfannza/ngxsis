package com.example.demo.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.Undangan;

public interface UndanganRepository extends JpaRepository<Undangan, Long> {

	@Query("SELECT U FROM Undangan U WHERE U.isDelete!=true")
	List<Undangan> findIsNotDelete();
	
	@Query("SELECT U FROM Undangan U WHERE U.id=?1")
	Undangan searchById(Long id);
	
	@Query("SELECT U FROM Undangan U WHERE U.isDelete!=true ORDER BY U.modifiedOn ASC")
	List<Undangan> findAscending();
	
	@Query("SELECT U FROM Undangan U WHERE U.isDelete!=true ORDER BY U.modifiedOn DESC")
	List<Undangan> findDescending();
	
	@Query("SELECT U FROM Undangan U WHERE U.scheduleTypeId=?1")
	List<Undangan> findByScheduleTypeId(Long id);
	
	@Query("SELECT U FROM Undangan U WHERE U.createdOn = :createdOn")
	Undangan findByCreatedOn(
			@Param("createdOn") Date createdOn);
	
	@Query("SELECT U FROM Undangan U WHERE U.modifiedOn = :modifiedOn")
	Undangan findByModifiedOn(
			@Param("modifiedOn") Date modifiedOn);
}
