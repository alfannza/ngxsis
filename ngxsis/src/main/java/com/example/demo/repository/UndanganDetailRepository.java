package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.UndanganDetail;

public interface UndanganDetailRepository extends JpaRepository<UndanganDetail, Long> {

	@Query("SELECT UD FROM UndanganDetail UD WHERE UD.isDelete != true")
	List<UndanganDetail> findIsNotDelete();
	
	@Query("SELECT UD FROM UndanganDetail UD WHERE UD.isDelete != true ORDER BY UD.biodataId ASC")
	List<UndanganDetail> findAscendingBiodata();
	
	@Query("SELECT UD FROM UndanganDetail UD WHERE UD.isDelete != true ORDER BY UD.biodataId DESC")
	List<UndanganDetail> findDescendingBiodata();
	
	@Query("SELECT UD FROM UndanganDetail UD WHERE UD.id = ?1")
	UndanganDetail searchById(Long id);
	
	@Query("SELECT UD FROM UndanganDetail UD WHERE UD.biodataModel.fullname LIKE %?1% AND UD.isDelete != true")
	List<UndanganDetail> searchLikeNamePelamar(String name);
}
