package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.CompanyModel;

public interface CompanyRepository extends JpaRepository<CompanyModel, Long> {

	@Query("SELECT C FROM CompanyModel C WHERE C.isDelete != true")
	List<CompanyModel> findIsNotDelete();
	
}
