package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.demo.model.NoteTypeModel;

public interface NoteTypeRepository extends JpaRepository<NoteTypeModel, String>{
	
}
