package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.RoleModel;

public interface RoleRepository extends JpaRepository<RoleModel, Long>  {

	@Query("SELECT RM FROM RoleModel RM WHERE RM.isDelete != true")
	List<RoleModel> findIsNotDelete();
	
	@Query("SELECT RM FROM RoleModel RM WHERE RM.name = ?1")
	RoleModel findByName(String name);
	
	@Query("SELECT RM FROM RoleModel RM WHERE RM.code = ?1")
	RoleModel findByCode(String code);
	
	@Query("SELECT RM FROM RoleModel RM WHERE RM.isDelete != true ORDER BY RM.name ASC")
	List<RoleModel> findAscending();
	
	@Query("SELECT RM FROM RoleModel RM WHERE RM.isDelete != true ORDER BY RM.name DESC")
	List<RoleModel> findDescending();
	
	@Query("SELECT RM FROM RoleModel RM WHERE RM.name LIKE %?1% AND RM.isDelete != true ORDER BY RM.name ASC")
	List<RoleModel> searchLikeName(String name);
	
	@Query("SELECT COUNT(RM.id) FROM RoleModel RM")
	Integer countRole();
}
