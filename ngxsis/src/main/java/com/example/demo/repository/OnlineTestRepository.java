package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.OnlineTestModel;

public interface OnlineTestRepository extends JpaRepository<OnlineTestModel, String>{
	
	@Query("SELECT MAX(OT.id) FROM OnlineTestModel OT")
	Long searchMaxId();
	
	@Query("SELECT OT FROM OnlineTestModel OT WHERE OT.id = ?1")
	OnlineTestModel searchIdOnlineTest(Long id);
	
	@Query("SELECT OT FROM OnlineTestModel OT WHERE OT.isDelete = FALSE")
	List<OnlineTestModel> searchNotDelete();
	
	@Query("SELECT OT FROM OnlineTestModel OT WHERE OT.id = ?1")
	OnlineTestModel searchId(String id);
	
	@Query("SELECT OT FROM OnlineTestModel OT WHERE OT.biodataId = ?1")
	List<OnlineTestModel> searchOnlineTestByBiodata(Long id);
	
	@Query("SELECT OT FROM OnlineTestModel OT WHERE OT.biodataId=?1 AND OT.isDelete != true ORDER BY OT.createdOn ASC")
	List<OnlineTestModel> findByBiodataAndIsNotDelete(Long biodataId);
	
	@Query("SELECT OT FROM OnlineTestModel OT WHERE OT.biodataId=?1")
	List<OnlineTestModel> findByBiodata(Long biodataId);
	
	@Query("SELECT MAX(OT.period) FROM OnlineTestModel OT WHERE OT.biodataId =?1")
	Integer searchMaxPeriodByBiodataId(Long biodataId);
	
	@Query("SELECT MAX(OT.periodCode) FROM OnlineTestModel OT")
	String searchLastPRD();
	
	@Query("SELECT COUNT(OT.id) FROM OnlineTestModel OT")
	Integer getSize();
}
