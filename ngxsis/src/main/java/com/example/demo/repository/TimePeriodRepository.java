package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.TimePeriodModel;

public interface TimePeriodRepository extends JpaRepository<TimePeriodModel, Long> {

	@Query("SELECT TP FROM TimePeriodModel TP WHERE TP.isDelete != true")
	List<TimePeriodModel> searchIsNotDelete();
}
