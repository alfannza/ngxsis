package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.AddrBookModel;

public interface AddrBookRepository extends JpaRepository<AddrBookModel, Long> {
    
	@Query("SELECT ABM FROM AddrBookModel ABM WHERE ABM.email = ?1 OR ABM.abuid = ?2 AND ABM.abpwd = ?3 AND ABM.isDelete != true AND ABM.isLocked != true")
	AddrBookModel searchUsername(String email, String abuid, String abpwd); 
	
	@Query("SELECT ABM FROM AddrBookModel ABM WHERE ABM.email = ?1 OR ABM.abuid = ?2")
	AddrBookModel cekUsername(String email, String abuid);
	
	@Query("SELECT ABM FROM AddrBookModel ABM WHERE ABM.id = ?1")
	AddrBookModel findByIdAddrbook(Long id);
	
	@Query("SELECT COUNT(ABM.id) FROM AddrBookModel ABM")
	Long getSize();
}