package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.OnlineTestDetailModel;

public interface OnlineTestDetailRepository extends JpaRepository<OnlineTestDetailModel, String>{
	
	@Query("SELECT MAX(OTD.id) FROM OnlineTestDetailModel OTD")
	Long searchMaxId();
	
	@Query("SELECT OTD FROM OnlineTestDetailModel OTD WHERE OTD.id = ?1")
	OnlineTestDetailModel searchIdOnlineTest(Long id);
	
	@Query("SELECT OTD FROM OnlineTestDetailModel OTD WHERE OTD.isDelete = FALSE")
	List<OnlineTestDetailModel> searchNotDelete();
	
	@Query("SELECT OTD FROM OnlineTestDetailModel OTD WHERE OTD.id = ?1")
	OnlineTestDetailModel searchId(String id);
	
	@Query("SELECT OTD FROM OnlineTestDetailModel OTD WHERE OTD.onlineTestId = ?1")
	List<OnlineTestDetailModel> searchOnlineTestDetailByOnlineTestId(Long id);
	
	@Query("SELECT OTD FROM OnlineTestDetailModel OTD WHERE OTD.onlineTestId=?1 AND OTD.isDelete != true ORDER BY OTD.testOrder ASC")
	List<OnlineTestDetailModel> findByOnlineTestIdAndIsNotDelete(Long onlineTestId);
	
	//@Query("SELECT MAX(OTD.testOrder) FROM OnlineTestDetailModel OTD WHERE OTD.onlineTestId = ?1")
	@Query("SELECT MAX(OTD.testOrder) FROM OnlineTestDetailModel OTD WHERE OTD.isDelete = false AND OTD.onlineTestId = ?1")
	Integer findMaxOrderByOnlineTestId(Long onlineTestId);
	
	@Modifying
	@Query("UPDATE OnlineTestDetailModel SET testOrder = testOrder - 1 WHERE onlineTestId = ?1 AND testOrder > ?2")
	void updateTestOrder(Long onlineTestId, Integer testOrder);
	
}
