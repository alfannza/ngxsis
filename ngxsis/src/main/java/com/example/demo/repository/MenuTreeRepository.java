package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.MenuTreeModel;

public interface MenuTreeRepository extends JpaRepository<MenuTreeModel, Long> {

	@Query("SELECT MT FROM MenuTreeModel MT WHERE MT.isDelete != true")
	List<MenuTreeModel> findIsNotDelete();

	@Query("SELECT MT FROM MenuTreeModel MT	JOIN MenuAccessModel MA ON MA.menutreeId = MT.id WHERE MT.isDelete=false AND MA.roleId= ?1 AND MT.menuType='HEADER' AND MT.menuLevel=0")
	List<MenuTreeModel> getMenuList(Long id);
	
	@Query("SELECT MT FROM MenuTreeModel MT	JOIN MenuAccessModel MA ON MA.menutreeId = MT.id WHERE MT.isDelete=false AND MA.roleId= ?1 AND MT.menuType='HEADER' AND MT.menuLevel=1")
	List<MenuTreeModel> getSubMenuList(Long id);
	
	@Query("SELECT MT FROM MenuTreeModel MT	JOIN MenuAccessModel MA ON MA.menutreeId = MT.id WHERE MT.isDelete=false AND MA.roleId= ?1 AND MT.menuType='NAVBAR' AND MT.menuLevel=0")
	List<MenuTreeModel> getNavbarList(Long id);
	
	@Query("SELECT MT FROM MenuTreeModel MT	JOIN MenuAccessModel MA ON MA.menutreeId = MT.id WHERE MT.isDelete=false AND MA.roleId= ?1 AND MT.menuType='NAVBAR' AND MT.menuLevel=1")
	List<MenuTreeModel> getSubNavbarList(Long id);

}
