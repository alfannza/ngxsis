package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.OrganisasiModel;

public interface OrganisasiRepository extends JpaRepository<OrganisasiModel, String>{
	
	@Query("SELECT MAX(OM.id) FROM OrganisasiModel OM")
	Long searchMaxId();
	
	@Query("SELECT OM FROM OrganisasiModel OM WHERE OM.id = ?1")
	OrganisasiModel searchIdOrganisasi(Long id);
	
	@Query("SELECT OM FROM OrganisasiModel OM WHERE OM.isDelete = FALSE")
	List<OrganisasiModel> searchNotDelete();
	
	@Query("SELECT OM FROM OrganisasiModel OM WHERE OM.id = ?1")
	OrganisasiModel searchId(String id);
	
	@Query("SELECT OM FROM OrganisasiModel OM WHERE OM.biodataId=?1 AND OM.isDelete != true ORDER BY OM.createdOn ASC")
	List<OrganisasiModel> findByBiodataAndIsNotDelete(Long biodataId);
}
