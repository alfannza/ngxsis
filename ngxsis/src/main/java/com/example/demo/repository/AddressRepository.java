package com.example.demo.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.AddressModel;

public interface AddressRepository extends JpaRepository<AddressModel, String>
{
	@Query("SELECT A FROM AddressModel A WHERE A.biodataModel.id=?1")	
	AddressModel searchIdBiodata(Long id);
}