package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.ScheduleTypeModel;

public interface ScheduleTypeRepository extends JpaRepository<ScheduleTypeModel, Long> {
	
	@Query("SELECT S FROM ScheduleTypeModel S WHERE S.isDelete!=true")
	List<ScheduleTypeModel> findIsNotDelete();
	
	@Query("SELECT S FROM ScheduleTypeModel S WHERE S.id=?1")
	ScheduleTypeModel searchById(Long id);
	
	@Query("SELECT S FROM ScheduleTypeModel S WHERE S.isDelete != true ORDER BY S.name ASC")
	List<ScheduleTypeModel> findAscending();
	
	@Query("SELECT S FROM ScheduleTypeModel S WHERE S.isDelete != true ORDER BY S.name DESC")
	List<ScheduleTypeModel> findDescending();
	
	@Query("SELECT S FROM ScheduleTypeModel S WHERE S.name LIKE %?1% AND S.isDelete != true ORDER BY S.name ASC")
	List<ScheduleTypeModel> searchLikeName(String name);
}
