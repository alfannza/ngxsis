package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.model.PeReferensiModel;

@Repository
public interface PeReferensiRepository extends JpaRepository<PeReferensiModel, Long>{

	@Query("SELECT PR FROM PeReferensiModel PR WHERE PR.biodataId=?1")	
	List<PeReferensiModel> searchPeRefrensiByBiodataId(Long biodataId);
	
	@Query("SELECT PR FROM PeReferensiModel PR WHERE PR.id=?1 AND PR.isDelete != true")	
	PeReferensiModel searchPeRefrensiById(Long id);
	
	@Query("SELECT PR FROM PeReferensiModel PR WHERE PR.isDelete != true  AND PR.biodataId = ?1 ORDER BY PR.id ASC")
	List<PeReferensiModel> getReference(Long id);
	
	@Query("SELECT PR FROM PeReferensiModel PR WHERE PR.name = ?1")
	PeReferensiModel getReference(String name);
	
	@Query("SELECT PR FROM PeReferensiModel PR WHERE PR.isDelete != true")
	List<PeReferensiModel> findIsNotDelete();
	
}
