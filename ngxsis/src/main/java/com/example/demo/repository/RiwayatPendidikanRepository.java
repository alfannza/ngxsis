package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.demo.model.RiwayatPendidikanModel;
import org.springframework.data.jpa.repository.Query;

public interface RiwayatPendidikanRepository extends JpaRepository<RiwayatPendidikanModel, Long> {

	@Query("SELECT RP FROM RiwayatPendidikanModel RP WHERE RP.id=?1 AND RP.isDelete != true")
	RiwayatPendidikanModel searchIdRiwayatPendidikan(Long id);
	
	@Query("SELECT RP FROM RiwayatPendidikanModel RP WHERE RP.biodataId=?1 AND RP.isDelete!=true ORDER BY RP.educationLevelId ASC")
	List<RiwayatPendidikanModel> searchPendidikanByBiodataId(Long biodataId);
	
	@Query("SELECT RP FROM RiwayatPendidikanModel RP WHERE RP.biodataId= ?1 AND RP.isDelete!=true ORDER BY RP.graduationYear DESC")
	List<RiwayatPendidikanModel> searchIdPendidikanTerahir(Long biodataId);

	@Query("SELECT RP FROM RiwayatPendidikanModel RP WHERE RP.biodataId=?1 AND RP.isDelete != true ORDER BY RP.educationLevelId ASC")
	List<RiwayatPendidikanModel> findByBiodataAndIsNotDelete(Long biodataId);
	
}
