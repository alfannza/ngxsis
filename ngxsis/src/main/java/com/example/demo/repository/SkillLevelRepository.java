package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.SkillLevelModel;

public interface SkillLevelRepository extends JpaRepository<SkillLevelModel, String> {

	@Query("SELECT SL FROM SkillLevelModel SL WHERE SL.isDelete != true")
	List<SkillLevelModel> searchSkillLevelIsNotDelete();
}
