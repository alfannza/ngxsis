package com.example.demo.repository;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.model.BiodataModel;

@Repository
public interface BiodataCRUDRepository extends CrudRepository<BiodataModel, Long> {
	
	@Query("SELECT B.fullname FROM BiodataModel B WHERE B.fullname LIKE '%:keyword%'  AND B.isDelete != true")
	List<String> search(@Param("keyword") String keyword);

}
