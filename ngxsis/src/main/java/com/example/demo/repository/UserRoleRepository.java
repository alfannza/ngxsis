package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.UserRoleModel;

public interface UserRoleRepository extends JpaRepository<UserRoleModel, Long> {
	
	@Query("SELECT UR FROM UserRoleModel UR WHERE UR.addrbookId = ?1 AND UR.isDelete !=true")
	List<UserRoleModel> findByAddrBookId(Long id);
	
	@Query("SELECT UR FROM UserRoleModel UR WHERE UR.addrbookId = ?1 AND UR.roleId = ?2")
	UserRoleModel findByUserRoleAndAddrBook(Long addrBookId, Long roleId);
}
