package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.RiwayatPelatihanModel;

public interface RiwayatPelatihanRepository extends JpaRepository<RiwayatPelatihanModel, Long> {

	@Query("SELECT P FROM RiwayatPelatihanModel P WHERE P.id=?1 AND P.isDelete != true")
	RiwayatPelatihanModel searchIdRiwayatPelatihan(Long id);
	
	@Query("SELECT P FROM RiwayatPelatihanModel P WHERE P.biodataId = ?1 AND P.isDelete != true ORDER BY P.createdOn ASC")
	List<RiwayatPelatihanModel> findByBiodataAndIsNotDelete(Long biodataId);
	
	@Query("SELECT P FROM RiwayatPelatihanModel P WHERE P.biodataId=?1 AND P.isDelete != true ORDER BY P.createdOn ASC")
	List<RiwayatPelatihanModel> searchPelatihanByBiodataId(Long biodataId);
}
