package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import com.example.demo.model.RiwayatProyekModel;

public interface RiwayatProyekRepository extends JpaRepository<RiwayatProyekModel, String>{
	
	@Query("SELECT MAX(RPM.id) FROM RiwayatProyekModel RPM")
	Long searchMaxId();
	
	@Query("SELECT OM FROM RiwayatProyekModel OM WHERE OM.id = ?1")
	RiwayatProyekModel searchIdProyek(Long id);
	
	//@Query("SELECT OM FROM RiwayatProyekModel OM WHERE OM.isDelete = FALSE")
	@Query("SELECT OM FROM RiwayatProyekModel OM WHERE OM.isDelete = FALSE ORDER BY OM.startYear ASC, OM.startMonth ASC")
	List<RiwayatProyekModel> searchNotDelete();
	
	@Query("SELECT OM FROM RiwayatProyekModel OM WHERE OM.id = ?1")
	RiwayatProyekModel searchId(String id);
	
	@Query("SELECT RPM FROM RiwayatProyekModel RPM WHERE RPM.riwayatPekerjaanId=?1 AND RPM.isDelete != true ORDER BY RPM.createdOn ASC")
	List<RiwayatProyekModel> searchByPekerjaanId(Long idPekerjaan);
}
