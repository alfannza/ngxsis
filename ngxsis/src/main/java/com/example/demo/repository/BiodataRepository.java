package com.example.demo.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.BiodataModel;


public interface BiodataRepository extends JpaRepository<BiodataModel, Long>
{
	@Query("SELECT J FROM BiodataModel J WHERE J.id=?1 AND J.isDelete != true")	
	BiodataModel searchIdBiodata(Long id);
	
	@Query("select count(B)>0 from BiodataModel B WHERE B.email=?1")
	boolean emailSama(String email);
	
	@Query("select count(B)>0 from BiodataModel B WHERE B.phoneNumber1=?1")
	boolean noSama(String phoneNumber1);
	
	@Query("select count(B)>0 from BiodataModel B WHERE B.identityTypeId=?1 AND B.identityNo=?2")
	boolean identitasSama(Long identityTypeId, String identityNo);
	
	@Query("SELECT B FROM BiodataModel B WHERE B.fullname LIKE %?1%")
	List<BiodataModel> searchNamaPelamar(String fullname);
	
	@Query("SELECT B FROM BiodataModel B WHERE B.isDelete != true ORDER BY B.fullname ASC")
	List<BiodataModel> ascPelamar();
	
	@Query("SELECT B FROM BiodataModel B WHERE B.isDelete != true ORDER BY B.fullname DESC")
	List<BiodataModel> descPelamar();
	
	@Query("SELECT B FROM BiodataModel B WHERE B.addrbookId=?1 AND B.isDelete != true")
	BiodataModel searchAddrBookId(Long addrbookid);
	
	@Query("SELECT B FROM BiodataModel B WHERE B.isDelete != true")
	List<BiodataModel> searchNotDelete();
	
	@Query("SELECT B FROM BiodataModel B WHERE B.fullname LIKE '%?1%'")
	List<BiodataModel> searchUser(String search);
	
}