package com.example.demo.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.example.demo.model.IdentityTypeModel;
public interface IdentityTypeRepository extends JpaRepository<IdentityTypeModel, Long>{
	@Query("SELECT B FROM IdentityTypeModel B WHERE B.id=?1")
	IdentityTypeModel searchIdIdentityType(Long id);

}
