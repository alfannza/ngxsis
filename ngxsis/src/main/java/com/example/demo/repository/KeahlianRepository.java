package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.KeahlianModel;

public interface KeahlianRepository extends JpaRepository<KeahlianModel, String> {

	@Query("SELECT K FROM KeahlianModel K WHERE K.id=?1 AND K.isDelete != true")
	KeahlianModel searchIdKeahlian(Long id);
	
	@Query("SELECT K FROM KeahlianModel K WHERE K.biodataId = ?1 AND K.isDelete !=true ORDER BY K.createdOn ASC")
	List<KeahlianModel> findByBiodataAndIsNotDelete(Long biodataId);
	
	@Query("SELECT K FROM KeahlianModel K WHERE K.biodataId=?1 AND K.skillLevelId=?2 AND K.isDelete != true")
	List<KeahlianModel> searchKeahlianByBiodataId(Long biodataId, Long skillLevelId);
}
