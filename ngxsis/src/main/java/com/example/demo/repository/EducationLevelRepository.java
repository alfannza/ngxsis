package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.EducationLevel;

public interface EducationLevelRepository extends JpaRepository<EducationLevel, Long> {

	@Query("SELECT EL FROM EducationLevel EL WHERE EL.isDelete != true")
	List<EducationLevel> searchIsNotDelete();
	
	@Query("SELECT EL FROM EducationLevel EL WHERE EL.id = ?1 AND EL.isDelete != true")
	EducationLevel searchById(Long id);
	
}
