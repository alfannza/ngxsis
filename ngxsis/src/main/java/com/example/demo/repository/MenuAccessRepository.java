package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.MenuAccessModel;

public interface MenuAccessRepository extends JpaRepository<MenuAccessModel, String> {
	
	
}
