package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.TestTypeModel;

public interface TestTypeRepository extends JpaRepository<TestTypeModel, String>{
	
	@Query("SELECT T FROM TestTypeModel T WHERE T.isDelete = FALSE")
	List<TestTypeModel> searchNotDelete();
}
