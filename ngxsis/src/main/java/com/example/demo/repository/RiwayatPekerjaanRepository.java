package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.RiwayatPekerjaanModel;

public interface RiwayatPekerjaanRepository extends JpaRepository<RiwayatPekerjaanModel, String> {

	@Query("SELECT RPM FROM RiwayatPekerjaanModel RPM WHERE RPM.id=?1 AND RPM.isDelete != true")	
	RiwayatPekerjaanModel searchIdPekerjaan(Long id);
	
	@Query("SELECT OM FROM RiwayatPekerjaanModel OM WHERE OM.isDelete = FALSE")
	List<RiwayatPekerjaanModel> searchNotDelete();
	
	@Query("SELECT RPM FROM RiwayatPekerjaanModel RPM WHERE RPM.biodataId=?1")
	List<RiwayatPekerjaanModel> searchPekerjaanByBiodataId(Long id);

	//@Query("SELECT RPM FROM RiwayatPekerjaanModel RPM WHERE RPM.biodataId=?1 AND RPM.isDelete != true ORDER BY RPM.createdOn ASC")
	@Query("SELECT RPM FROM RiwayatPekerjaanModel RPM WHERE RPM.biodataId=?1 AND RPM.isDelete != true ORDER BY RPM.joinYear ASC, RPM.joinMonth ASC")
	List<RiwayatPekerjaanModel> findByBiodataAndIsNotDelete(Long biodataId);
}
