package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.model.FamilyRelationModel;

@Repository
public interface FamilyRelationRepository extends JpaRepository<FamilyRelationModel, Long>{

	@Query("SELECT FR FROM FamilyRelationModel FR WHERE FR.familyTreeTypeId = ?1 AND FR.isDelete != true")
	List<FamilyRelationModel> searchRelationList(Long id);
}
