package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.BiodataAttachmentModel;

public interface BiodataAttachmentRepository extends JpaRepository<BiodataAttachmentModel, String>{

	@Query("SELECT BAM FROM BiodataAttachmentModel BAM WHERE BAM.biodataId = ?1 AND BAM.isDelete != true AND BAM.isPhoto = true")
	BiodataAttachmentModel searchByBiodataId(Long biodataId);
}
