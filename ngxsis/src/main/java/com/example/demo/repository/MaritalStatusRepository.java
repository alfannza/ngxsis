package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.MaritalStatusModel;


public interface MaritalStatusRepository extends JpaRepository<MaritalStatusModel, String>{
	@Query("SELECT B FROM MaritalStatusModel B WHERE B.id=?1")	
	MaritalStatusModel searchIdMaritalStatus(Long id);
}
