package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.model.KeteranganTambahanModel;

@Repository
public interface KeteranganTambahanRepository extends JpaRepository<KeteranganTambahanModel, Long>{
	
	@Query("SELECT KT FROM KeteranganTambahanModel KT WHERE KT.id=?1")
	KeteranganTambahanModel getAdditionalInfo(Long id);
	
	

}
