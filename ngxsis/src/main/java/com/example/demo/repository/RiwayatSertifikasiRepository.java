package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.RiwayatSertifikasiModel;

public interface RiwayatSertifikasiRepository extends JpaRepository<RiwayatSertifikasiModel, String>{
	
	@Query("SELECT RS FROM RiwayatSertifikasiModel RS WHERE RS.biodataId = ?1 AND RS.isDelete != true ORDER BY RS.createdOn ASC")
	List<RiwayatSertifikasiModel> searchSertifikasiByBiodataId(Long biodataId);
	
	@Query("SELECT RS FROM RiwayatSertifikasiModel RS WHERE RS.biodataId = ?1 AND RS.isDelete != true ORDER BY RS.createdOn ASC")
	List<RiwayatSertifikasiModel> findByBiodataAndIsNotDelete(Long biodataId);
	
	@Query("SELECT RS FROM RiwayatSertifikasiModel RS WHERE RS.id=?1")
	RiwayatSertifikasiModel searchIdRiwayatSertifikasi(Long id);
}
