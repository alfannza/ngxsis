package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.ReligionModel;

public interface ReligionRepository extends JpaRepository<ReligionModel, String>{
	@Query("SELECT B FROM ReligionModel B WHERE B.id=?1")	
	ReligionModel searchIdReligion(Long id);
}
