package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.model.RiwayatSertifikasiModel;

@Repository
public interface SertifikasiRepository extends JpaRepository<RiwayatSertifikasiModel, Long>{

	@Query("SELECT S FROM RiwayatSertifikasiModel S WHERE S.biodataId=?1")	
	List<RiwayatSertifikasiModel> searchSertifikasiByBiodataId(Long biodataId);
}
