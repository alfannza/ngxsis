package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.CatatanModel;

public interface CatatanRepository extends JpaRepository<CatatanModel, String>{
	
	@Query("SELECT MAX(C.id) FROM CatatanModel C")
	Long searchMaxId();
	
	@Query("SELECT C FROM CatatanModel C WHERE C.id = ?1")
	CatatanModel searchIdCatatan(Long id);
	
	@Query("SELECT C FROM CatatanModel C WHERE C.isDelete = FALSE")
	List<CatatanModel> searchNotDelete();
	
	@Query("SELECT C FROM CatatanModel C WHERE C.id = ?1")
	CatatanModel searchId(String id);
	
	@Query("SELECT C FROM CatatanModel C WHERE C.biodataId = ?1")
	List<CatatanModel> searchCatatanByBiodata(Long id);
	
	@Query("SELECT C FROM CatatanModel C WHERE C.biodataId=?1 AND C.isDelete != true ORDER BY C.createdOn ASC")
	List<CatatanModel> findByBiodataAndIsNotDelete(Long biodataId);
}
