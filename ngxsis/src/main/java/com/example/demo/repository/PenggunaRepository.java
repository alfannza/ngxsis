package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.model.AddrBookModel;
import com.example.demo.model.BiodataModel;

@Repository
public interface PenggunaRepository extends JpaRepository<AddrBookModel, Long>{

	@Query("SELECT B FROM BiodataModel B WHERE B.fullname LIKE '%?1%'")
	List<BiodataModel> searchUser(String search);
}
