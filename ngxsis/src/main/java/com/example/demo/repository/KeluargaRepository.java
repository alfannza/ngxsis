package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.KeluargaModel;
import com.example.demo.model.PeReferensiModel;

public interface KeluargaRepository extends JpaRepository<KeluargaModel, Long> {
	
	@Query("SELECT K FROM KeluargaModel K WHERE K.isDelete != true AND K.biodataId=?1")	
	List<KeluargaModel> searchKeluargaByBiodataId(Long biodataId);

	@Query("SELECT K FROM KeluargaModel K WHERE K.isDelete != true AND K.id=?1")	
	KeluargaModel searchKeluargaById(Long id);
	
}
